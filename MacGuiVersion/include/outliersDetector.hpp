/*
  A class implementing an Outlier Detector (a filter of fireflies for combined tracer).
*/

#ifndef outliersDetector_hpp
#define outliersDetector_hpp


#include <cstdio>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <mutex>

#include <GL/glew.h>
#include <glm/vec4.hpp>
#include <glm/vec2.hpp>

#include <someMath.hpp>


class OutliersDetector
{
protected:

  std::mutex bufferMutex;

  std::vector<float> valBuf[2];
  std::vector<float> sppBuf[2];

  glm::ivec2 imageSize;

  int maxDepth   = 5;     // Maximum number of layers
  float valScale = 1.0f;  // Income value multiplier

  const int minSPL = 3;   // Minimum samples per layer when outlier can work


  [[nodiscard]] int pointIndex(int buf, const glm::vec2 & crd) const;

  [[nodiscard]] int fillIndex(int ipage) const;

  [[nodiscard]] int checkIndex(int ipage) const;

public:

  OutliersDetector();

  void setParameters(int md, float sf);

  void initBuffer(const glm::ivec2 & size);

  void addToBuffer(const glm::vec2 & crd, int isample, float value);

  float checkOutlier(const glm::vec2 & crd, int isample, float value);

  [[nodiscard]] int getMaxBufferDepth() const;

  [[nodiscard]] float getValue(int buf, const glm::vec2 & crd);
};


#endif
