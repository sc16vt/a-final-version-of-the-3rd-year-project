/*
  A class implementing a combined Monte Carlo + MLT tracer.
  Has two versions: with Path Tracer as a base and Bidirectional Path Tracer as a base.
*/

#ifndef smmltTracer_hpp
#define smmltTracer_hpp


#include <mltTracer.hpp>
#include <outliersDetector.hpp>


enum SMMCLType
{
  smmPathBased,
  smmBDirBased,
};


class SMMCLTracer : public MLTTracer
{
protected:

  OutliersDetector outliersDetector;

  SMMCLType typeOfSMMCL;

  void runMarkovChain(const LightPath & path, float weight);

  void checkPathAndRunMlt(const LightPath & path, HeuristicType ht);

  void prepareRender() override;

  void updateSample() override;

  void tracePixelPath(const glm::vec2 & crd);

  void tracePixelBDir(const glm::vec2 & crd);

  void tracePixel(const glm::vec2 & crd) override;

public:

  SMMCLTracer(const Scene & sc, const Camera & cam, SMMCLType tp);
};


#endif
