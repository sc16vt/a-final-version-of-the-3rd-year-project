/*
  A class representing an abstract sphere an object is inside.
  It is used to check whether the ray intersects an object that lies inside this abstract sphere.
*/

#ifndef boundingSphere_hpp
#define boundingSphere_hpp

#include <GL/glew.h>
#include<vector>

#include<boundingBox.hpp>



class BoundingSphere : protected BoundingBox
{

  private:
    
    glm::vec3 center = glm::vec3(0.0f);
    float radius = 0;

  public:

    inline void expand(const BoundingBox & box)
    {
      BoundingBox::expand(box);

      center = 0.5f * (pointMin + pointMax);
      radius = glm::length(pointMin - pointMax);
    }

    void reset()
    {
      center = glm::vec3(0.0f);
      radius = 0;
    }

    [[nodiscard]] inline const glm::vec3 & getCenter() const { return center; }

    [[nodiscard]] inline float getRadius() const { return radius; }


};



#endif
