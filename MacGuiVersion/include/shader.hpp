//
//  shader.hpp
//  ANTTWEAKBAR_116_OGLCORE_GLFW
//  THIS CLASS IS TAKEN FROOM TUTORIALS MANY MANY MOON AGO AND IS NOT MADE BY ME. BE CAREFUL IF EVER REFERIMNG TO IT

#ifndef shader_hpp
#define shader_hpp

#include <cstdio>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>

class Shader
{
private:

  void checkCompileErrors(unsigned int shader, std::string type);

public:

  // the program ID
  unsigned int ID;

  // constructor reads and builds the shader
  Shader(const GLchar* vertexPath, const GLchar* fragmentPath);

  // use/activate the shader
  void use();

  // utility uniform functions
  void setBool(const std::string &name, bool value) const;

  void setInt(const std::string &name, int value) const;

  void setFloat(const std::string &name, float value) const;

  void set4Float(const std::string &name, float x, float y, float z, float w) const;
};


#endif /* shader_hpp */
