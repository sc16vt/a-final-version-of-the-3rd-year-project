/*
  A class representing a ray.
  Ray is used in tracers to find the point the light travels to or from.
*/
#ifndef ray_hpp
#define ray_hpp

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <intersectionPoint.hpp>


class Triangle;


class Ray
{
public:

  Ray();

  Ray(const glm::vec3 & ori, const glm::vec3 & dir);

  Ray(const IntersectionPoint & point, const glm::vec3 & dir);


  // Getters -------------------------------------------------------------------
  [[nodiscard]] inline const glm::vec3 & getOrigin() const { return origin; }

  [[nodiscard]] inline const Triangle * getStartSurf() const { return startSurf; }

  [[nodiscard]] inline const glm::vec3 & getDirection() const { return direction; }

  [[nodiscard]] inline float getDistance() const { return distance; }

  [[nodiscard]] IntersectionPoint getIntersection() const;


  // Setters -------------------------------------------------------------------
  void setIntersection(const Triangle * t, const glm::vec3 & p, float d);

private:

  glm::vec3 origin;
  glm::vec3 direction;

  const Triangle * startSurf = nullptr;

  float distance = FLT_MAX;

  glm::vec3 isectPoint;
  const Triangle * isectSurf;
};

#endif
