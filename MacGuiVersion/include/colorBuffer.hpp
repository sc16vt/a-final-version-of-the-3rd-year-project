/*
  A class is a buffer pixels' colors.
  It is used to store various stages of the pictuire being rendered.
*/

#ifndef colorBuffer_hpp
#define colorBuffer_hpp

#include <cstdio>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <mutex>
#include <sys/stat.h>

#include <GL/glew.h>
#include <glm/vec4.hpp>
#include <glm/vec2.hpp>

#include <someMath.hpp>


class ColorBuffer
{
private:

  // Should be private because protected by mutex, use addToCamera instead
  std::vector<glm::vec4> pureBuffer;
  std::mutex pureBufferMutex;

  std::vector<GLubyte> imageBufferRGB;
  std::vector<GLfloat> imageBufferHDR;

  glm::ivec2 imageSize;
  std::string imageName;

  float scaleFactor = 1.0f;

  [[nodiscard]] inline int makeIndex(const glm::vec2 & crd) const;

  [[nodiscard]] bool hasData() const;

  float getMaxLuminance();

  float getAverageLuminance();

public:

  explicit ColorBuffer(const std::string & name);


  [[nodiscard]] inline const std::vector<GLubyte> & getImageRGB() const { return imageBufferRGB; }

  [[nodiscard]] inline const std::vector<GLfloat> & getImageHDR() const { return imageBufferHDR; }

  inline void setScaleFactor(float factor) { scaleFactor = factor; }


  void addToBuffer(const glm::vec4 & color);

  void addToPixel(const glm::vec2 & crd, const glm::vec4 & color);

  void addFiltered(const glm::vec2 & crd, const glm::vec4 & color);


  void initBuffer(const glm::ivec2 & size);

  void generateImage();

  void saveToFile(const std::string & path, const std::string & name, bool hdr, bool png);
};

#endif
