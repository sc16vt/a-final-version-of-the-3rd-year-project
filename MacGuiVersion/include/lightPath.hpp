/*
  A class representing a light path.
  It is a collection of intersection points with additional information
  on the path that allows it to be used by tracers.
  Additionally, has functionality on changing the path.
*/

#ifndef lightPath_hpp
#define lightPath_hpp

#include <cstdio>
#include <string>


#include<object.hpp>
#include<scene.hpp>
#include<triangle.hpp>
#include<ray.hpp>
#include<material.hpp>
#include <sampler.hpp>
#include <intersectionPoint.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

#include <cmath>
#include <functional>
#include <random>


enum PathDir
{
  Forward,
  Reverse
};


enum HeuristicType
{
  Always1,
  Balanced,
  Power2,
  Maximum,
};


class LightPath : public std::vector<IntersectionPoint>
{
protected:

  // Target screen pixel for this path
  glm::vec2 ScreenCrd;

  // This path contribution in pixel light
  glm::vec3 radiance = glm::vec3(0.0f);

  // Weight of the path
  float wBalance = 0.0f;
  float wPower2  = 0.0f;
  float wMaximum = 0.0f;

  // Point insert direction and position
  PathDir insertDir = Forward;
  int insertPos = 0;

  std::vector<float> refractStack;

public:

  LightPath() {}

  LightPath(const glm::vec2 & crd, int len) : ScreenCrd(crd)
  {
    reserve(len);
    refractStack.reserve(len);
  }


  [[nodiscard]] const glm::vec2 & getScreenCrd() const { return ScreenCrd; }

  [[nodiscard]] glm::vec2 getScreenPos() const { return glm::vec2((float) ScreenCrd.x, (float) ScreenCrd.y); }

  bool updateScreenCrd(const Camera & cam)
  {
    // Camera should be placed at position 0. If path from Light ended at position 1 we have to update screen coordinates.
    if (1 != getLastInsertPos()) return true;
    return cam.getProjCoord(getLastInsertPoint().getPoint(), ScreenCrd);
  }


  // Some path check methods
  [[nodiscard]] bool isDirectCamToLight() const
  {
    return (size() == 2) && (*this)[0].isE() && (*this)[1].isL();
  }

  // If current position points out of bounds we have a path consists of only one part
  [[nodiscard]] bool isSingle() const { return insertPos < 0 || insertPos >= size(); }

  [[nodiscard]] bool isBlack() const { return (radiance.r + radiance.g + radiance.b <= 0.0f); }


  // Get new point insert direction
  [[nodiscard]] PathDir getPathDir() const { return insertDir; }

  // Set insert direction,
  //   forward to add points second after first for trace from Cam to Light (will insert AT saved position and UPDATE saved position to next)
  //   backward to insert points second before first for trace from Light to Can (will insert AT saved position and DON'T change saved position)
  void setPathDir(PathDir dir) { insertDir = dir; }

  // Return a position of the last inserted point
  [[nodiscard]] int getLastInsertPos() const { return (Forward == insertDir) ? insertPos - 1 : insertPos; }

  // Return a position of the last point of the cam sub path
  [[nodiscard]] int getConnectPos() const { return isSingle() ? insertPos : insertPos - 1; }

  // Return the last inserted point
  IntersectionPoint & getLastInsertPoint() { return (*this)[getLastInsertPos()]; }

  // Store pos as position before which insertPoint will always insert points
  void setCurrentPos(int pos) { insertPos = pos; }

  // Store the end of vector as current position and insertPoint will always insert points AT it
  void setCurrentPosToEnd() { insertPos = std::max(0, (int) size()); }

  // Insert point
  //   forward to add points second after first for trace from Cam to Light (will insert AFTER saved position and UPDATE saved position to next)
  //   backward to insert points second before first for trace from Light to Can (will insert BEFORE saved position and DON'T change saved position)
  void insertPoint(const IntersectionPoint & point);

  // Delete point and correct current insert pos if needed
  void deletePoint(int index);

  // Create new cut path from the current one
  void makeStrategy(int c, int l);


  // Refraction stack support
  [[nodiscard]] float findPrevRefraction();

  [[nodiscard]] float findNextRefraction();


  // Path types. Some mutations of MLT need certain light paths
  // (a combinations of intersection point of certain type one after another)
  int isE_D_Sm_DorL();

  int isE_Sm_D_DorL();


  // Radiance and probability calculations
  void computePath();

  [[nodiscard]] const glm::vec3 & getRadiance() const { return radiance; }

  [[nodiscard]] float getWeight(HeuristicType ht) const;


  [[nodiscard]] glm::vec3 getRadianceN() const  { return radiance / remap0to1(luminance(radiance)); }

  [[nodiscard]] glm::vec3 getRadianceW(HeuristicType ht) const { return getWeight(ht) * radiance; };


  [[nodiscard]] float getLuminance() const { return luminance(getRadiance()); }

  [[nodiscard]] float getLuminanceW(HeuristicType ht) const { return luminance(getRadianceW(ht)); }
};


#endif
