/*
A class representing a film to project the scene onto.
It is used to output tracing results into GUI.

!!! Uses OpenGl and is not used in this version of the program
(see report chapter on Design for explanation)
*/

#ifndef quad_hpp
#define quad_hpp

#include <cstdio>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#include <GL/glew.h>

#include <shader.hpp>
#include <glm/detail/type_vec.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>


class Quad
{
private:

  GLuint VAO, EBO, VBO, TBO;
  unsigned int number_of_elements;

  Shader * program;

  inline void setTextureFromArrayRGB
  (
    const std::vector<GLubyte> & buffer,
    int width, int height, const char * uniName, int num
  );

  inline void setTextureFromArrayHDR
  (
    const std::vector<GLfloat> & buffer,
    int width, int height, const char * uniName, int num
  );

  void setTextureFromImage(const char * texturePath, const char * uniName, int num);

public:

  Quad();
  ~Quad();

  void renderImage(const std::vector<GLubyte> & tracerBuffer, const glm::ivec2 & size);

  void renderImage(const std::vector<GLfloat> & tracerBuffer, const glm::ivec2 & size);

  void renderImage(char * texturePath);
};


#endif
