/*
  A class implementing a Bidirectional Path Tracer with Importance Sampling algorithm.
*/
#ifndef bdirTracer_hpp
#define bdirTracer_hpp


#include <pathTracer.hpp>


class BDirTracer : public PathTracer
{
protected:

  HeuristicType heuristicType;

  [[nodiscard]] int makeStrategyID(int c, int l, bool w) const;

  [[nodiscard]] std::string makeStrategyName(int c, int l, bool w) const;

  void tracePathFromLight(LightPath & path, int length);

  void traceBiDirPath(LightPath & path, int lcam, int llight);

  bool runOneStrategy(LightPath & path, const glm::vec2 & crd);

  void tracePixel(const glm::vec2 & crd) override;

public:

  BDirTracer(const Scene & sc, const Camera & cam);
};


#endif
