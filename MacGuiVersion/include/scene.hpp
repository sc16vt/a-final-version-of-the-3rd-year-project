/*
  A class representing a collection of test scenes.

*/

#ifndef scene_hpp
#define scene_hpp

#include <GL/glew.h>
#include <object.hpp>
#include <ray.hpp>
#include <intersectionPoint.hpp>
#include <boundingSphere.hpp>
#include <boundingBox.hpp>

#include <vector>

#include <shader.hpp>
#include <camera.hpp>
#include <material.hpp>
#include <set>


class Scene
{
private:

  std::string sceneName = "";

  std::vector<Object *> objects;
  bool dirLightFlag = false;


  std::vector< std::vector<Object *> > meshes; // A list of the scenes
  std::set<int> meshesWithDirLight;
  std::set<Object *> allObjects;

  std::vector<Object *> lights;

  Camera & camera;
  Shader * program;

  glm::vec3 dirLightColor;
  glm::vec3 dirLightDir;

  glm::vec3 ambientColor;

  int worldToScreenLoc;

  BoundingBox wholeSceneBox;
  BoundingSphere wholeSceneSphere;

public:
  static bool isOpenGLEnabled;

  Scene(Camera &cam);

  ~Scene();

  // Getters -------------------------------------------------------------------
  [[nodiscard]] inline int getNumeOfTestMeshes() const { return meshes.size(); }

  [[nodiscard]] inline const glm::vec3 & getAmbientColor() const { return ambientColor; }

  [[nodiscard]] inline const glm::vec3 & getDirLightDir() const { return dirLightDir; }

  [[nodiscard]] inline const glm::vec3 & getDirLightColor() const { return dirLightColor; }

  [[nodiscard]] inline const std::vector<Object *> & getLights() const { return lights; }

  [[nodiscard]] inline const BoundingBox & getBoundingBox() const { return wholeSceneBox; }

  [[nodiscard]] inline const BoundingSphere & getBoundingSphere() const { return wholeSceneSphere; }

  [[nodiscard]] inline const std::string & getSceneName() const { return sceneName; }



  [[nodiscard]] inline bool hasDirLight() const { return dirLightFlag; }

  // Select a scene out of the list of them
  void selectTestMesh(int sel);

  // Add an object to the scenes specified by indices
  void addObjToMeshes(Object * obj, const std::vector<int> & indices);

  // Generate the objects and put them into the test scenes
  void generateTestMeshes();

  // The object can both traced and rasterised.
  // This function prepares the abstract model of the scene to be traced
  void prepareForTracer();


  // Finds a triangle in the scene the goven ray intersects first.
  // If it is, returns the specific triangle (the object is modeled from) the ray hits
  const Triangle * traceRay(Ray & ray) const;


  // The scene can both traced and rasterised.
  // This function prepares the abstract model of the scene to be rasterised
  // !!! Uses OpenGl and is not used in this version of the program
  // (see report chapter on Design for explanation)
  void prepareForRender();

  // Renders the scene using rasterisation
  // !!! Uses OpenGl and is not used in this version of the program
  // (see report chapter on Design for explanation)
  void render() const;

};


#endif
