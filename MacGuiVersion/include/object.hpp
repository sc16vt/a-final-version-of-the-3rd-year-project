/*
  A class representing an object.
  Is used to model a scene and
  enable intersection check
  (whether a given ray goes through this triangle or not).

  Has predefined shapes of the object it can represent.
*/

#ifndef object_hpp
#define object_hpp

#include <GL/glew.h>
#include <triangle.hpp>
#include <ray.hpp>
#include <boundingSphere.hpp>
#include <boundingBox.hpp>

#include<vector>
#include<cmath>

#include <shader.hpp>
#include <camera.hpp>
#include <random>


class Object
{
private:

  Material material;

  std::vector<GLfloat> vertices, colors, normals, texcoords;
  std::vector<GLuint> indices;

  std::vector<Triangle> triangles;

  std::discrete_distribution<int> trianglesDistr;
  float objectArea;

  glm::mat4 objToWorld;
  glm::vec3 objScale;

  GLuint VBO, CBO, NBO, EBO;
  GLuint VAO;

  bool isWireframe = false;
  bool isSmooth = true;

  Camera & camera;

  std::vector<BoundingBox> innerBoxes;
  BoundingBox wholeObjectBox;
  BoundingSphere wholeObjectSphere;

public:

  static bool isOpenGLEnabled;


  Object(const glm::vec3 & translate, float rotateAngle, const glm::vec3 & rotateLine, const glm::vec3 & scale, Camera & cam);

  Object(const glm::vec3 & translate, const glm::vec3 & scale, Camera & cam);

  ~Object();

  // The shapes the object can represent ---------------------------------------

  Object & generateCube(const Material & mat);

  Object & generateSphere(const Material & mat, int samples, bool smooth);

  Object & generateWaveSurface(const Material & mat, int samples, float freq, float ampl, const glm::vec2 & dot);

  Object & generatePlaneXY(const Material & mat);

  Object & generatePlaneYZ(const Material & mat);


  // Setters -------------------------------------------------------------------
  inline Material & setMaterial(const Material & mat) { return (material = mat); }


  // Getters -------------------------------------------------------------------
  // Returns a list of triangles this object is modeled as
  [[nodiscard]] inline const std::vector<Triangle> & getTriangles() const { return triangles; }

  [[nodiscard]] inline float getObjectArea() const { return objectArea; }

  inline Material & getMaterial() { return material; }

  [[nodiscard]] inline const BoundingBox & getBoundingBox() const { return wholeObjectBox; }

  [[nodiscard]] inline const BoundingSphere & getBoundingSphere() const { return wholeObjectSphere; }

  [[nodiscard]] inline std::discrete_distribution<int> & getTrianglesDistr() { return trianglesDistr; }


  // The object can both traced and rasterised.
  // This function prepares the abstract model of the object to be traced
  void prepareForTracer();

  // Checks if this object is hit by the given ray and is the closest to it so far.
  // If it is, returns the specific triangle (the object is modeled from) the ray hits
  const Triangle * traceRay(Ray & ray);

  // The object can both traced and rasterised.
  // This function prepares the abstract model of the object to be rasterised
  // !!! Uses OpenGl and is not used in this version of the program
  // (see report chapter on Design for explanation)
  void prepareForRender();

  // Renders the object using rasterisation
  // !!! Uses OpenGl and is not used in this version of the program
  // (see report chapter on Design for explanation)
  void render(Shader * program) const;

};


#endif
