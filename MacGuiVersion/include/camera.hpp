/*
  A class representing a camera.
  It is a point prom which the scene is seen.
  Used both by rasterisation rendered and tracing renderers.
*/

#ifndef camera_hpp
#define camera_hpp

#include <cstdio>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

#include<someMath.hpp>
#include<ray.hpp>


class Camera
{
private:

  glm::vec3 startCamPos;
  glm::vec3 currCamPos;
  glm::vec3 lookingAt;
  glm::vec3 topPointingAt;

  glm::mat4 cameraTransf;
  glm::mat4 perspTransf;

  glm::vec3 view_dir;

  glm::vec3 xVector;
  glm::vec3 yVector;

  float xLength;
  float yLength;

  float lensRadius;

  glm::vec2 resolution;


  // The corners of the screen the scene is mapped to
  // Calculated every time the camera is updated
  // depending on the position and direction of it
  glm::vec3 world_lu_corner;
  glm::vec3 world_ru_corner;
  glm::vec3 world_ld_corner;
  glm::vec3 world_rd_corner;

  glm::vec3 base_lu_corner;
  glm::vec3 base_ru_corner;
  glm::vec3 base_ld_corner;
  glm::vec3 base_rd_corner;

  glm::vec3 user_shift  = glm::vec3(0.0f);
  glm::vec2 user_rotate = glm::vec2(0.0f);

  void generateCameraParameters();

public:

  Camera(const glm::vec3 & startPos, const glm::vec2 & res, float fov);

  // Getters -------------------------------------------------------------------
  [[nodiscard]] inline const glm::vec2 & getResolution() const { return resolution; }

  [[nodiscard]] inline glm::mat4 getToScreenTransf() const { return perspTransf * cameraTransf; } // Includes perspectiveTransformation

  [[nodiscard]] inline Ray getRayThrough(const glm::vec2 & p) const
  {
    return Ray(currCamPos, world_ld_corner + p.x * xVector + p.y * yVector - currCamPos);
  }

  [[nodiscard]] inline float getPixelArea() const
  {
    return xLength / resolution.x * yLength / resolution.y;
  }

  [[nodiscard]] inline const glm::vec3 & getDirection() const { return view_dir; }

  // Calculates which pixel does a point in the scene is inside
  // If it is not returns false
  bool getProjCoord(const glm::vec3 & pointCoord, glm::vec2 & pixelCoord) const;




  [[nodiscard]] float calcPDF(const glm::vec3 & dir) const;

  // Accurately speaking camera does not have BSDF
  // (it is a property of the object in the scene, while camera is an abstarction on point of view).
  // However, when calculating the light samples in Bidirectional Path Tracer, MLT and combinations
  // the camera is treated as a point of intersection of sorts. Hence, for the code to be more uniformal
  // camera is treated as a point of intersection with BSDF
  [[nodiscard]] glm::vec3 calcBSDF(const glm::vec3 & dir) const;

  void resetPos(const glm::vec3 & pos, const glm::vec2 & turn);


  // The functions allowing to change the camera possition in the preview
  // !!! Uses OpenGl and is not used in this version of the program
  // (see report chapter on Design for explanation)
  void moveHorBy(float shift);
  void moveFwdBy(float shift);
  void moveVerBy(float shift);

  void moveDirBy(float shift);
  void moveSideBy(float shift);

  void turnHorBy(float shift);
  void turnVerBy(float shift);
};


#endif
