/*
  A class implementing a general base for all the tracing algorithms.

  It contains all the functionality to form a light path and calculate a sample from it.
  The way the light path is constructed (from camera or from light source or
  by modifying the existing light path) is different for each tracing algorithm
  and is defined in the respective tracing classes. Those classes inherit from this.
*/

#ifndef baseTracer_hpp
#define baseTracer_hpp

#include <cstdio>
#include <string>
#include <mutex>

#include<object.hpp>
#include<scene.hpp>
#include<triangle.hpp>
#include<ray.hpp>
#include<material.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

#define _USE_MATH_DEFINES
#include <cmath>

#include <quad.hpp>
#include <sampler.hpp>
#include <intersectionPoint.hpp>
#include <lightPath.hpp>
#include <colorBuffer.hpp>
#include <map>


class BaseTracer
{
protected:

  ColorBuffer cameraBuffer; // The buffer to trace to
  std::map<int, ColorBuffer *> strategyBuffers;

  // Setting
  int numOfSamplesTotal  = 0;
  int numOfCurrentRun    = 0;
  int numOfCurRunSample  = 0;

  bool readyToFinish     = false;
  bool readyIntermediate = false;

  int samplesLowQuality  = 1;
  bool isLowImageReady   = false;

  int samplesGoodQuality = 2;
  bool isGoodImageReady  = false;

  int samplesBestQuality = 5;
  bool isBestImageReady  = false;


  std::map<int, std::string> strategyIDs;

  const Scene & scene;

  const Camera & camera;

  glm::ivec2 imageSize;

  Sampler sampler;

  std::mutex commonDataMutex;

  std::string tracerName = "Base";

  std::string pathCurr  = "../results";
  std::string pathSave  = "../results";
  std::string fileName  = "";

  // Save the current buffer in a file
  void saveCurrentImageToFile();

  // Simple addition functions. Add different thing to the buffer
  void addColorToCamera(const glm::vec2 & crd, const glm::vec3 & color);

  void addCountToCamera(float counter);

  void addCountToCamera(const glm::vec2 & crd, float counter);

  void addColorToStrategy(int id, const glm::vec2 & crd, const glm::vec3 & color);

  void addCountToStrategy(int id, float counter);

  void addCountToStrategy(int id, const glm::vec2 & crd, float counter);

  // Scans through the current scene and finds a closet point on the surface
  // which the given ray intersects
  IntersectionPoint scanThroughTriangles(Ray &ray);

  // For tracers to calculate the samples certain atributes of the points in the light paths
  // have to be known (BSDF in respect to both incoming and leaving rays, PDF of the incoming and leaving ray).
  // Since the calculations are very different for different types of the point
  // those functions calculate the attributes for them
  void createCameraPoint(IntersectionPoint & Point, Ray & rayFrom, const glm::vec2 & screenPos);

  void sampleLightPoint(IntersectionPoint & Point, Ray & rayFrom);

  void calcAmbientPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom);

  void calcDirLightPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom);

  void calcEmissionPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom);

  void calcDiffSpecPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom);

  void calcReflectionPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom);

  void calcRefractionPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom);

  void calcPathPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom);


  // Given a starting ray (from light source or camera depending on the specific tracer)
  // this function cretes a light path in the scene
  bool findPath(LightPath & path, Ray & rayTo, int depth);

  // For light paths that are a union of two subpaths from and to light sources
  // find a connection between them. If the endpoints of the subpaths have
  // an object in between them return false
  bool findConnection(LightPath & path);

  // Virual functions the actual tracers implement to calculate
  // the picture using the functions above
  virtual void prepareRender();

  virtual void updateSample();

  virtual void tracePixel(const glm::vec2 & crd) = 0;

public:

  BaseTracer(const Scene & sc, const Camera & cam);

  virtual ~BaseTracer();

  void initRender();

  bool updateRender();

  void generateImage();


  // Getters and setters -------------------------------------------------------
  [[nodiscard]] inline const std::string & getTracerName() const { return tracerName; }

  inline void setImagePath(const std::string & curr, const std::string & save, const std::string & name)
  {
    pathCurr  = curr + "/";
    pathSave  = save + "/";
    fileName  = name + " " + tracerName;
  }

  [[nodiscard]] inline const std::vector<GLubyte> & getImageRGB() const { return cameraBuffer.getImageRGB(); }

  [[nodiscard]] inline const std::vector<GLfloat> & getImageHDR() const { return cameraBuffer.getImageHDR(); }

  [[nodiscard]] inline const glm::ivec2 & getSizeInPixels() const { return imageSize; }

  [[nodiscard]] inline bool isReadyToFinish() const { return readyToFinish; }


  static bool saveStrategies;

  static bool renderForLow;
  static bool renderForGood;
  static bool renderForBest;
};


#endif
