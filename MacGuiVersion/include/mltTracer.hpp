/*
  A class implementing a Metropolise Light Transport algorithm.
*/

#ifndef mltTracer_hpp
#define mltTracer_hpp

#include <bdirTracer.hpp>


enum MutationType
{
  NewPath = 0,
  Pixel   = 1,
  Caustic = 2,
  CamRay  = 3,
};


class MLTTracer : public BDirTracer
{
protected:

  int maxNumOfSeeds;
  int qtyMutPerPixel;

  int seedsForAvrLum;
  float averageLuminance = 1.0f;


  std::vector<MutationType> mutations;
  std::discrete_distribution<int> mutDistr;


  void sampleInitialPath(LightPath & path, const glm::vec2 & crd);


  float mutateNewPath(LightPath & path);

  float mutatePixel(LightPath & path, float r1, float r2);

  float mutateCaustic(LightPath & path, float t1, float t2);

  float mutateCamRay(LightPath & path, float t1, float t2);

  float mutate(LightPath & path, MutationType & mutType);


  void computeAverageLuminance();

  void runMarkovChain(LightPath & path);


  void prepareRender() override;

  void updateSample() override;

public:

  MLTTracer(const Scene & sc, const Camera & cam);
};


#endif
