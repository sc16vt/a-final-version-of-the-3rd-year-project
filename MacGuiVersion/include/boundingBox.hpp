/*
  A class representing an abstract box an object is inside.
  It is used to check whether the ray intersects an object that lies inside this abstract box.
  Speeds up the intersection test sugnificantly.
*/

#ifndef boundingBox_hpp
#define boundingBox_hpp

#include <GL/glew.h>
#include <vector>

#include <ray.hpp>
#include <triangle.hpp>


class BoundingBox
{
  protected:

    glm::vec3 pointMin = glm::vec3( FLT_MAX);
    glm::vec3 pointMax = glm::vec3(-FLT_MAX);

    std::vector<int> triangles;

  public:

    // Check if the given ray intersects the bounding box
    [[nodiscard]] bool checkRay(const Ray & ray) const;

    // Check that the given point is inside the boundix box
    [[nodiscard]] bool checkPoint(const glm::vec3 & point) const;

    // Expand this box to include the thing given (triangle, box or some area)
    void expand(const Triangle & tri);
    void expand(const BoundingBox & box);
    void expand(const glm::vec3 & pmin, const glm::vec3 & pmax);

    void reset();

    void addIndex(int triIndex) { triangles.emplace_back(triIndex); }

    [[nodiscard]] const std::vector<int> & getIndices() const { return triangles; }


};



#endif
