// Fragment Shader

#version 330 core

struct LightSource
{
  vec3 position;
  vec3 power;
};

in vec3 thePosition;
in vec3 theColor;
in vec3 theNormal;

uniform LightSource lightSource;

out vec4 FragColor;

void main()
{
  vec3 normal = normalize(theNormal);
  vec3 lightToFragment = normalize(lightSource.position - thePosition);

  // diffuse color
  vec3 diffuseComp = max(dot(normal, lightToFragment), 0.0) * lightSource.power;
  FragColor = vec4(theColor+diffuseComp, 1.0f);
}
