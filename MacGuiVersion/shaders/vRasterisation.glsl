// Vertex Shader

#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 aNormal;

uniform mat4 modelToWorld;
uniform mat4 worldToScreen;

out vec3 thePosition;
out vec3 theColor;
out vec3 theNormal;

void main()
{
    gl_Position = worldToScreen * modelToWorld * vec4(aPos, 1.0);
    thePosition = vec3(modelToWorld * vec4(aPos, 1.0f));;
    theColor = aColor;
    theNormal = mat3(transpose(inverse(modelToWorld))) * aNormal;
}
