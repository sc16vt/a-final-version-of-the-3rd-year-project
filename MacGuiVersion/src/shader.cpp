//
//  shader.hpp
//  ANTTWEAKBAR_116_OGLCORE_GLFW
//  THIS CLASS IS TAKEN FROOM TUTORIALS MANY MANY MOON AGO AND IS NOT MADE BY ME. BE CAREFUL IF EVER REFERIMNG TO IT

//int modelToWorldLoc = glGetUniformLocation(program->ID, "modelToWorld");


#include <shader.hpp>

Shader::Shader(const char* vertex_file_path, const char* fragment_file_path)
{
  // Create the shaders
  GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
  GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

  // Read the Vertex Shader code from the file
  std::string VertexShaderCode;
  std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
  if(VertexShaderStream.is_open())
  {
      std::stringstream sstr;
      sstr << VertexShaderStream.rdbuf();
      VertexShaderCode = sstr.str();
      VertexShaderStream.close();
  }
  else
  {
      printf("Impossible to open %s.\n", vertex_file_path);
      getchar();
  }

  // Read the Fragment Shader code from the file
  std::string FragmentShaderCode;
  std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
  if(FragmentShaderStream.is_open())
  {
      std::stringstream sstr;
      sstr << FragmentShaderStream.rdbuf();
      FragmentShaderCode = sstr.str();
      FragmentShaderStream.close();
  }
  else
  {
      printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", fragment_file_path);
      getchar();
  }

  GLint Result = GL_FALSE;
  int InfoLogLength;


  // Compile Vertex Shader
  printf("Compiling shader : %s\n", vertex_file_path);
  char const * VertexSourcePointer = VertexShaderCode.c_str();
  glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
  glCompileShader(VertexShaderID);

  // Check Vertex Shader
  glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
  glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  if ( InfoLogLength > 0 )
  {
      char infoLog[1024];
      glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, infoLog);
      printf("%s\n", infoLog);
  }



  // Compile Fragment Shader
  printf("Compiling shader : %s\n", fragment_file_path);
  char const * FragmentSourcePointer = FragmentShaderCode.c_str();
  glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
  glCompileShader(FragmentShaderID);

  // Check Fragment Shader
  glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
  glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  if ( InfoLogLength > 0 ){
      char infoLog[1024];
      glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, infoLog);
      printf("%s\n", infoLog);
  }



  // Link the program
  printf("Linking program\n");
  ID = glCreateProgram();
  glAttachShader(ID, VertexShaderID);
  glAttachShader(ID, FragmentShaderID);
  glLinkProgram(ID);

  // Check the program
  glGetProgramiv(ID, GL_LINK_STATUS, &Result);
  glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  if ( InfoLogLength > 0 ){
      char infoLog[1024];
      glGetProgramInfoLog(ID, InfoLogLength, NULL, infoLog);
      printf("%s\n", infoLog);
  }


  glDetachShader(ID, VertexShaderID);
  glDetachShader(ID, FragmentShaderID);

  glDeleteShader(VertexShaderID);
  glDeleteShader(FragmentShaderID);
}

void Shader::use()
{
  glUseProgram(ID);
}

void Shader::setBool(const std::string &name, bool value) const
{
  glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value);
}
void Shader::setInt(const std::string &name, int value) const
{
  glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
}
void Shader::setFloat(const std::string &name, float value) const
{
  glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
}

void Shader::set4Float(const std::string &name, float x, float y, float z, float w) const
{
  glUniform4f(glGetUniformLocation(ID, name.c_str()), x, y, z, w);
}

//void Shader::set4Matrix(const std::string &name, glm::mat4 objToWorld) const
//{
//  glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, glm::value_ptr(objToWorld));
//}
