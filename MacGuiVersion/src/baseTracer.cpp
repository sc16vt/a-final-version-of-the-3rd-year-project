
#include <baseTracer.hpp>


bool BaseTracer::saveStrategies = false;

bool BaseTracer::renderForLow   = false;
bool BaseTracer::renderForGood  = false;
bool BaseTracer::renderForBest  = false;


BaseTracer::BaseTracer(const Scene & sc, const Camera & cam) :
  scene(sc), camera(cam), cameraBuffer("")
{
  cameraBuffer.setScaleFactor(1.0f);
}


BaseTracer::~BaseTracer()
{
  for (auto & buffer : strategyBuffers)
    delete buffer.second;
  strategyBuffers.clear();
}


void BaseTracer::initRender()
{
  sampler.resetRandomGenerator();

  // Init samples num and flags
  numOfSamplesTotal = 0;
  numOfCurrentRun   = 0;
  numOfCurRunSample = 0;

  isLowImageReady   = false;
  isGoodImageReady  = false;
  isBestImageReady  = false;

  readyToFinish     = false;
  readyIntermediate = false;

  // Get image size
  imageSize = glm::ivec2(camera.getResolution());

  // Create image buffers
  if (strategyBuffers.empty() && saveStrategies)
  {
    for (auto & id : strategyIDs)
      strategyBuffers.emplace(id.first, new ColorBuffer(id.second));
  }

  // Init image buffers
  for (auto & buffer : strategyBuffers)
    buffer.second->initBuffer(imageSize);

  cameraBuffer.initBuffer(imageSize);

  prepareRender();
}


bool BaseTracer::updateRender()
{
  bool wasIntermediateReady = true;

  commonDataMutex.lock();

  sampler.resetRandomGenerator();

  if (!readyIntermediate && !readyToFinish)
  {
    wasIntermediateReady = false;

    numOfSamplesTotal++;

    isLowImageReady  |= renderForLow  && numOfSamplesTotal == samplesLowQuality;
    isGoodImageReady |= renderForGood && numOfSamplesTotal == samplesGoodQuality;
    isBestImageReady |= renderForBest && numOfSamplesTotal == samplesBestQuality;

    readyIntermediate |= isBestImageReady || isGoodImageReady || isLowImageReady;

    if (renderForBest)
      readyToFinish |= isBestImageReady;
    else
    if (renderForGood)
      readyToFinish |= isGoodImageReady;
    else
    if (renderForLow)
      readyToFinish |= isLowImageReady;
  }

  commonDataMutex.unlock();

  if (!wasIntermediateReady) updateSample();

  return readyIntermediate;
}


void BaseTracer::prepareRender()
{
}


void BaseTracer::updateSample()
{
  for(int y = 0; y < imageSize.y; y++)
    for(int x = 0; x < imageSize.x; x++)
      tracePixel(sampler.pixelPointSample(glm::vec2(x, y)));
}


void BaseTracer::generateImage()
{
  numOfCurrentRun++;
  numOfCurRunSample = numOfSamplesTotal;

  for (auto & buffer : strategyBuffers)
    buffer.second->generateImage();

  cameraBuffer.generateImage();

  saveCurrentImageToFile();

  std::cout << "  " << tracerName
    << " runs: "    << numOfCurrentRun
    << " samples: " << numOfSamplesTotal
    << std::endl;
}


void BaseTracer::saveCurrentImageToFile()
{
  // Save current render result
  cameraBuffer.saveToFile(pathCurr, fileName, true, true);

  // Save current strategies results
  if (!strategyBuffers.empty() && saveStrategies)
  {
    for (auto & buffer : strategyBuffers)
      buffer.second->saveToFile(pathCurr + "/strategies", "", true, false);
  }

  // Save ready images
  if (isLowImageReady)
  {
    std::string name = fileName + " low-" + std::to_string(numOfSamplesTotal) + "s";
    cameraBuffer.saveToFile(pathSave, name, true, true);
    isLowImageReady = false;
  }

  if (isGoodImageReady)
  {
    std::string name = fileName + " good-" + std::to_string(numOfSamplesTotal) + "s";
    cameraBuffer.saveToFile(pathSave, name, true, true);
    isGoodImageReady = false;
  }

  if (isBestImageReady)
  {
    std::string name = fileName + " best-" + std::to_string(numOfSamplesTotal) + "s";
    cameraBuffer.saveToFile(pathSave, name, true, true);
    isBestImageReady = false;
  }

  readyIntermediate = false;
}


void BaseTracer::addColorToCamera(const glm::vec2 & crd, const glm::vec3 & color)
{
  cameraBuffer.addFiltered(crd, glm::vec4(color, 0.0f));
}


void BaseTracer::addCountToCamera(float counter)
{
  cameraBuffer.addToBuffer(glm::vec4(0.0f, 0.0f, 0.0f, counter));
}


void BaseTracer::addCountToCamera(const glm::vec2 & crd, float counter)
{
  cameraBuffer.addToPixel(crd, glm::vec4(0.0f, 0.0f, 0.0f, counter));
}


void BaseTracer::addColorToStrategy(int id, const glm::vec2 & crd, const glm::vec3 & color)
{
  if (!saveStrategies) return;

  auto buffer = strategyBuffers.find(id);
  if (buffer != strategyBuffers.end())
    buffer->second->addToPixel(crd, glm::vec4(color, 0.0f));
}


void BaseTracer::addCountToStrategy(int id, float counter)
{
  if (!saveStrategies) return;

  auto buffer = strategyBuffers.find(id);
  if (buffer != strategyBuffers.end())
    buffer->second->addToBuffer(glm::vec4(glm::vec3(0.0f), counter));
}


void BaseTracer::addCountToStrategy(int id, const glm::vec2 & crd, float counter)
{
  if (!saveStrategies) return;

  auto buffer = strategyBuffers.find(id);
  if (buffer != strategyBuffers.end())
    buffer->second->addToPixel(crd, glm::vec4(glm::vec3(0.0f), counter));
}


IntersectionPoint BaseTracer::scanThroughTriangles(Ray & ray)
{
  const Triangle * triangle = scene.traceRay(ray);
  if (nullptr != triangle)
    return ray.getIntersection();

  // Create point outside of scene bounding
  return IntersectionPoint(Ambient,
          ray.getOrigin() + scene.getBoundingSphere().getRadius() * ray.getDirection());
}


void BaseTracer::createCameraPoint(IntersectionPoint & Point, Ray & rayFrom, const glm::vec2 & screenPos)
{
  // Create a ray from camera through sample coordinates
  rayFrom = camera.getRayThrough(screenPos / glm::vec2(imageSize));

  // Create a view point
  Point = IntersectionPoint(rayFrom.getOrigin());

  glm::vec3 BSDF = camera.calcBSDF(rayFrom.getDirection());
  float PDF = camera.calcPDF(rayFrom.getDirection());

  Point.setSampledData(CamPoint, BSDF, BSDF, PDF, PDF);
}


void BaseTracer::sampleLightPoint(IntersectionPoint & Point, Ray & rayFrom)
{
  // Get a random point on it
  Point = sampler.lightPointSample(scene);

  // Create a random ray from it
  rayFrom = Ray(Point, sampler.lightRaySample(Point, scene));
  Ray rayTo = Ray(Point.getPoint(), -rayFrom.getDirection());

  calcPathPoint(Reverse, Point, rayTo, rayFrom);
}


void BaseTracer::calcAmbientPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom)
{
  // Compute BSDF and PDF
  glm::vec3 BSDF = scene.hasDirLight() ? scene.getAmbientColor() : glm::vec3(0.0f);

  // Set point parameters
  Point.setSampledData(Ambient, BSDF, BSDF, 1.0f, 1.0f);
}


void BaseTracer::calcDirLightPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom)
{
  glm::vec3 lightDir = scene.getDirLightDir();

  float cosOut = glm::dot(lightDir, rayFrom.getDirection());
  float cosIn  = glm::dot(lightDir, -rayTo.getDirection());
  float cosLit = (dir == Forward) ? cosIn : cosOut;

  // Make ray black if we doesn't meet conditions
  if (cosIn < 1.0f - std::numeric_limits<float>::epsilon())
  {
    Point.setSampledData(DirLight, glm::vec3(0.0f), glm::vec3(0.0f), 1.0f, 1.0f);
    return;
  }

  // Compute BSDF and PDF
  glm::vec3 BSDF = scene.hasDirLight() ? fabsf(cosIn) * scene.getDirLightColor() : glm::vec3(0.0f);
  float rayPDF   = Point.isUnset() ? 1.0f : sampler.lightRaySamplePDF(Point, fabsf(cosIn));
  float pointPDF = Point.isUnset() ? 1.0f : sampler.lightPointSamplePDF(Point, scene);

  // Set point parameters
  Point.setSampledData(DirLight, BSDF, BSDF, rayPDF, pointPDF);
}


void BaseTracer::calcEmissionPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom)
{
  float cosIn  = glm::dot(Point.getNormal(), -rayTo.getDirection());
  float cosOut = glm::dot(Point.getNormal(), rayFrom.getDirection());
  float cosLit = (dir == Forward) ? cosIn : cosOut;

  float beamRadius = Point.getMaterial().getBeamRadius();
  float cosMin = sqrtf(1.0f - beamRadius * beamRadius);

  // Make ray black if we doesn't meet conditions
  if (cosLit < 0.0f || fabsf(cosLit) <= cosMin)
  {
    Point.setSampledData(Emission, glm::vec3(0.0f), glm::vec3(0.0f), 1.0f, 1.0f);
    return;
  }

  // Compute BSDF and PDF
  glm::vec3 BSDF = (Point.getTriangle()->getRadiance() / Point.getTriangle()->getObjArea()) * Point.getSurfaceColor();
  float rayPDF   = sampler.lightRaySamplePDF(Point, fabsf(cosLit));
  float pointPDF = Point.getMaterial().getEmissionPDF() * sampler.lightPointSamplePDF(Point, scene);

  // Set point parameters
  Point.setSampledData(Emission, BSDF, BSDF, rayPDF, pointPDF);
}


void BaseTracer::calcDiffSpecPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom)
{
  // Find a point on hemisphere to cast through (sampler)
  // Cast a ray through the point of intersection and a calculated sample
  if (Point.isUnset())
    rayFrom = Ray(Point, sampler.diffuseSample(Point));

  // Compute diffuse
  float cosOut = glm::dot(Point.getNormal(), rayFrom.getDirection());
  float cosIn  = glm::dot(Point.getNormal(), -rayTo.getDirection());
  float cosDiff = (dir == Forward) ? cosOut : cosIn;

  // Make ray black if we doesn't meet conditions
  if (cosIn < 0.0f)
  {
    Point.setSampledData(DiffSpec, glm::vec3(0.0f), glm::vec3(0.0f), 1.0f, 1.0f);
    return;
  }

  // Compute specular
  glm::vec3 reflectionDir = 2.0f * cosIn * Point.getNormal() + rayTo.getDirection();
  float cosSpec = (cosDiff >= 0.0f) ? std::max(0.0f, glm::dot(reflectionDir, rayFrom.getDirection())) : 0.0f;
  float powSpec = Point.getMaterial().getSpecularPower();

  // Compute BSDF and PDF
  glm::vec3 BSDF =
          Point.getMaterial().getDiffSpecPDF() *
          (1.0f - Point.getMaterial().getSpecularCoef()) * std::max(cosDiff, 0.0f) * SM_INV_PI * Point.getSurfaceColor()
                + Point.getMaterial().getSpecularCoef() * powf(cosSpec, powSpec) * SM_INV_PI;

  float rayPDF =
          Point.getMaterial().getDiffSpecPDF() *
          (1.f - Point.getMaterial().getSpecularCoef()) * sampler.diffuseSamplePDF(std::max(cosIn, 0.0f))
               + Point.getMaterial().getSpecularCoef() * sampler.specularSamplePDF(cosSpec, powSpec);

  // Set point parameters
  Point.setSampledData(DiffSpec, BSDF, BSDF, rayPDF, rayPDF);
}


void BaseTracer::calcReflectionPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom)
{
  // Calculate the direction of reflection
  float cosIn = glm::dot(Point.getNormal(), -rayTo.getDirection());

  // Make ray black if we doesn't meet conditions
  if (cosIn < 0.0f)
  {
    Point.setSampledData(Reflection, glm::vec3(0.0f), glm::vec3(0.0f), 1.0f, 1.0f);
    return;
  }

  // Cast the ray
  if (Point.isUnset()) rayFrom = Ray(Point, glm::normalize(2.0f * cosIn * Point.getNormal() + rayTo.getDirection()));

  // Compute BSDF and PDF
  // In theory here should be Delta function but we are handling this situation in the LightPath
  // (10.3.5) https://graphics.stanford.edu/courses/cs348b-03/papers/veach-chapter10.pdf
  glm::vec3 BSDF = Point.getMaterial().getReflectionPDF() * glm::vec3(1.0f);
  float rayPDF   = Point.getMaterial().getReflectionPDF();

  // Set point parameters
  Point.setSampledData(Reflection, BSDF, BSDF, rayPDF, rayPDF);
}


void BaseTracer::calcRefractionPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom)
{
  // The material coefficient
  float coefIn  = (dir == Forward ? Point.getPrevRefraction() : Point.getNextRefraction());
  float coefOut = (dir == Forward ? Point.getNextRefraction() : Point.getPrevRefraction());

  // Depending on whether the light is coming from outside or inside the object
  // the normal of the triangle or a flipped normal has to be used to calculate the ray
  float cosIn = glm::dot(rayTo.getDirection(), Point.getNormal());

  // To check whether the ray is from outside or from inside use dot product between normal and incident reay direction
  // If less than 0 -> from outside
  RefractionDir transmitDir = (cosIn <= 0.0f ? Inside : Outside);

  // Set refraction coeff from material depends on direction
  if (transmitDir == Inside)
    coefOut = Point.getMaterial().getRefractionCoef();
  else
    coefIn  = Point.getMaterial().getRefractionCoef();

  cosIn = fabsf(cosIn);

  float partReflected  = Point.getMaterial().fresnel(cosIn, coefIn, coefOut);
  float partRefracted  = 1.0f - partReflected;

  // Sample type of ray reflection or refraction
  if (Point.isUnset())
    Point.setRefractionSample(sampler.refractionSample());

  if (Point.getRefractionSample() < partReflected)
    // reflection
  {
    // Cast the ray
    if (Point.isUnset())
      rayFrom = Ray(Point, glm::normalize(2.0f * fabsf(cosIn) * Point.getNormal() + rayTo.getDirection()));

    // Set refraction coef for next point in path, in case of reflection Out sould be the same as In
    coefOut = coefIn;

    // Compute BSDF and PDF
    // In theory here should be Delta function but it is handled in the LightPath
    glm::vec3 BSDF = Point.getMaterial().getRefractionPDF() * partReflected * glm::vec3(1.0f);
    float rayPDF   = Point.getMaterial().getRefractionPDF() * partReflected;

    // Set point parameters
    Point.setSampledData(Refraction, BSDF, BSDF, rayPDF, rayPDF);
  }
  else
    // refraction
  {
    const float coef2InOut = coefOut * coefOut / (coefIn * coefIn);
    const float prevCoef2 = (dir == Forward) ? coef2InOut : 1.0f / coef2InOut;
    const float nextCoef2 = (dir == Reverse) ? coef2InOut : 1.0f / coef2InOut;

    // Compute cos of angle between normal and incident ray, and between - normal and ray to be found
    float cosOut = sqrtf(1.0f - (1.0f - cosIn * cosIn) / coef2InOut);

    // Compute direction of the ray using Snell's law
    glm::vec3 refractionDir = glm::normalize(rayTo.getDirection() -
            (coefOut * cosOut - coefIn * cosIn) * (transmitDir == Inside ? 1.0f : -1.0f) * Point.getNormal());

    // Cast a ray with that direction
    if (Point.isUnset()) rayFrom = Ray(Point, refractionDir);

    // Compute BSDF with surface color and probability
    // In theory here should be Delta function but it is handled in the LightPath
    glm::vec3 BSDF = Point.getMaterial().getRefractionPDF() * partRefracted * Point.getSurfaceColor();
    float rayPDF   = Point.getMaterial().getRefractionPDF() * partRefracted;

    // Set point parameters
    Point.setSampledData(Refraction, prevCoef2 * BSDF, nextCoef2 * BSDF, rayPDF, rayPDF);
    Point.setRefractionDir(transmitDir);
  }

  // Set refraction coef for next point only in case of transfer
  if (dir == Forward)
  {
    Point.setPrevRefraction(coefIn);
    Point.setNextRefraction(coefOut);
  }
  else
  {
    Point.setPrevRefraction(coefOut);
    Point.setNextRefraction(coefIn);
  }
}


void BaseTracer::calcPathPoint(PathDir dir, IntersectionPoint & Point, const Ray & rayTo, Ray & rayFrom)
{
  // Randomly choose material type or use the last one from previous find path
  PointType pointType = Point.isUnset() ? sampler.materialSample(Point.getMaterial().getDistribution()) : Point.getType();

  switch (pointType)
  {
    case Ambient:    calcAmbientPoint(dir, Point, rayTo, rayFrom);    break;
    case DirLight:   calcDirLightPoint(dir, Point, rayTo, rayFrom);   break;
    case Emission:   calcEmissionPoint(dir, Point, rayTo, rayFrom);   break;
    case DiffSpec:   calcDiffSpecPoint(dir, Point, rayTo, rayFrom);   break;
    case Reflection: calcReflectionPoint(dir, Point, rayTo, rayFrom); break;
    case Refraction: calcRefractionPoint(dir, Point, rayTo, rayFrom); break;

    case CamPoint:
    case Unknown:    Point.setSampledData(pointType); break;
  }
}



bool BaseTracer::findPath(LightPath & path, Ray & rayTo, int depth)
{
  // If depth is exactly as asked return true;
  if (depth <= 0) { return true; }

  // Find the point of intersection with closest triangle, return that point
  IntersectionPoint & pointX = path.getLastInsertPoint();
  IntersectionPoint   pointY = scanThroughTriangles(rayTo);

  // Check if hit the same point but on the other triangle
  if (rayTo.getDistance() == 0.0f) return false;

  // Set refraction coefficients
  if (path.getPathDir() == Forward)
  {
    pointY.setPrevRefraction(pointX.getNextRefraction());
    float coefOut = path.findPrevRefraction();
    if (coefOut < 0.0f) return false;
    pointY.setNextRefraction(coefOut);
  }
  else
  {
    pointY.setNextRefraction(pointX.getPrevRefraction());
    float coefOut = path.findNextRefraction();
    if (coefOut < 0.0f) return false;
    pointY.setPrevRefraction(coefOut);
  }

  // Compute and set distribution params to point
  Ray rayFrom;
  calcPathPoint(path.getPathDir(), pointY, rayTo, rayFrom);


  // Set refraction coefficients
  if (pointY.getType() != Refraction)
  {
    if (path.getPathDir() == Forward)
      pointY.setNextRefraction(pointY.getPrevRefraction());
    else
      pointY.setPrevRefraction(pointY.getNextRefraction());
  }

  // Skip if the point is black
  if (pointY.isBlack())
  {
      return false;
  }

  // Compute geometry coefficients
  // We are tracing from x to this point y: x->y
  float cosX = pointX.isSurface() ? glm::dot(rayTo.getDirection(), pointX.getNormal()) : 1.0f;
  float cosY = pointY.isSurface() ? glm::dot(rayTo.getDirection(), pointY.getNormal()) : 1.0f;

  // Compute G for this point
  float squareDist = pointY.isLi() ? 1.0f : rayTo.getDistance() * rayTo.getDistance();
  float G = fabsf(cosX) * fabsf(cosY) / squareDist;

  if (path.getPathDir() == Forward)
  {
    // Py = Pr * cos(y) / d^2 = Pr * G / cos(x)
    pointY.setPrevCos(fabsf(cosX));

    // In case of back tracing
    // Px = Pr * cos(x) / d^2 = Pr * G / cos(y)
    pointX.setNextCos(fabsf(cosY));

    pointY.setPrevG(G);
    pointX.setNextG(G);

    pointX.setNextIsDelta(pointY.isDelta());
    pointY.setPrevIsDelta(pointX.isDelta());

    pointY.swapPrevPDF(pointX);
  }
  else
  {
    // Py = Pr * cos(y) / d^2 = Pr * G / cos(x)
    pointY.setNextCos(fabsf(cosX));

    // In case of back tracing
    // Px = Pr * cos(x) / d^2 = Pr * G / cos(y)
    pointX.setPrevCos(fabsf(cosY));

    pointY.setNextG(G);
    pointX.setPrevG(G);

    pointX.setPrevIsDelta(pointY.isDelta());
    pointY.setNextIsDelta(pointX.isDelta());

    pointY.swapNextPDF(pointX);
  }

  // Store found point in the path
  path.insertPoint(pointY);

  // Stop search if we have found end point (Light source for example)
  if (pointY.isEnd())
  {
    return false;
  }

  // Call itself find an intersection point between the just cast ray and some triangle
  return findPath(path, rayFrom, depth - 1);
}


bool BaseTracer::findConnection(LightPath & path)
{
  auto prevP = path.begin() + path.getConnectPos();
  auto nextP = path.begin() + path.getConnectPos() + 1;

  // Skip if we are trying to connect Delta surfaces because the probability to connect them is 0
  if (prevP->isDelta() || nextP->isDelta()) return false;

  // If from point on surface check if we can skip
  if (prevP->isSurface() && nextP->isSurface())
  {
    // Check if points on the same plane and skip this situation. It's cheaper than tracing ray
    if (prevP->getTriangle()->isSamePlane(nextP->getTriangle()))
    {
      prevP->setNextG(0.0f);
      nextP->setPrevG(0.0f);
      return false;
    }
  }

  glm::vec3 lineFromTo = nextP->getPoint() - prevP->getPoint();
  float lineFromToLenth = glm::length(lineFromTo);

  // Compute ray between points
  Ray rayLineFwd = Ray(*prevP, lineFromTo);
  rayLineFwd.setIntersection(nextP->getTriangle(), nextP->getPoint(), lineFromToLenth);

  Ray rayLineRev = Ray(*nextP, -lineFromTo);
  rayLineRev.setIntersection(prevP->getTriangle(), prevP->getPoint(), lineFromToLenth);

  // Check if no obstacles between points (we dont't need to check both rays because we only need to check for obstacles)
  const Triangle * triangle = scene.traceRay(rayLineFwd);
  if (nullptr != triangle && nextP->getTriangle() != triangle)
  {
    prevP->setNextG(0.0f);
    nextP->setPrevG(0.0f);
    return false;
  }

  // Swap back PDFs to remember them because calcPathPoint will overwrite it
  if (!prevP->isEnd()) prevP->swapPrevPDF(*(prevP - 1));
  if (!nextP->isEnd()) nextP->swapNextPDF(*(nextP + 1));


  // Update from point render parameters
  Ray rayBeforeFrom(*(prevP - 1), prevP->isEnd() ? rayLineFwd.getDirection() : prevP->getPoint() - (prevP - 1)->getPoint());
  calcPathPoint(Forward, *prevP, rayBeforeFrom, rayLineFwd);

  // Skip if the point is black
  if (prevP->isBlack()) return false;


  // Update to point render parameters
  Ray rayAfterTo(*nextP, nextP->isEnd() ? rayLineRev.getDirection() : nextP->getPoint() - (nextP + 1)->getPoint());
  calcPathPoint(Reverse, *nextP, rayAfterTo, rayLineRev);

  // Skip if the point is black
  if (nextP->isBlack()) return false;


  // Compute cosines between line and surfaces
  float cosFrom = prevP->isSurface() ? glm::dot(rayLineFwd.getDirection(), prevP->getNormal()) : 1.0f;
  float cosTo   = nextP->isSurface() ? glm::dot(rayLineRev.getDirection(), nextP->getNormal()) : 1.0f;

  // Compute G between this two points. Both subpaths have their own Gs computed from different sources so we need the only this one
  float squareDist = rayLineFwd.getDistance() * rayLineRev.getDistance();
  float crossG = nextP->isLi() ? 1.0f : fabsf(cosFrom) * fabsf(cosTo) / squareDist;

  prevP->setNextG(crossG);
  prevP->setNextCos(fabsf(cosTo));

  nextP->setPrevG(crossG);
  nextP->setPrevCos(fabsf(cosFrom));

  // Swap PDFs again to put them at right place
  if (!prevP->isEnd()) prevP->swapPrevPDF(*(prevP - 1));
  if (!nextP->isEnd()) nextP->swapNextPDF(*(nextP + 1));

  prevP->swapNextPDF(*nextP);

  prevP->setNextIsDelta(nextP->isDelta());
  nextP->setPrevIsDelta(prevP->isDelta());

  return true;
}
