
#include <object.hpp>

bool Object::isOpenGLEnabled = false;


Object::Object(const glm::vec3 & translate, float rotateAngle, const glm::vec3 & rotateLine, const glm::vec3 & scale, Camera & cam) : camera(cam)
{
  objToWorld = glm::mat4(1.0f);

  objToWorld = glm::translate(objToWorld, translate);
  objToWorld = glm::rotate(objToWorld, rotateAngle, rotateLine);
  objScale   = scale;

  if (isOpenGLEnabled)
  {
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &CBO);
    glGenBuffers(1, &NBO);
    glGenBuffers(1, &EBO);
  }
}


Object::Object(const glm::vec3 & translate, const glm::vec3 & scale, Camera & cam) : camera(cam)
{
  objToWorld = glm::mat4(1.0f);

  objToWorld = glm::translate(objToWorld, translate);
  objScale   = scale;

  if (isOpenGLEnabled)
  {
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &CBO);
    glGenBuffers(1, &NBO);
    glGenBuffers(1, &EBO);
  }
}

Object::~Object()
{
  if (isOpenGLEnabled)
  {
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &CBO);
    glDeleteBuffers(1, &NBO);
    glDeleteBuffers(1, &EBO);
  }
}


Object & Object::generateCube(const Material & mat)
{
  material = mat;

  vertices.reserve(8 * 3 * 3);
  normals.reserve(8 * 3 * 3);
  colors.reserve(8 * 3 * 3);
  texcoords.reserve(8 * 3 * 3);

  for (auto i = 0; i < 3; i++)
    for (auto x = -1; x <= 1; x += 2)
      for (auto z = -1; z <= 1; z += 2)
        for (auto y = -1; y <= 1; y += 2)
        {
          vertices.emplace_back(-x * 0.5f      * objScale.x);
          vertices.emplace_back( y * 0.5f * -z * objScale.y);
          vertices.emplace_back( z * 0.5f      * objScale.z);

          colors.emplace_back(0.0f);
          colors.emplace_back(0.0f);
          colors.emplace_back(0.0f);

          texcoords.emplace_back(0.0f);
          texcoords.emplace_back(0.0f);
        }

  for (auto x = -1; x <= 1; x += 2)
    for (auto z = -1; z <= 1; z += 2)
      for (auto y = -1; y <= 1; y += 2)
      {
        normals.emplace_back(0.0f);
        normals.emplace_back(0.0f);
        normals.emplace_back(z);
      }

  for (auto x = -1; x <= 1; x += 2)
    for (auto z = -1; z <= 1; z += 2)
      for (auto y = -1; y <= 1; y += 2)
      {
        normals.emplace_back(-x);
        normals.emplace_back(0.0f);
        normals.emplace_back(0.0f);
      }

  for (auto x = -1; x <= 1; x += 2)
    for (auto z = -1; z <= 1; z += 2)
      for (auto y = -1; y <= 1; y += 2)
      {
        normals.emplace_back(0.0f);
        normals.emplace_back(y * -z);
        normals.emplace_back(0.0f);
      }

  GLuint ofs1 = 8;
  GLuint ofs2 = 8 * 2;

  unsigned int ind[] =
  {
    6, 7, 3,  // 0 triangle green
    6, 3, 2,  // 2 triangle green

    1, 0, 4,  // 3 triangle green
    1, 4, 5,  // 4 triangle green

    2+ofs1, 3+ofs1, 0+ofs1,  // 5 triangle red
    2+ofs1, 0+ofs1, 1+ofs1,  // 6 triangle red

    5+ofs1, 4+ofs1, 7+ofs1,  // 7 triangle red
    5+ofs1, 7+ofs1, 6+ofs1,  // 8 triangle red

    7+ofs2, 4+ofs2, 0+ofs2,  // 9 triangle blue
    7+ofs2, 0+ofs2, 3+ofs2,  // 10 triangle blue

    5+ofs2, 6+ofs2, 2+ofs2,  // 11 triangle blue
    5+ofs2, 2+ofs2, 1+ofs2,  // 12 triangle blue
  };

  std::copy(std::begin(ind), std::end(ind), std::back_inserter(indices));

  return *this;
}


Object & Object::generateSphere(const Material & mat, int samples, bool smooth)
{
  material = mat;
  isSmooth = smooth;

  int segmentsH = samples < 16 ? 4 : samples / 6;
  int segmentsV = samples < 16 ? 4 : samples / 6;

  const int sampPerSegH = samples / segmentsH;
  const int samplesH = segmentsH * sampPerSegH;

  const int sampPerSegV = samples / segmentsV;
  const int samplesV = segmentsV * sampPerSegV;

  const int shiftV = samplesV + 1;

  int toReserve = (samplesH + 1) * (samplesV + 1);
  vertices.reserve(toReserve * 3);
  normals.reserve(toReserve * 3);
  colors.reserve(toReserve * 3);
  texcoords.reserve(toReserve * 2);

  for (auto h = 0; h <= samplesH; h++)
    for (auto v = 0; v <= samplesV; v++)
    {
      const float H = (float)h / (float)samplesH;
      const float V = (float)v / (float)samplesV;

      const float A = SM_PI * H;
      const float B = 2.0f * SM_PI * V;

      glm::vec3 point(sinf(A) * cosf(B), sinf(A) * sinf(B), cosf(A));
      glm::vec3 norm = glm::normalize(point);

      vertices.emplace_back(0.5f * point.x * objScale.x);
      vertices.emplace_back(0.5f * point.y * objScale.y);
      vertices.emplace_back(0.5f * point.z * objScale.z);

      colors.emplace_back(0.0f);
      colors.emplace_back(0.0f);
      colors.emplace_back(0.0f);

      texcoords.emplace_back(0.0f);
      texcoords.emplace_back(0.0f);

      normals.emplace_back(norm.x);
      normals.emplace_back(norm.y);
      normals.emplace_back(norm.z);
    }


  indices.reserve((samplesH - 1) * (samplesV - 1) * 2 * 3);
  innerBoxes.resize(segmentsH * segmentsV);

  int t = 0;
  for (auto h = 0; h < samplesH; h++)
    for (auto v = 0; v < samplesV; v++)
    {
      int base = h * shiftV + v;
      int bIdx = h / sampPerSegH * segmentsV + v / sampPerSegV;

      indices.emplace_back(base);
      indices.emplace_back(base + shiftV);
      indices.emplace_back(base + shiftV + 1);
      innerBoxes[bIdx].addIndex(t++);

      indices.emplace_back(base);
      indices.emplace_back(base + shiftV + 1);
      indices.emplace_back(base + 1);
      innerBoxes[bIdx].addIndex(t++);
    }

  return *this;
}


Object & Object::generateWaveSurface(const Material & mat, int samples, float freq, float ampl, const glm::vec2 & dot)
{
  material = mat;

  int segmentsH = samples < 16 ? 4 : samples / 8;
  int segmentsV = samples < 16 ? 4 : samples / 8;

  const int sampPerSegH = samples / segmentsH;
  const int samplesH = segmentsH * sampPerSegH;

  const int sampPerSegV = samples / segmentsV;
  const int samplesV = segmentsV * sampPerSegV;

  const int shiftV = samplesV + 1;

  int toReserve = (samplesH + 1) * (samplesV + 1);
  vertices.reserve(toReserve * 3);
  normals.reserve(toReserve * 3);
  colors.reserve(toReserve * 3);
  texcoords.reserve(toReserve * 2);

  for (auto x = -samplesH; x <= samplesH; x += 2)
    for (auto y = -samplesV; y <= samplesV; y += 2)
    {
      float radius = 3.0f / (float)samples * glm::length(glm::vec2(x, y) - (float)samples * dot);
      float A = SM_PI * radius * freq;

      vertices.emplace_back(x * 0.5f / (float)samplesH * objScale.x);
      vertices.emplace_back(y * 0.5f / (float)samplesV * objScale.y);
      vertices.emplace_back(sinf(A) * ampl / std::max(powf(radius, 0.2f), 1.0f) * objScale.z);

      colors.emplace_back(0.0f);
      colors.emplace_back(0.0f);
      colors.emplace_back(0.0f);

      texcoords.emplace_back(0.0f);
      texcoords.emplace_back(0.0f);

      normals.emplace_back(0.0f);
      normals.emplace_back(0.0f);
      normals.emplace_back(0.0f);
    }


  indices.reserve((samplesH - 1) * (samplesV - 1) * 2 * 3);
  innerBoxes.resize(segmentsH * segmentsV);

  int t = 0;
  for (auto h = 0; h < samplesH; h++)
    for (auto v = 0; v < samplesV; v++)
    {
      int base = h * shiftV + v;
      int bIdx = h / sampPerSegH * segmentsV + v / sampPerSegV;

      indices.emplace_back(base);
      indices.emplace_back(base + shiftV);
      indices.emplace_back(base + shiftV + 1);
      innerBoxes[bIdx].addIndex(t++);

      {
        const unsigned int i1 = (base) * 3;
        const unsigned int i2 = (base + shiftV) * 3;
        const unsigned int i3 = (base + shiftV + 1) * 3;

        glm::vec3 pA(vertices[i1+0], vertices[i1+1], vertices[i1+2]);
        glm::vec3 pB(vertices[i2+0], vertices[i2+1], vertices[i2+2]);
        glm::vec3 pC(vertices[i3+0], vertices[i3+1], vertices[i3+2]);

        glm::vec3 norm = glm::normalize(glm::cross((pB-pA), (pC-pA)));

        normals[i1+0] += norm.x; normals[i1+1] += norm.y; normals[i1+2] += norm.z;
        normals[i2+0] += norm.x; normals[i2+1] += norm.y; normals[i2+2] += norm.z;
        normals[i3+0] += norm.x; normals[i3+1] += norm.y; normals[i3+2] += norm.z;
      }

      indices.emplace_back(base);
      indices.emplace_back(base + shiftV + 1);
      indices.emplace_back(base + 1);
      innerBoxes[bIdx].addIndex(t++);

      {
        const unsigned int i1 = (base) * 3;
        const unsigned int i2 = (base + shiftV + 1) * 3;
        const unsigned int i3 = (base + 1) * 3;

        glm::vec3 pA(vertices[i1+0], vertices[i1+1], vertices[i1+2]);
        glm::vec3 pB(vertices[i2+0], vertices[i2+1], vertices[i2+2]);
        glm::vec3 pC(vertices[i3+0], vertices[i3+1], vertices[i3+2]);

        glm::vec3 norm = glm::normalize(glm::cross((pB-pA), (pC-pA)));

        normals[i1+0] += norm.x; normals[i1+1] += norm.y; normals[i1+2] += norm.z;
        normals[i2+0] += norm.x; normals[i2+1] += norm.y; normals[i2+2] += norm.z;
        normals[i3+0] += norm.x; normals[i3+1] += norm.y; normals[i3+2] += norm.z;
      }
    }

  for (int i = 0; i < normals.size(); i += 3)
  {
    glm::vec3 norm = glm::normalize(glm::vec3(normals[i+0], normals[i+1], normals[i+2]));
    normals[i+0] = norm.x;
    normals[i+1] = norm.y;
    normals[i+2] = norm.z;
  }

  isWireframe = true;

  return *this;
}


Object & Object::generatePlaneXY(const Material & mat)
{
  material  = mat;

  vertices.reserve(8 * 3 * 3);
  normals.reserve(8 * 3 * 3);
  colors.reserve(8 * 3 * 3);
  texcoords.reserve(8 * 3 * 3);

  for (auto x = -1; x <= 1; x += 2)
    for (auto y = -1; y <= 1; y += 2)
    {
      vertices.emplace_back(x * 0.5f * objScale.x);
      vertices.emplace_back(y * 0.5f * objScale.y);
      vertices.emplace_back(0.0f);

      colors.emplace_back(0.0f);
      colors.emplace_back(0.0f);
      colors.emplace_back(0.0f);

      texcoords.emplace_back(0.0f);
      texcoords.emplace_back(0.0f);

      normals.emplace_back(0.0f);
      normals.emplace_back(0.0f);
      normals.emplace_back(1.0f);
    }

  unsigned int ind[] =
  {
    0, 2, 3,
    0, 3, 1,
  };
  std::copy(std::begin(ind), std::end(ind), std::back_inserter(indices));

  return *this;
}


Object & Object::generatePlaneYZ(const Material & mat)
{
  material  = mat;

  vertices.reserve(8 * 3 * 3);
  normals.reserve(8 * 3 * 3);
  colors.reserve(8 * 3 * 3);
  texcoords.reserve(8 * 3 * 3);

  for (auto z = -1; z <= 1; z += 2)
    for (auto y = -1; y <= 1; y += 2)
    {
      vertices.emplace_back(0.0f);
      vertices.emplace_back(y * 0.5f * objScale.y);
      vertices.emplace_back(z * 0.5f * objScale.z);

      colors.emplace_back(0.0f);
      colors.emplace_back(0.0f);
      colors.emplace_back(0.0f);

      texcoords.emplace_back(0.0f);
      texcoords.emplace_back(0.0f);

      normals.emplace_back(-1.0f);
      normals.emplace_back(0.0f);
      normals.emplace_back(0.0f);
    }

  unsigned int ind[] =
  {
    0, 2, 3,
    0, 3, 1,
  };
  std::copy(std::begin(ind), std::end(ind), std::back_inserter(indices));

  return *this;
}


void Object::prepareForTracer()
{
  if (!triangles.empty()) return;

  objectArea = 0.0f;

  material.prepareForTracer();

  std::vector<float> distr;

  wholeObjectBox.reset();


  for (auto i = 0, t = 0; i < indices.size(); i += 3, t++)
  {
    const unsigned int i1 = indices[i+0] * 3;
    const unsigned int i2 = indices[i+1] * 3;
    const unsigned int i3 = indices[i+2] * 3;

    const unsigned int t1 = indices[i+0] * 2;
    const unsigned int t2 = indices[i+1] * 2;
    const unsigned int t3 = indices[i+2] * 2;

    glm::vec4 pA(objToWorld * glm::vec4(vertices[i1+0], vertices[i1+1], vertices[i1+2], 1.0f));
    glm::vec4 pB(objToWorld * glm::vec4(vertices[i2+0], vertices[i2+1], vertices[i2+2], 1.0f));
    glm::vec4 pC(objToWorld * glm::vec4(vertices[i3+0], vertices[i3+1], vertices[i3+2], 1.0f));

    glm::vec4 nA(objToWorld * glm::vec4(normals[i1+0], normals[i1+1], normals[i1+2], 0.0f));
    glm::vec4 nB(objToWorld * glm::vec4(normals[i2+0], normals[i2+1], normals[i2+2], 0.0f));
    glm::vec4 nC(objToWorld * glm::vec4(normals[i3+0], normals[i3+1], normals[i3+2], 0.0f));

    // If the object is not supposed to be smooth, make the normals for the triangle vertices the same
    if (!isSmooth)
      nA = nB = nC = glm::vec4(glm::normalize(glm::cross(glm::vec3(pB-pA), glm::vec3(pC-pA))), 0.0f);

    triangles.emplace_back(Triangle(
      glm::vec3(pA), glm::vec3(pB), glm::vec3(pC),
      glm::vec3(nA), glm::vec3(nB), glm::vec3(nC),

      glm::vec3(colors[i1+0], colors[i1+1], colors[i1+2]),
      glm::vec3(colors[i2+0], colors[i2+1], colors[i2+2]),
      glm::vec3(colors[i3+0], colors[i3+1], colors[i3+2]),

      glm::vec2(texcoords[t1+0], texcoords[t1+1]),
      glm::vec2(texcoords[t2+0], texcoords[t2+1]),
      glm::vec2(texcoords[t3+0], texcoords[t3+1]),
      material
    ));

    objectArea += triangles.rbegin()->getSurfArea();
    distr.emplace_back(triangles.rbegin()->getSurfArea());

    wholeObjectBox.expand(*triangles.rbegin());

  }

  for (auto & triangle : triangles)
    triangle.setObjArea(objectArea);

  for (auto & box : innerBoxes)
  {
    box.reset();
    for (auto idx : box.getIndices())
      box.expand(triangles[idx]);
  }

  trianglesDistr = std::discrete_distribution<int>(distr.begin(), distr.end());
  wholeObjectSphere.expand(wholeObjectBox);


}


const Triangle * Object::traceRay(Ray & ray)
{
  // Сheck bounding box.
  if (!wholeObjectBox.checkRay(ray)) return nullptr;

  const Triangle * bestTriangle = nullptr;

  if (!innerBoxes.empty())
  {
    for (auto & box : innerBoxes)
      // find intersection with bounding box
      if (box.checkRay(ray))
      {
        for (auto & idx : box.getIndices())
          if (triangles[idx].traceRay(ray))
            bestTriangle = &triangles[idx];
      }
  }
  else
  {
    // find intersection with triangles
    for (auto & triangle : triangles)
      if (triangle.traceRay(ray))
        bestTriangle = &triangle;
  }

  return bestTriangle;
}


void Object::prepareForRender()
{
  if (!isOpenGLEnabled) return;

  // Fill color bufer
  for (auto i = 0; i < colors.size(); )
  {
    colors[i++] = material.getColor().r;
    colors[i++] = material.getColor().g;
    colors[i++] = material.getColor().b;
  }

  // Attributes
  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(decltype(vertices)::value_type), vertices.data(), GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, CBO);
  glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(decltype(colors)::value_type), colors.data(), GL_STATIC_DRAW);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  glEnableVertexAttribArray(1);

  glBindBuffer(GL_ARRAY_BUFFER, NBO);
  glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(decltype(normals)::value_type), normals.data(), GL_STATIC_DRAW);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  glEnableVertexAttribArray(2);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(decltype(indices)::value_type), indices.data(), GL_STATIC_DRAW);

}


void Object::render(Shader * program) const
{
  if (!isOpenGLEnabled) return;

  // Update uniforms (object to world)
  int modelToWorldLoc = glGetUniformLocation(program->ID, "modelToWorld"); // TODO: put it in the shader class
  glUniformMatrix4fv(modelToWorldLoc, 1, GL_FALSE, glm::value_ptr(objToWorld));

  // Bind buffer
  glBindVertexArray(VAO);

  glPolygonMode(GL_FRONT_AND_BACK, isWireframe ? GL_LINE : GL_FILL);

  // Render
  glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
}
