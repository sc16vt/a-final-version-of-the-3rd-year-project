
#include <material.hpp>


Material & Material::setColor(const glm::vec3 & color)
{
  Color = color;
  return *this;
}

Material & Material::initEmission(float prob, float rad, float beam)
{
  Emission   = prob;
  radiance   = rad;
  beamRadius = beam < 1.0f ? beam : 1.0f;
  return *this;
}

Material & Material::initDiffSpec(float prob, float spec, float spow)
{
  DiffSpec     = prob;
  specularCoef = spec;
  specularPow  = spow;
  return *this;
}

Material & Material::initDiffuse(float prob)
{
  DiffSpec     = prob;
  specularCoef = 0.0f;
  specularPow  = 1.0f;
  return *this;
}

Material & Material::initReflection(float prob)
{
  Reflection = prob;
  return *this;
}

Material & Material::initRefraction(float prob, float coef)
{
  Refraction  = prob;
  refractCoef = coef;
  return *this;
}


void Material::prepareForTracer()
{
  float sum = remap0to1(Emission + DiffSpec + Reflection + Refraction);

  Emission   /= sum;
  DiffSpec   /= sum;
  Reflection /= sum;
  Refraction /= sum;

  distribution = std::discrete_distribution<int>({ Emission, DiffSpec, Reflection, Refraction });
}


float Material::fresnel(float cosi, float etai, float etat) const
{
  float sini = sqrtf(1.0f - cosi * cosi);
  float sint = sini * etai / etat;

  // Total internal reflection
  if (sint >= 1) { return 1.0f; }

  float cost = sqrtf(1.0f - sint * sint);

  float Rs = (etai * cosi - etat * cost) / (etai * cosi + etat * cost);
  float Rp = (etai * cost - etat * cosi) / (etai * cost + etat * cosi);

  return (Rs * Rs + Rp * Rp) / 2;
}
