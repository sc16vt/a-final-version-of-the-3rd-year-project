
#include <colorBuffer.hpp>
#include <glm/glm.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"


void create_directories(const std::string &path)
{
  size_t curPos= 0;
  do
  {
    curPos = path.find('/', curPos);
    if(curPos != std::string::npos)
    {
      std::string folder = path.substr(0, curPos);
      if(folder != "..")
        mkdir(folder.c_str(), S_IRWXU |S_IRWXG | S_IROTH | S_IXOTH );
      curPos++;
    }
  } while(curPos != std::string::npos);
  std::cout << path << "-" << mkdir(path.c_str(), S_IRWXU |S_IRWXG | S_IROTH | S_IXOTH) << std::endl;

}


ColorBuffer::ColorBuffer(const std::string & name)
{
  imageName = name;
}


int ColorBuffer::makeIndex(const glm::vec2 & crd) const
{
  return (int)crd.y * imageSize.x + (int)crd.x;
}


bool ColorBuffer::hasData() const
{
  for(auto & pixel : pureBuffer)
    if (pixel.r + pixel.g + pixel.b > 0.0f) return true;

  return false;
}


float ColorBuffer::getMaxLuminance()
{
  float result = 0.0f;
  for (auto & pixel : pureBuffer)
    result = std::max(result, luminance(pixel));
  return remap0to1(result);
}


float ColorBuffer::getAverageLuminance()
{
  float result = 0.0f;
  for (auto & pixel : pureBuffer)
    result += luminance(pixel) / (float)pureBuffer.size();
  return result;
}


void ColorBuffer::addToBuffer(const glm::vec4 & color)
{
  std::lock_guard<std::mutex> lock(pureBufferMutex);
  for (auto & pixel : pureBuffer) pixel += color;
}


void ColorBuffer::addToPixel(const glm::vec2 & crd, const glm::vec4 & color)
{
  std::lock_guard<std::mutex> lock(pureBufferMutex);
  glm::vec2 v00 = glm::vec2(floor(crd.x), floor(crd.y));
  if (vbounds(v00, imageSize))

    pureBuffer[makeIndex(v00)] += color;
}


void ColorBuffer::addFiltered(const glm::vec2 & crd, const glm::vec4 & color)
{
  // std::cout << color.x << " " << color.y << " " << color.z << " " << color.w << std::endl;

  std::lock_guard<std::mutex> lock(pureBufferMutex);

  glm::vec2 v00 = glm::vec2(floor(crd.x), floor(crd.y));
  float a00 = varea(glm::vec2(1.0f) - (v00 - crd));
  if (vbounds(v00, imageSize)) pureBuffer[makeIndex(v00)] += a00 * color;

  glm::vec2 v10 = glm::vec2(ceilf(crd.x), floor(crd.y));
  float a10 = varea(glm::vec2(1.0f) - (v10 - crd));
  if (vbounds(v10, imageSize)) pureBuffer[makeIndex(v10)] += a10 * color;

  glm::vec2 v01 = glm::vec2(floor(crd.x), ceilf(crd.y));
  float a01 = varea(glm::vec2(1.0f) - (v01 - crd));
  if (vbounds(v01, imageSize)) pureBuffer[makeIndex(v01)] += a01 * color;

  glm::vec2 v11 = glm::vec2(ceilf(crd.x), ceilf(crd.y));
  float a11 = varea(glm::vec2(1.0f) - (v11 - crd));
  if (vbounds(v11, imageSize)) pureBuffer[makeIndex(v11)] += a11 * color;

}


void ColorBuffer::initBuffer(const glm::ivec2 & size)
{
  imageSize = size;
  pureBuffer.resize(varea(imageSize));

  for(auto & pixel : pureBuffer)
    pixel = glm::vec4(0.0f);
}


void ColorBuffer::generateImage()
{
  imageBufferRGB.resize(varea(imageSize) * 3);
  imageBufferHDR.resize(varea(imageSize) * 3);

  for(int y = 0; y < imageSize.y; y++)
    for(int x = 0; x < imageSize.x; x++)
    {
      const int sIdx = makeIndex(glm::ivec2(x, y));

      glm::vec4 pixel = pureBuffer[sIdx];
      pixel *= scaleFactor / std::max(1.0f, pixel.w);

      const int dIdx = ((imageSize.y - y - 1) * imageSize.x + x) * 3;

      imageBufferHDR[dIdx+0] = pixel.r;
      imageBufferHDR[dIdx+1] = pixel.g;
      imageBufferHDR[dIdx+2] = pixel.b;


      if (std::min(std::min(pixel.r, pixel.g), pixel.b) > 1.0f)
        pixel = glm::vec4(1.0f);

      imageBufferRGB[dIdx+0] = ((int)(powf(pixel.r, 1.0f / 2.2f) * SM_RGB));
      imageBufferRGB[dIdx+1] = ((int)(powf(pixel.g, 1.0f / 2.2f) * SM_RGB));
      imageBufferRGB[dIdx+2] = ((int)(powf(pixel.b, 1.0f / 2.2f) * SM_RGB));
    }
}


void ColorBuffer::saveToFile(const std::string & path, const std::string & name, bool hdr, bool png)
{

  if (!hasData()) return;
  std::cout << "Saving to file!" << std::endl;

  std::string pathHDR = path;// + (hdr && png ? "/HDR" : "");
  std::string pathPNG = path + (hdr && png ? "/png" : "");

  if (hdr)
  {

    create_directories(pathHDR);

    std::string fileName = pathHDR + "/" + name + imageName + ".hdr";
    stbi_write_hdr(fileName.c_str(), imageSize.x, imageSize.y, 3, imageBufferHDR.data());
  }

  if (png)
  {
    create_directories(pathPNG);

    std::string fileName = pathPNG + "/" + name + imageName + ".png";
    stbi_write_png(fileName.c_str(), imageSize.x, imageSize.y, 3, imageBufferRGB.data(), imageSize.x * 3);
  }
}
