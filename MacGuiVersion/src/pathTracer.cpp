
#include <pathTracer.hpp>


PathTracer::PathTracer(const Scene & sc, const Camera & cam) : BaseTracer(sc, cam)
{
  // Set tracer parameters
  tracerName      = "Path";
  maxPathLength   = 9;        // Max light path length of path

  samplesLowQuality  = 16;    // Num of samples for low quality image
  samplesGoodQuality = 512;   // Num of samples for good quality image
  samplesBestQuality = 2048;  // Num of samples for best quality image

  // Set camera buffer scale to adjust brightness
  cameraBuffer.setScaleFactor(2.0f);
};


void PathTracer::tracePathFromCam(LightPath & path, int length)
{
  IntersectionPoint camPoint;
  Ray rayFromCam;
  createCameraPoint(camPoint, rayFromCam, path.getScreenPos());

  // Init path insert position to add points to the end ot path
  path.setPathDir(Forward);
  path.setCurrentPosToEnd();

  // Init full path with eye point as first point in path
  path.insertPoint(camPoint);

  // Trace path from eye
  findPath(path, rayFromCam, length - 1);
}


void PathTracer::tracePixel(const glm::vec2 & crd)
{
  // Create an initial path (a path through sample point on the screen (in the pixel) towards one of the light sources)
  LightPath path(crd, maxPathLength + 1);

  // Trace path from Camera
  tracePathFromCam(path, maxPathLength);

  // Skip if we haven't hit any light source (including background)
  if (path.getLastInsertPoint().isL())
  {
    // Update screen coords and check if we are still in screen bounds
    if (!path.updateScreenCrd(camera)) return;

    // Compute path Radiance and Weight
    path.computePath();

    // Compute and store color
    addColorToCamera(path.getScreenCrd(), path.getRadiance());
  }

  // Add samples counter
  addCountToCamera(crd, 1.0f);
}
