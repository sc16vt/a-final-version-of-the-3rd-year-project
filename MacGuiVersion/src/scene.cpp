
#include <scene.hpp>


bool Scene::isOpenGLEnabled = false;


Scene::Scene(Camera & cam) : camera(cam) {}

Scene::~Scene()
{
  for (auto & object : allObjects)
    delete object;
}


void Scene::selectTestMesh(int sel)
{
  if (sel <= meshes.size())
  {
    objects = meshes[sel];
    prepareForRender();
  }

  dirLightFlag = (meshesWithDirLight.find(sel) != meshesWithDirLight.end());

  sceneName = std::to_string(sel);
}


void Scene::addObjToMeshes(Object * obj, const std::vector<int> & indices)
{
  for (auto index : indices)
  {
    while (index >= meshes.size())
      meshes.emplace_back(std::vector<Object *>());

    meshes[index].emplace_back(obj);
    allObjects.emplace(obj);
  }
}

void Scene::generateTestMeshes()
{
  glm::mat4 transf = glm::mat4(1.0f);

  glm::vec3 blue(0.2f, 0.2f, 1.0f);
  glm::vec3 green(0.2f, 1.0f, 0.2f);
  glm::vec3 red(1.0f, 0.2f, 0.2f);
  glm::vec3 orange(1.0f, 0.5f, 0.2f);
  glm::vec3 white(0.9f, 0.9f, 0.9f);
  glm::vec3 black(0.0f, 0.0f, 0.0f);
  glm::vec3 gray(0.7f, 0.7f, 0.7f);
  glm::vec3 dark(0.2f, 0.2f, 0.2f);
  glm::vec3 yellow(1.0f, 1.0f, 0.2f);
  glm::vec3 aqua(0.2f, 1.0f, 1.0f);
  glm::vec3 pink(1.0f, 0.2f, 1.0f);

  glm::vec3 aquaGlass(0.6f, 0.9f, 0.9f);
  glm::vec3 pinkGlass(0.9f, 0.6f, 0.9f);


  // Meshes --------------------------------------------------
  //
  // 0 - Just walls and ceil lamp
  // 1 - All objects with ceil lights
  // 2 - Beam through mirrors
  // 3 - Beams through glass sphere
  // 4 - Wall hole with glass sphere
  // 5 - Clear glass boxes
  // 6 - Small light from a crack in the wall
  // 7 - Water
  // 8 - Wall hole with glass box
  // 9 - No ceil but direct light


  // Walls --------------------------------------------------

  float holeSize   = 0.05f;
  float holeShift  = -0.5;

  float wallHeight = 3.0f;
  float wallWidth  = 4.0f;
  float wallDepth  = 6.0f;


  glm::vec3 wall1 = glm::vec3(0.3f, 1.0f, 1.0f);
  glm::vec3 wall2 = glm::vec3(1.0f, 0.5f, 0.2f);
  glm::vec3 wall3 = glm::vec3(0.3f, 0.3f, 1.0f);
  glm::vec3 wall4 = glm::vec3(0.3f, 0.9f, 0.3f);
  glm::vec3 floor = white;
  glm::vec3 ceil  = yellow;


  Material materialGlass;
  materialGlass.initRefraction(1.0f, 1.5);



  // Walls --------------------------------------------------

  Material materialFlCl;
  materialFlCl.initDiffSpec(1.0f, 0.3, 5.0);

  Material materialWall;
  materialWall.initDiffSpec(1.0f, 0.1, 5.0);

  { // floor
    auto object = new Object(glm::vec3(0.0f, 0.0f, 0.0f),
                             glm::vec3(wallWidth, 0.02f, wallDepth), camera);
    object->generateCube(materialFlCl.setColor(floor));
    addObjToMeshes(object, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
  }
  { // ceiling
    auto object = new Object(glm::vec3(0.0f, wallHeight, 0.0f),
                             glm::vec3(wallWidth, 0.02f, wallDepth), camera);
    object->generateCube(materialFlCl.setColor(ceil));
    addObjToMeshes(object, { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
  }
  { // back wall
    auto object = new Object(glm::vec3(0.0f, wallHeight*0.5f, -wallDepth*0.5f),
                             glm::vec3(wallWidth, wallHeight, 0.02f), camera);
    object->generateCube(materialWall.setColor(wall1));
    addObjToMeshes(object, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
  }
  { // left wall
    auto object = new Object(glm::vec3(-wallWidth*0.5f, wallHeight*0.5f, 0.0f),
                             glm::vec3(0.02f, wallHeight, wallDepth), camera);
    object->generateCube(materialWall.setColor(wall2));
    addObjToMeshes(object, { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
  }
  { // right wall
    auto object = new Object(glm::vec3(wallWidth*0.5f, wallHeight*0.5f, 0.0f),
                             glm::vec3(0.02f, wallHeight, wallDepth), camera);
    object->generateCube(materialWall.setColor(wall3));
    addObjToMeshes(object, { 0, 1, 2, 3, 5, 7, 9 });
  }
  { // near wall
    auto object = new Object(glm::vec3(0.0f, wallHeight*0.5f, wallDepth*0.5f),
                             glm::vec3(wallWidth, wallHeight, 0.02f), camera);
    object->generateCube(materialWall.setColor(wall4));
    addObjToMeshes(object, { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
  }



  // Mirrors --------------------------------------------------

  Material materialMirror;
  materialMirror.initReflection(0.9f)
                .setColor(white);

  { // beam from center to the left mirror about
    Material materialBeamThin;
    materialBeamThin.initEmission(1.0f, 20.0f, 0.07f)
                    .setColor(glm::vec3(1.0f));

    auto object = new Object(glm::vec3(0.0f, wallHeight*0.5, -wallDepth*0.25f+2.1f),
                             glm::radians(-132.5f), glm::vec3(0.0f, 1.0f, 0.0f),
                             glm::vec3(0.02f, 0.02f, 0.02f), camera);
    object->generatePlaneXY(materialBeamThin);
    addObjToMeshes(object, { 2 });
  }
  { // mirror on the left wall
    auto object = new Object(glm::vec3(-(wallWidth*0.5f-0.02f), wallHeight*0.5f, -1.0f),
                             glm::vec3(0.02f, wallHeight*0.8f, 2.5f), camera);
    object->generateCube(materialMirror);
    addObjToMeshes(object, { 1, 2, 4, 5, 7, 8 });
  }
  { // mirror on the right wall
    auto object = new Object(glm::vec3( (wallWidth*0.5f-0.02f), wallHeight*0.5f, -wallDepth*0.25f),
                             glm::vec3(0.02f, wallHeight*0.8, 1.5f), camera);
    object->generateCube(materialMirror);
    addObjToMeshes(object, { 1, 3, 4, 5, 7, 8, 9 });
  }
  { // mirror on the back wall
    auto object = new Object(glm::vec3(0.0f, wallHeight*0.5f, -wallDepth*0.5f+0.02),
                             glm::vec3(2.5f, wallHeight*0.8, 0.02f), camera);
    object->generateCube(materialMirror);
    addObjToMeshes(object, {  1, 2, 3, 4, 5, 6, 7, 8, 9 });
    // CHANGED
  }



  // Ceiling 5 lights --------------------------------------------------

  Material materialSmallLight;
  materialSmallLight.initEmission(1.0f, 1.0f, 1.0f)
                   .setColor(glm::vec3(1.0));

  { // ceiling center light
    auto object = new Object(glm::vec3(0.0f, wallHeight-0.11, -wallDepth*0.25),
                             glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                             glm::vec3(0.2f, 0.2f, 0.2f), camera);
    object->generatePlaneXY(materialSmallLight);
    addObjToMeshes(object, { 1 });
  }

  // ceiling side lights
  for (auto x = -1; x <= 1; x += 2)
    for (auto y = -1; y <= 1; y += 2)
    {
      auto object = new Object(glm::vec3(x*(wallWidth*0.5f-0.7f), wallHeight-0.11, -wallDepth*0.25f+y*0.5f),
                               glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                               glm::vec3(0.2f, 0.2f, 0.2f), camera);
      object->generatePlaneXY(materialSmallLight);
      addObjToMeshes(object, { 1 });
    }



  // Clear glass  --------------------------------------------------

  { // ceiling lamp for clear glass objects
    Material materialDownLight;
    materialDownLight.initEmission(1.0f, 3.0f, 1.0f).setColor(glm::vec3(1.0));

    auto object = new Object(glm::vec3(0.0f, wallHeight-0.11, -wallDepth*0.25),
                                     glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::vec3(0.9f, 0.9f, 0.2f), camera);
    object->generatePlaneXY(materialDownLight);
    addObjToMeshes(object, { 5 });
  }

  { // cube rotated
    auto object = new Object(glm::vec3(-0.9f, 0.6f, -wallDepth*0.25+0.9),
                             glm::radians(35.0f), glm::vec3(0.0f, 1.0f, 0.0f),
                             glm::vec3(0.9f), camera);
    object->generateCube(materialGlass.setColor(aquaGlass));
    addObjToMeshes(object, { 1, 5, 6 });
  }
  { // sphere inside
    Material material;
    material.initDiffSpec(1.0f, 0.3, 7.0).setColor(pink);

    auto object = new Object(glm::vec3(-0.9f, 0.6f, -wallDepth*0.25+0.9),
                             glm::vec3(0.5), camera);

    object->generateSphere(material, 64, true);
    addObjToMeshes(object, { 1, 5, 6 });
  }

  { // clear glass sphere
    auto object = new Object(glm::vec3(0.9f, 0.7f, -wallDepth*0.25+0.5),
                             glm::vec3(1.0), camera);

    object->generateSphere(materialGlass.setColor(glm::vec3(1.0f)), 64, true);
    addObjToMeshes(object, { 5 });
  }

  { // cube rotated inside
    Material material;
    material.initDiffSpec(1.0f, 0.3, 7.0).setColor(green);

    auto object = new Object(glm::vec3(0.9f, 0.7f, -wallDepth*0.25+0.5),
                             glm::radians(35.0f), glm::vec3(0.0f, 1.0f, 0.0f),
                             glm::vec3(0.2f), camera);
    object->generateCube(material);
    addObjToMeshes(object, { 5 });
  }



  // Hole light with glass cube & sphere --------------------------------------------------

  { // right wall back part
    auto object = new Object(glm::vec3(wallWidth*0.5f, wallHeight*0.5f, -(wallDepth+holeSize)*0.25f+holeShift*0.5f),
                             glm::vec3(0.02f, wallHeight, (wallDepth-holeSize)*0.5f+holeShift), camera);
    object->generateCube(materialWall.setColor(wall3));
    addObjToMeshes(object, { 4, 6, 8 });
  }
  { // right wall near part
    auto object = new Object(glm::vec3(wallWidth*0.5f, wallHeight*0.5f, (wallDepth+holeSize)*0.25f+holeShift*0.5f),
                             glm::vec3(0.02f, wallHeight, (wallDepth-holeSize)*0.5f-holeShift), camera);
    object->generateCube(materialWall.setColor(wall3));
    addObjToMeshes(object, { 4, 6, 8 });
  }


  { // hole lamp
    Material material;
    material.initEmission(1.0f, 20.0f, 1.0f).setColor(glm::vec3(1.0f));

    auto object = new Object(glm::vec3(wallWidth*0.5f+0.05, wallHeight*0.5f, holeShift),
                             glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f),
                             glm::vec3(holeSize*2.0f, wallHeight+0.2f, 0.02f), camera);
    object->generatePlaneXY(material);
    addObjToMeshes(object, { 4, 6, 8 });
  }


  { // glass cube near hole
    auto object = new Object(glm::vec3(wallWidth*0.5f-0.8f, 0.4f, holeShift),
                             glm::vec3(0.2f, 0.9f, 0.7f), camera);
    object->generateCube(materialGlass.setColor(pinkGlass));
    addObjToMeshes(object, { 8 });
  }
  { // glass sphere near hole
    auto object = new Object(glm::vec3(wallWidth*0.5f-1.0f, 0.6f, holeShift),
                             glm::vec3(0.9, 0.9, 0.9f), camera);

    object->generateSphere(materialGlass.setColor(pinkGlass), 64, true);
    addObjToMeshes(object, { 1, 4, 7 });
  }


  { // right small wall
    auto object = new Object(glm::vec3(wallWidth*0.5f-0.2, wallHeight*0.5f, holeShift + 0.2),
                             glm::vec3(0.02f, wallHeight+0.3f, holeSize+0.7f), camera);
    object->generateCube(materialWall.setColor(dark));
    addObjToMeshes(object, { 6 });
  }
  { // glass sphere near hole
    auto object = new Object(glm::vec3(wallWidth*0.5f-0.5f, 0.6f, holeShift+0.5f),
                             glm::vec3(0.9, 0.9, 0.9f), camera);

    object->generateSphere(materialGlass.setColor(pinkGlass), 64, true);
    addObjToMeshes(object, { 6 });
  }




  // Ceiling up light --------------------------------------------------

  { // ceiling light shining up for diffuse lighting
    Material materialLight;
    materialLight.initEmission(1.0f, 3.0f, 1.0f).setColor(glm::vec3(1.0));

    auto object = new Object(glm::vec3(0.0f, wallHeight-0.7, -wallDepth*0.25),
                             glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                             glm::vec3(0.5f, 0.5f, 0.5f), camera);
    object->generatePlaneXY(materialLight.setColor(glm::vec3(0.4f)));
    addObjToMeshes(object, { 2, 3 });
  }



  // Objects --------------------------------------------------

  { // diffuse + specular sphere
    Material material;
    material.initDiffSpec(1.0f, 0.3, 7.0).setColor(black);

    auto object = new Object(glm::vec3(1.0f, wallHeight*0.5f+0.3f, -wallDepth*0.5f+0.7f),
                             glm::vec3(0.9f), camera);
    object->generateSphere(material, 64, true);
    addObjToMeshes(object, { 1, 6, 7, 9 });
  }

  { // absolute mirror sphere
    Material material;
    material.initReflection(1.0f).setColor(black);

    auto object = new Object(glm::vec3(-1.0f, wallHeight*0.5f+0.3f, -wallDepth*0.5f+0.7f),
                             glm::vec3(1.3f), camera);
    object->generateSphere(material, 64, true);
    addObjToMeshes(object, { 1, 6, 7 });
  }



  // Glass sphere with beam --------------------------------------------------

  { // beam for glass sphere
    Material material;
    material.initEmission(1.0f, 0.5f, 0.5f).setColor(glm::vec3(1.0));

    auto object = new Object(glm::vec3(-wallWidth*0.5f+0.06f, 1.1f, holeShift),
                             glm::radians(145.0f), glm::vec3(0.0f, 0.0f, 1.0f),
                             glm::vec3(0.01f, 0.01f, 0.01f), camera);
    object->generatePlaneYZ(material);
    addObjToMeshes(object, { 3 });
  }
  { // glass sphere for beam
    auto object = new Object(glm::vec3(-wallWidth*0.5f+0.8f, 0.5f, holeShift),
                             glm::vec3(0.7, 0.7, 0.7f), camera);
    object->generateSphere(materialGlass.setColor(aquaGlass), 64, true);
    addObjToMeshes(object, { 3, 7, 9 });
  }



  // Dir Light --------------------------------------------------

  ambientColor  = 1.0f * SM_INV_RGB * glm::vec3(117.0f, 187.0f, 253.0f);

  dirLightDir   = glm::normalize(glm::vec3(1.0f, -1.0f, 0.5f));
  dirLightColor = 15.0f * glm::vec3(0.9f, 0.8f, 0.7f);
  meshesWithDirLight = { 9 };

  { // glass cube for direct light
    auto object = new Object(glm::vec3(0.0f, 0.4f, 0.7f),
                             glm::vec3(0.5f, 0.9f, 0.5f), camera);
    object->generateCube(materialGlass.setColor(pinkGlass));
    addObjToMeshes(object, { 9 });
  }


  // Water --------------------------------------------------

  { // ceiling up light for water
    Material material;
    material.initEmission(1.0f, 1.0f, 1.0f).setColor(glm::vec3(1.0));

    auto object = new Object(glm::vec3(0.0f, wallHeight-0.2, -wallDepth*0.25),
                             glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                             glm::vec3(0.5f, 0.5f, 0.2f), camera);
    object->generatePlaneXY(material);
    addObjToMeshes(object, { 7 });
  }
  { // ceiling down light for water
    Material material;
    material.initEmission(1.0f, 3.0f, 1.0f).setColor(glm::vec3(1.0));

    auto object = new Object(glm::vec3(0.0f, wallHeight-0.21, -wallDepth*0.25),
                             glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                             glm::vec3(0.5f, 0.5f, 0.2f), camera);
    object->generatePlaneXY(material);
    addObjToMeshes(object, { 7 });
  }
  { // water surface
    Material material;
    material.initRefraction(1.0f, 1.3f).setColor(0.7f * glm::vec3(0.5f, 1.0f, 1.0f));

    auto object = new Object(glm::vec3(0.0f, 1.1f, 0.0f),
                             glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                             glm::vec3(wallWidth, wallDepth, 0.1f), camera);
    object->generateWaveSurface(material, 128, 11.0f, 0.05f, glm::vec2(0.0f, -0.1f));
    addObjToMeshes(object, { 7 });
  }



  // Test --------------------------------------------------

  Material materialTestLight;
  materialTestLight.initEmission(1.0f, 10.0f, 1.0f)
                   .setColor(glm::vec3(1.0));

  auto lightCeilPlaneL = new Object(glm::vec3(0.0f, wallHeight-0.11, -wallDepth*0.25),
                                    glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                                    glm::vec3(0.9f, 0.9f, 0.9f), camera);
  lightCeilPlaneL->generatePlaneXY(materialTestLight);
  addObjToMeshes(lightCeilPlaneL, { 0 });

  Material materialClearClear;
  materialClearClear.initRefraction(1.0f, 1.0f).setColor(glm::vec3(1.0f));

  auto testClearCube1 = new Object(glm::vec3(0.7f, 1.5f, -wallDepth*0.25),
                                  glm::vec3(1.0f, 0.4f, 1.0f), camera);
  testClearCube1->generateCube(materialClearClear);

  auto testClearCube2 = new Object(glm::vec3(-0.7f, 1.0f, -wallDepth*0.25+0.5),
                                   glm::vec3(1.0f, 1.5f, 0.4f), camera);
  testClearCube2->generateCube(materialClearClear);
}

// Build a list of triangles that a tracer will check for intersections
void Scene::prepareForTracer()
{
  wholeSceneBox.reset();
  wholeSceneSphere.reset();

  lights.clear();

  for (auto & object : objects)
  {
    object->prepareForTracer();
    if (object->getMaterial().hasEmission())
      lights.emplace_back(object);

    wholeSceneBox.expand(object->getBoundingBox());
    wholeSceneSphere.expand(object->getBoundingBox());
  }
}


const Triangle * Scene::traceRay(Ray & ray) const
{
  const Triangle * bestTriangle = nullptr;

  for (auto & object : objects)
  {
    const Triangle * triangle = object->traceRay(ray);
    if (triangle != nullptr)
      bestTriangle = triangle;
  }

  return bestTriangle;
}


void Scene::prepareForRender()
{
  if (!isOpenGLEnabled) return;

  for (auto & object : objects)
  {
    object->prepareForRender();
  }

  program = new Shader("../shaders/vRasterisation.glsl", "../shaders/fRasterisation.glsl");
  program->use();

  worldToScreenLoc = glGetUniformLocation(program->ID, "worldToScreen");
  glUniformMatrix4fv(worldToScreenLoc, 1, GL_FALSE, glm::value_ptr(camera.getToScreenTransf()));

  struct LightSource
  {
    glm::vec3 position;
    glm::vec3 power;

  } lightSource;

  lightSource.position = glm::vec3(0.0f, 1.0f, -0.5f);
  lightSource.power = glm::vec3(0.5f, 0.5f, 0.5f);

  int lightPosLoc = glGetUniformLocation(program->ID, "lightSource.position");
  int lightPowerLoc = glGetUniformLocation(program->ID, "lightSource.power");
  glUniform3fv(lightPosLoc, 1,  glm::value_ptr(lightSource.position));
  glUniform3fv(lightPowerLoc, 1,  glm::value_ptr(lightSource.power));

}


void Scene::render() const
{
  if (!isOpenGLEnabled) return;

  // Activate shaders
  program->use();

  // Update uniforms (camera)
  glUniformMatrix4fv(worldToScreenLoc, 1, GL_FALSE, glm::value_ptr(camera.getToScreenTransf()));

  // Render all the objects
  for (auto & object : objects)
    object->render(program);
}
