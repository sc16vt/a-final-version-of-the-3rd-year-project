#include <sampler.hpp>

void Sampler::coordSysFromNorm(const glm::vec3 & n, glm::vec3 & a, glm::vec3 & b)
{
  if (fabsf(n.x) > fabsf(n.y))
  {
    float invLen = 1.0f / sqrt(n.x * n.x + n.z * n.z);
    b = glm::vec3(n.z * invLen, 0.f, -n.x * invLen);
  }
  else
  {
    float invLen = 1.f / sqrt(n.y * n.y + n.z * n.z);
    b = glm::vec3(0.f, n.z * invLen, -n.y * invLen);
  }
  a = glm::cross(b, n);
}


int Sampler::random_uniform_int_0I(int i)
{
  std::uniform_int_distribution<int> random_distribution(0, i);
  return random_distribution(random_generator);
}


glm::vec2 Sampler::random_uniform_2D_0I(float to)
{
  std::uniform_real_distribution<float> random_distribution(0.0f, to);
  return glm::vec2(random_distribution(random_generator), random_distribution(random_generator));
}

float Sampler::random_uniform_float_0I(float to)
{
  std::uniform_real_distribution<float> random_distribution(0.0f, to);
  return random_distribution(random_generator);
}


float Sampler::random_normal_float(float a, float d)
{
  std::normal_distribution<float> random_distribution(a, d);
  return random_distribution(random_generator);
}

glm::vec2 Sampler::random_normal_float_2D(float a, float d)
{
  std::normal_distribution<float> random_distribution(a, d);
  return glm::vec2(
    random_distribution(random_generator),
    random_distribution(random_generator)
  );
}

glm::vec3 Sampler::random_normal_float_3D(float a, float d)
{
  std::normal_distribution<float> random_distribution(a, d);
  return glm::vec3(
    random_distribution(random_generator),
    random_distribution(random_generator),
    random_distribution(random_generator)
  );
}


glm::vec2 Sampler::uniformDiskSample(float rad)
{
  float theta  = SM_2PI * random_uniform_float_0I();
  float radius = rad * sqrtf(random_uniform_float_0I());
  return glm::vec2(radius * cos(theta), radius * sin(theta));
}


glm::vec3 Sampler::cosineHemisphereSample(const glm::vec3 & dir, float radius)
{
  glm::vec2 d = uniformDiskSample(radius);
  float z = sqrt(std::max(0.0f, 1.0f - glm::dot(d, d)));

  glm::vec3 a, b;
  coordSysFromNorm(dir, a, b);

  return d.x * a + d.y * b + z * dir;
}

glm::vec3 Sampler::uniformHemisphereSample(const glm::vec3 & dir)
{
  glm::vec3 rnd;

  do { rnd = random_normal_float_3D(0.0f, 1.0f); }
  while (rnd.x == 0.0f && rnd.y == 0.0f && rnd.z == 0.0f);

  rnd = glm::normalize(rnd);
  return glm::dot(rnd, dir) > 0.0f ? rnd : -rnd;
}


// Initialization -------------------------------------------------------------------------------

Sampler::Sampler() : random_generator(random_device())
{
}

unsigned int Sampler::resetRandomGenerator()
{
  unsigned int seed = random_device();
  random_generator.seed(seed);
  return seed;
}

void Sampler::resetRandomGenerator(unsigned int seed)
{
  random_generator.seed(seed);
}

// Sample screen points -------------------------------------------------------------------------------

glm::vec2 Sampler::pixelPointSample(const glm::vec2 & o)
{
  return (o + random_uniform_2D_0I(1.0f));
}

glm::vec2 Sampler::screenPointSample(const glm::ivec2 & size)
{
  return glm::vec2(random_uniform_float_0I((float)size.x),
                   random_uniform_float_0I((float)size.y));
}


// Sample materials -------------------------------------------------------------------------------

PointType Sampler::materialSample(const std::discrete_distribution<int> & distribution)
{
  std::discrete_distribution<int> random_distribution = distribution;
  return static_cast<PointType>(random_distribution(random_generator) + Emission);
}

glm::vec3 Sampler::diffuseSample(const IntersectionPoint & point)
{
  return uniformHemisphereSample(point.getNormal());
}

float Sampler::diffuseSamplePDF(float cosine) const
{
  return SM_INV_PI;
}

float Sampler::specularSamplePDF(float cosine, float pow) const
{
  return powf(cosine, pow) * SM_INV_PI;
}

float Sampler::refractionSample()
{
  return random_uniform_float_0I();
}


// Sample lights -------------------------------------------------------------------------------

glm::vec3 Sampler::lightRaySample(const IntersectionPoint & point, const Scene & scene)
{
  if (point.isLd()) { return scene.getDirLightDir(); }

  float radius = point.getMaterial().getBeamRadius();
  if (radius < 1.0f)
    return cosineHemisphereSample(point.getNormal(), radius);
  else
    return uniformHemisphereSample(point.getNormal());
}

float Sampler::lightRaySamplePDF(const IntersectionPoint & point, float cosine) const
{
  if (point.isLd()) { return 1.0f; }

  float radius = point.getMaterial().getBeamRadius();
  if (radius < 1.0f)
    return cosine * SM_INV_PI / (1.0f - sqrtf(1.0f - radius * radius));
  else
    return SM_INV_PI;
}

IntersectionPoint Sampler::lightPointSample(const Scene & scene)
{
  const std::vector<Object *> & lights = scene.getLights();
  int lightIdx = random_uniform_int_0I((int)lights.size() - (scene.hasDirLight() ? 0 : 1));

  // If we randomly selected direct light
  if (lightIdx == lights.size())
  {
    float radius = scene.getBoundingSphere().getRadius();
    glm::vec3 center = scene.getBoundingSphere().getCenter();

    glm::vec2 d = uniformDiskSample(radius);
    glm::vec3 a, b;
    coordSysFromNorm(scene.getDirLightDir(), a, b);

    glm::vec3 point = glm::vec3(d.x * a + d.y * b) + center - radius * scene.getDirLightDir();

    return IntersectionPoint(DirLight, point);
  }

  Object * lightObj = lights[lightIdx];

  std::discrete_distribution<int> & random_distribution = lightObj->getTrianglesDistr();
  int triIdx = random_distribution(random_generator);

  const Triangle & triangle = lightObj->getTriangles()[triIdx];
  glm::vec3 point = triangle.getUniformPoint(random_uniform_2D_0I());

  return IntersectionPoint(Emission, point, &triangle);
}

float Sampler::lightPointSamplePDF(const IntersectionPoint & point, const Scene & scene) const
{
  if (point.isLd())
  {
    float radius = scene.getBoundingSphere().getRadius();
    return 1.0f / (SM_PI * radius * radius);
  }

  return 1.0f / (((float)(scene.getLights().size()) + (scene.hasDirLight() ? 1.0f : 0.0f)) * point.getObjectArea());
}


// Sample path length -------------------------------------------------------------------------------

int Sampler::pathLengthSample(int lmax)
{
  return random_uniform_int_0I(lmax - 2) + 2;
}

int Sampler::pathPositionSample(int mxl)
{
  return random_uniform_int_0I(mxl);
}


// Sample mutations -------------------------------------------------------------------------------

int Sampler::mltMutationSample(std::discrete_distribution<int> & distribution)
{
  return distribution(random_generator);
}

float Sampler::pathAcceptanceSample()
{
  return random_uniform_float_0I();
}


// The following function perturbs the direction N by a random angle that is exponentially
// distributed between θ1 and θ2. We assume that D is normalized and θ1 and θ2 are small.
// As suggested by Veach and Guibas, we use 0.0001 radians for θ1 and 0.1 radians for θ2.
// (Appendix C) http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.71.4481&rep=rep1&type=pdf
// http://graphics.stanford.edu/papers/metro/metro.pdf
glm::vec3 Sampler::mutRayOffsetSample(const glm::vec3 & o, float t1, float t2)
{
  // Make a UVN coordinate system from N Point U, V;
  glm::vec3 u, v;
  coordSysFromNorm(o, u, v);

  // Determine offsets using the approximation θ ≈ sin θ
  float phi = random_uniform_float_0I() * SM_2PI;
  float r = t2 * exp(-log(t2 / t1) * random_uniform_float_0I());

  // Calculate the new direction
  return glm::normalize(o + r * cosf(phi) * u + r * sinf(phi) * v);
}


// The function below moves the pixel location (x,y) by an exponentially distributed random distance r
// between r1 and r2 . In other words, r = r2 × e−l n(r2 /r1 )ξ , where ξ is a random number between 0 and 1.
// This procedure is used to determine the new pixel location for a lens perturbation.
// We use values of 0.1 pixels for r1 and 10% of the image width for r2
// (Appendix C) http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.71.4481&rep=rep1&type=pdf
// http://graphics.stanford.edu/papers/metro/metro.pdf
glm::vec2 Sampler::mutPixelOffsetSample(const glm::vec2 & o, const glm::vec2 & s, float r1, float r2)
{
  float phi = random_uniform_float_0I() * SM_2PI;
  float r = r2 * exp( -log(r2/r1) * random_uniform_float_0I() );

  float x = o.x + r * cos(phi);
  float y = o.y + r * sin(phi);

  return glm::vec2(x < s.x ? x : x - s.x, y < s.y ? y : y - s.y);
}

