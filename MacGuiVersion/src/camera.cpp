
#include <camera.hpp>

Camera::Camera(const glm::vec3 & startPos, const glm::vec2 & res, float fov)
{
  resolution = res;

  float aspect = res.x / res.y;

  lensRadius = 1.0f;

  perspTransf = glm::perspective(fov, aspect, 0.1f, 100.0f);

  startCamPos   = startPos;
  currCamPos    = startPos;
  lookingAt     = glm::vec3(0.0f, 0.0f, -1.0f);
  topPointingAt = glm::vec3(0.0f, 1.0f, 0.0f);

  float depth = 0.5f / aspect / std::tan(fov * 0.5f);

  base_lu_corner = glm::vec3(-0.5f,  0.5f / aspect, -depth);
  base_ru_corner = glm::vec3( 0.5f,  0.5f / aspect, -depth);
  base_ld_corner = glm::vec3(-0.5f, -0.5f / aspect, -depth);
  base_rd_corner = glm::vec3( 0.5f, -0.5f / aspect, -depth);

  generateCameraParameters();
}


void Camera::generateCameraParameters()
{
  // Update camera
  currCamPos = startCamPos + user_shift;

  // Calculate a matrix out of updated camera
  cameraTransf = glm::lookAt(currCamPos, currCamPos + lookingAt, topPointingAt);

  // Rotate cam matrix
  cameraTransf = glm::rotate(glm::rotate(glm::mat4(), glm::radians(user_rotate.y), glm::vec3(1.0f, 0.0f, 0.0f)),
                                                      glm::radians(user_rotate.x), glm::vec3(0.0f, 1.0f, 0.0f)) * cameraTransf;

  // Calculate screen corners
  glm::mat4 invertedTransf = glm::inverse(cameraTransf);
  world_lu_corner = glm::vec3(invertedTransf * glm::vec4(base_lu_corner, 1.0));
  world_ru_corner = glm::vec3(invertedTransf * glm::vec4(base_ru_corner, 1.0));
  world_ld_corner = glm::vec3(invertedTransf * glm::vec4(base_ld_corner, 1.0));
  world_rd_corner = glm::vec3(invertedTransf * glm::vec4(base_rd_corner, 1.0));

  xVector = world_rd_corner - world_ld_corner;
  yVector = world_lu_corner - world_ld_corner;

  xLength = glm::length(xVector);
  yLength = glm::length(yVector);

  view_dir = glm::normalize(0.5f * (world_lu_corner + world_rd_corner) - currCamPos);

  glm::vec3 origin = glm::vec3(invertedTransf * glm::vec4(glm::vec3(0.0f), 1.0f));
  std::cout << "CamPos: (" << origin.x << ", " << origin.y << ", " << origin.z << "), turn (" << user_rotate.x << ", " << user_rotate.y << ")" << std::endl;
}


float Camera::calcPDF(const glm::vec3 & dir) const
{
  return 1.0f;
}


glm::vec3 Camera::calcBSDF(const glm::vec3 & dir) const
{
  return glm::vec3(1.0f);
}


bool Camera::getProjCoord(const glm::vec3 & pointCoord, glm::vec2 & pixelCoord) const
{
  // Create a ray from eye to the point
  Ray ray(currCamPos, pointCoord - currCamPos);

  // Check if it intersects the screen (two of its triangles)
  // Check that the ray is not parallel to the triangle
  float cross = glm::dot(ray.getDirection(), view_dir);
  if (cross <= 0.0f) return false;

  // Find ray's distance to the intersection point with the plane where the screen lies
  const float distance = glm::dot(world_ld_corner - ray.getOrigin(), view_dir) / cross;

  // Find intersection point with the plane
  const glm::vec3 intersection = ray.getOrigin() + distance * ray.getDirection();
  glm::vec3 intersectionVec = intersection - world_ld_corner;

  // Project the intersection onto height vector and width vector
  float widthPoint  = glm::dot(xVector / xLength, intersectionVec) / xLength;
  float heightPoint = glm::dot(yVector / yLength, intersectionVec) / yLength;

  // Calculate relative h and w and store it as pixel coord
  glm::vec2 newCoord = glm::vec2(widthPoint * resolution.x, heightPoint * resolution.y);

  // Check that the projection vectors are less than h and w vectors
  // if not return false
  if (newCoord.x >=resolution.x || newCoord.y >= resolution.y || newCoord.x < 0 || newCoord.y < 0)
  return false;

  // return true
  pixelCoord = newCoord;
  return true;
}


void Camera::resetPos(const glm::vec3 & pos, const glm::vec2 & turn)
{
  user_shift  = pos - startCamPos;
  user_rotate = turn;
  generateCameraParameters();
}

void Camera::moveHorBy(float shift)
{
  user_shift.x -= shift;
  generateCameraParameters();
}

void Camera::moveFwdBy(float shift)
{
  user_shift.z -= shift;
  generateCameraParameters();
}

void Camera::moveVerBy(float shift)
{
  user_shift.y -= shift;
  generateCameraParameters();
}

void Camera::moveDirBy(float shift)
{
  glm::mat4 invertedTransf = glm::inverse(cameraTransf);
  user_shift += glm::vec3(invertedTransf * glm::vec4(0.0f, 0.0f, shift, 0.0));
  generateCameraParameters();
}

void Camera::moveSideBy(float shift)
{
  glm::mat4 invertedTransf = glm::inverse(cameraTransf);
  user_shift += glm::vec3(invertedTransf * glm::vec4(shift, 0.0f, 0.0f, 0.0));
  generateCameraParameters();
}


void Camera::turnHorBy(float shift)
{
  user_rotate.x += shift;
  generateCameraParameters();
}

void Camera::turnVerBy(float shift)
{
  user_rotate.y += shift;
  generateCameraParameters();
}
