This is a GUI version of the demo app created for the project "" by Valentina Tabunova.

To install the project:
- open the terminal
- go to folder build
- run "cmake .."
- run "make"
- an exicutable franken is the program


To use the project:

The project has 8 premade scenes available for render.
There are 8 scenes available. To navigate between them use number keys 1 to 8.
The scenes:
  8     A very dark scene with light shinning through a crack in the wall
  7     A scene with a bunch of spheres. Half of the scene and spheres are under water
  6     A scene with light shinning through a crack in the wall  and then through pink glass box
  5     A well lit scene with glass box and sphere containing a diffuse sphere and box respectively
  4     A scene with light shinning through a crack in the wall and then through pink glass sphere
  3     A scene with a projector like beam of light going through a glass sphere
  2     A scene with beam of light reflecting in the mirrors
  1     A well lit scene with spheres made of different material


It is possible to choose the tracing algorithms to render with.
Press:
  z     -     to turn on Path tracer
  x     -     to turn on Bidirectional Path tracer
  c     -     to turn on MLT
  v     -     to turn on a combination between Path tracer and MLT
  b     -     to turn on a combination between Bidirectional path tracer and MLT
  m     -     to turn on all of the tracers


It is possible to set the quality of the picture to be rendered,
i.e. an optimal number of samples predefined for each tracing algorithm.
Press:
  y     -     to set quality to low
  u     -     to set quality to good
  i     -     to set quality to best


To tilt camera up, down left and right use up, down, left and right keys on the keyboard.


To move camera around scene first person like press shift and up, down, left and right keys on the keyboard.


To make renderer faster paralell option can be turned on\off by pressing p on the keyboard.
