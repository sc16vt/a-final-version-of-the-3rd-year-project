#include <boundingBox.hpp>


bool BoundingBox::checkRay(const Ray & ray) const
{
  float mi = -FLT_MAX, ma = FLT_MAX;
  const float dist = ray.getDistance();

  const glm::vec3 & d = ray.getDirection();

  glm::vec3 l(pointMin - ray.getOrigin());
  glm::vec3 h(pointMax - ray.getOrigin());

  for (int i = 0; i < 3; i++)
  {
    if (0.0f == d[i]) continue;

    const float inv = 1.0f / d[i];

    const float lo = l[i] * inv;
    const float hi = h[i] * inv;

    mi = std::max(mi, std::min(lo, hi));
    ma = std::min(ma, std::max(lo, hi));
  }

  return (mi <= ma) && (ma > 0.0f) && (mi < dist);
}


bool BoundingBox::checkPoint(const glm::vec3 & point) const
{
  return (pointMin.x <= point.x) && (point.x <= pointMax.x) &&
         (pointMin.y <= point.y) && (point.y <= pointMax.y) &&
         (pointMin.z <= point.z) && (point.z <= pointMax.z);
}


void BoundingBox::expand(const Triangle & tri)
{
  const glm::vec3 pMin = tri.getMin();
  const glm::vec3 pMax = tri.getMax();

  for (int i = 0; i < 3; i++)
  {
    pointMin[i] = std::min(pointMin[i], pMin[i]);
    pointMax[i] = std::max(pointMax[i], pMax[i]);
  }
}


void BoundingBox::expand(const BoundingBox & box)
{
  for (int i = 0; i < 3; i++)
  {
    pointMin[i] = std::min(pointMin[i], box.pointMin[i]);
    pointMax[i] = std::max(pointMax[i], box.pointMax[i]);
  }
}

void BoundingBox::expand(const glm::vec3 & pmin, const glm::vec3 & pmax)
{
  for (int i = 0; i < 3; i++)
  {
    pointMin[i] = std::min(pointMin[i], pmin[i]);
    pointMax[i] = std::max(pointMax[i], pmax[i]);
  }
}

void BoundingBox::reset()
{
  pointMin = glm::vec3( FLT_MAX);
  pointMax = glm::vec3(-FLT_MAX);
}
