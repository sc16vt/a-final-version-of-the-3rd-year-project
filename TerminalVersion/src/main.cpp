/*
  The main class of the project.
    - Parses the user input (cmd line options);
    - Sets up the scenes, includeing the position of the camera inside of them;
    - Starts the tracing algorithms;
 */

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <camera.hpp>
#include <quad.hpp>
#include <scene.hpp>
#include <pathTracer.hpp>
#include <bdirTracer.hpp>
#include <mltTracer.hpp>
#include <smmcltTracer.hpp>

#include <thread>
#include <iostream>


// Main settings ---------------------------------------------------------------

const int GlobalWidth  = 600;
const int GlobalHeight = 400;

const int maxThreadTime = 60; // seconds
const int maxThreadsNum = 10;


// Common flags ----------------------------------------------------------------

bool isParallel  = false;
bool isAutoStop  = false;

// bool isWindow    = true;

void setAutoStopMode(bool val)
{
  isAutoStop = val;
  std::cout << "Auto stop mode is switched " << (val ? "ON" : "OFF") << std::endl;
}

void setParallelMode(bool val)
{
  isParallel = val;
  std::cout << "Parallel mode is switched " << (val ? "ON" : "OFF") << std::endl;
}

// void setWindowMode(bool val)
// {
//   isWindow = val;
//   std::cout << "Window mode is switched " << (val ? "ON" : "OFF") << std::endl;
// }


// Camera ----------------------------------------------------------------------

Camera * camera = nullptr;

struct CamPos
{
  glm::vec3 pos;
  glm::vec2 turn;

  CamPos(const glm::vec3 & p, const glm::vec2 & t)
  {
    pos  = p;
    turn = t;
  }

  CamPos(const CamPos & src)
  {
    pos  = src.pos;
    turn = src.turn;
  }
};

std::map<int, std::vector<CamPos> > camPosForScene;

void addCamPos(int s, const glm::vec3 & p, const glm::vec2 & t)
{
  auto pos = camPosForScene.find(s);
  if (pos == camPosForScene.end())
    camPosForScene.emplace(s, std::vector<CamPos>());

  camPosForScene[s].emplace_back(CamPos(p, t));
}

//
void initCamPos()
{
  // 0 - Just walls and ceil lamp
  addCamPos(0, glm::vec3(0.0f, 1.5f, 2.6f), glm::vec2(0.0f, 0.0f));

  // 1 - All objects with ceil lights
  addCamPos(1, glm::vec3(0.5f, 1.3f, 2.6f), glm::vec2(-11.0f, 0.0f));

  // 2 - Beam through mirrors
  addCamPos(2, glm::vec3(0.0f, 1.5f, 2.6f), glm::vec2(0.0f, 0.0f));

  // 3 - Beams through glass objects
  addCamPos(3, glm::vec3(-1.95f, 0.76f, 1.44f), glm::vec2(39.0f, 0.0f));
  addCamPos(3, glm::vec3(1.0f, 0.7f, 1.1f), glm::vec2(-39.0f, 2.0f));

  // 4 - Wall hole with glass sphere
  addCamPos(4, glm::vec3(1.8f, 0.9f, -2.58f), glm::vec2(-134.0f, 0.0f));
  addCamPos(4, glm::vec3(0.0f, 1.5f, 2.6f), glm::vec2(0.0f, 0.0f));

  // 5 - Clear glass boxes
  addCamPos(5, glm::vec3(0.5f, 1.3f, 2.4f), glm::vec2(-9.0f, 0.0f));
  addCamPos(5, glm::vec3(1.5f, 0.8f, -2.9f), glm::vec2(-145.0f, 0.0f));

  // 6 - Small amount of light from tiny split in the wall
  addCamPos(6, glm::vec3(-1.6f, 1.4f, 2.8f), glm::vec2(37.0f, 0.0f));

  // 7 - Water
  addCamPos(7, glm::vec3(0.0f, 1.7f, 2.6f), glm::vec2(0.0f, 0.0f));
  addCamPos(7, glm::vec3(0.0f, 2.8f, 1.5f), glm::vec2(0.0f, 33.0f));

  // 8 - Wall hole with glass box
  addCamPos(8, glm::vec3(0.0f, 2.4f, 2.6f), glm::vec2(0.0f, 20.0f));
  addCamPos(8, glm::vec3(1.8f, 0.9f, -2.58f), glm::vec2(-134.0f, 0.0f));

}

void selectCamPos(int s, int cp)
{
  auto camPos = camPosForScene.find(s);
  if (camPos == camPosForScene.end())
  {
    std::cout << "Error: scene " << s << " not found" << std::endl;
    return;
  }
  if (cp >= camPos->second.size())
  {
    std::cout << "Error: invalid camera position " << cp << " for scene " << s << std::endl;
    return;
  }

  camera->resetPos(camPos->second[cp].pos, camPos->second[cp].turn);
}


// Scene -----------------------------------------------------------------------

struct QueueItem
{
  int scene;
  int camPos;

  QueueItem(int s, int c)
  {
    scene  = s;
    camPos = c;
  };

  QueueItem(const QueueItem & src)
  {
    scene  = src.scene;
    camPos = src.camPos;
  };
};

Scene * scene  = nullptr;

std::vector<QueueItem> renderQueue;
QueueItem currScene(0, 0);


void addSceneToQueue(int sel, int cam)
{
  auto item = camPosForScene.find(sel);
  if (item == camPosForScene.end())
  {
    std::cout << "Error: scene " << sel << " not found" << std::endl;
    return;
  }
  if (cam >= item->second.size())
  {
    std::cout << "Error: invalid camera position " << cam << " for scene " << sel << std::endl;
    return;
  }

  renderQueue.emplace_back(QueueItem(sel, cam));
  std::cout << "  Scene " << sel << "." << cam << " is added to queue" << std::endl;
}

void addAllCamsForSceneToQueue(int sel)
{
  auto item = camPosForScene.find(sel);
  if (item == camPosForScene.end())
  {
    std::cout << "Error: scene " << sel << " not found" << std::endl;
    return;
  }

  // Add scene and all camera pos to queue
  for (int i = 0; i < item->second.size(); i++)
    addSceneToQueue(item->first, i);

  setAutoStopMode(true);
}

void addAllScenesToQueue()
{
  renderQueue.clear();
  for (auto item : camPosForScene)
  {
    // Skip[ 0 scene which is for test only
    if (item.first == 0) continue;

    // Add scene and all camera pos to queue
    addAllCamsForSceneToQueue(item.first);
  }

  setAutoStopMode(true);
}

void selectScene(int sel, int cam)
{
  auto camPos = camPosForScene.find(sel);
  if (camPos == camPosForScene.end())
  {
    std::cout << "Error: scene " << sel << " not found" << std::endl;
    return;
  }
  if (cam >= camPos->second.size())
  {
    std::cout << "Error: invalid camera position " << cam << " for scene " << sel << std::endl;
    return;
  }

  currScene = QueueItem(sel, cam);
  scene->selectTestMesh(sel);
  selectCamPos(sel, cam);

  std::cout << "Scene " << scene->getSceneName() << "." << currScene.camPos << " selected" << std::endl;
}

bool parseSceneSelectParam(const std::string & str)
{
  try
  {
    size_t shift;
    const float sel = abs(std::stoi(str, &shift));

    const std::string cst = str.substr(shift + 1);
    if (cst == "*")
    {
      addAllCamsForSceneToQueue(sel);
    }
    else
    {
      const float pos = abs(std::stoi(cst));
      addSceneToQueue((int)floorf(sel), (int)pos);
    }
  }
  catch (const std::invalid_argument & ia)
  {
    return false;
  }

  return true;
}


// Tracers ---------------------------------------------------------------------

enum TracerType
{
  ttAll,
  ttPath,
  ttBDir,
  ttMLT,
  ttSMMpt,
  ttSMMbd,
};

std::map<TracerType, BaseTracer *> tracersMap;
std::set<BaseTracer *> tracersSet;

std::string batchName = "";
bool isTraceRun  = false;


void UpdateTracer(BaseTracer * tracer)
{
  auto start = std::chrono::system_clock::now();
  std::chrono::duration<double> passed(0);

  do
  {
    if (tracer->updateRender()) return;
    passed = std::chrono::system_clock::now() - start;
  }
  while (passed.count() < maxThreadTime);
}

void initBatch()
{
  std::time_t currTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  char strTime[1024];
  std::strftime(strTime, sizeof(strTime) - 1, "%Y.%m.%d %H-%M-%S", localtime(&currTime));
  batchName = strTime;
}

bool isTracer(TracerType type)
{
  if (type == ttAll) return false;
  return (tracersSet.find(tracersMap[type]) != tracersSet.end());
}

void selectTracer(TracerType type, bool ison)
{
  if (type == ttAll)
  {
    if (ison)
      for (auto & tracer : tracersMap)
        tracersSet.emplace(tracer.second);
    else
      for (auto & tracer : tracersMap)
        tracersSet.erase(tracer.second);
  }
  else
  {
    if (ison)
      tracersSet.emplace(tracersMap[type]);
    else
      tracersSet.erase(tracersMap[type]);

    std::cout << tracersMap[type]->getTracerName() << " tracer is switched " << (ison ? "ON" : "OFF") << std::endl;
  }
}

void startTrace()
{
  if (!renderQueue.empty())
  {
    selectScene(renderQueue[0].scene, renderQueue[0].camPos);
    renderQueue.erase(renderQueue.begin());
  }
  scene->prepareForTracer();

  std::string fileName = "(" + scene->getSceneName() + "." + std::to_string(currScene.camPos) + ")";
  std::string pathSave = "../results/" + batchName;

  for (auto tracer : tracersSet)
  {
    std::string pathCurr = "../results/" + tracer->getTracerName();
    tracer->setImagePath(pathCurr, pathSave, fileName);
    tracer->initRender();
    std::cout << "Run " << tracer->getTracerName() << " tracer" << std::endl;
  }

  if (tracersSet.empty())
    std::cout << "Warning: no tracers were selected!" << std::endl;
  else
    isTraceRun = true;
}

void runTrace()
{
  int activeTracersQty = 0;
  for (auto tracer : tracersSet)
    if (!tracer->isReadyToFinish() || !isAutoStop) activeTracersQty++;

  if (activeTracersQty > 0)
  {
    auto start = std::chrono::system_clock::now();

    if (isParallel)
    {
      std::vector<std::thread> threads;
      threads.reserve(maxThreadsNum);

      int numThreads = maxThreadsNum / activeTracersQty;

      for (auto tracer : tracersSet)
        if (!tracer->isReadyToFinish() || !isAutoStop)
          for (auto i = 0; i < numThreads; i++)
            threads.emplace_back(std::thread(UpdateTracer, tracer));

      for (auto &thread : threads)
        thread.join();
    }
    else
    {
      for (auto tracer : tracersSet)
        if (!tracer->isReadyToFinish() || !isAutoStop)
          tracer->updateRender();
    }

    for (auto tracer : tracersSet) tracer->generateImage();

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> passed = end-start;
    std::cout << "Frame time: " << passed.count() << " sec" << std::endl;
  }
  else
  {
    if (!renderQueue.empty())
      startTrace();
    else
    {
      isTraceRun = false;
      std::cout << "Render finished" << std::endl;
    }
  }
}

void setSaveStrategies(bool val)
{
  BaseTracer::saveStrategies = val;
  std::cout << "Save strategies mode is switched " << (val ? "ON" : "OFF") << std::endl;
}

void setRenderForLow(bool val)
{
  BaseTracer::renderForLow  = val;
  std::cout << "Render for low is switched " << (val ? "ON" : "OFF") << std::endl;
}

void setRenderForGood(bool val)
{
  BaseTracer::renderForGood  = val;
  std::cout << "Render for good is switched " << (val ? "ON" : "OFF") << std::endl;
}

void setRenderForBest(bool val)
{
  BaseTracer::renderForBest  = val;
  std::cout << "Render for best is switched " << (val ? "ON" : "OFF") << std::endl;
}

// !!! This part of the program had to be removed to ensure compilation on the University's machines
// Explanation can be found in the report
// GLFW & OpenGL ---------------------------------------------------------------

// GLFWwindow * window = nullptr;
// Quad *       quad   = nullptr;

// void key_callback(GLFWwindow * window, int key, int scancode, int action, int mods)
// {
//   if (action != GLFW_PRESS) return;
//
//   switch (key)
//   {
//     case GLFW_KEY_W:     camera->moveVerBy(-0.1f);   break;
//     case GLFW_KEY_S:     camera->moveVerBy( 0.1f);   break;
//     case GLFW_KEY_D:     camera->moveHorBy(-0.1f);   break;
//     case GLFW_KEY_A:     camera->moveHorBy( 0.1f);   break;
//
//     case GLFW_KEY_Q:     camera->moveFwdBy( 0.1f);   break;
//     case GLFW_KEY_E:     camera->moveFwdBy(-0.1f);   break;
//
//     case GLFW_KEY_UP:    if ((mods & GLFW_MOD_SHIFT) > 0) camera->moveDirBy(-0.1f);  else camera->turnVerBy(-1.0f);  break;
//     case GLFW_KEY_DOWN:  if ((mods & GLFW_MOD_SHIFT) > 0) camera->moveDirBy( 0.1f);  else camera->turnVerBy( 1.0f);  break;
//     case GLFW_KEY_RIGHT: if ((mods & GLFW_MOD_SHIFT) > 0) camera->moveSideBy( 0.1f); else camera->turnHorBy( 1.0f);  break;
//     case GLFW_KEY_LEFT:  if ((mods & GLFW_MOD_SHIFT) > 0) camera->moveSideBy(-0.1f); else camera->turnHorBy(-1.0f);  break;
//
//     case GLFW_KEY_T:     setSaveStrategies(!BaseTracer::saveStrategies); break;
//     case GLFW_KEY_Y:     setRenderForLow(!BaseTracer::renderForLow);     break;
//     case GLFW_KEY_U:     setRenderForGood(!BaseTracer::renderForGood);   break;
//     case GLFW_KEY_I:     setRenderForBest(!BaseTracer::renderForBest);   break;
//
//     case GLFW_KEY_O:     setAutoStopMode(!isAutoStop); break;
//     case GLFW_KEY_P:     setParallelMode(!isParallel); break;
//
//     case GLFW_KEY_Z: selectTracer(ttPath,  !isTracer(ttPath));  break;
//     case GLFW_KEY_X: selectTracer(ttBDir,  !isTracer(ttBDir));  break;
//     case GLFW_KEY_C: selectTracer(ttMLT,   !isTracer(ttMLT));   break;
//     case GLFW_KEY_V: selectTracer(ttSMMpt, !isTracer(ttSMMpt)); break;
//     case GLFW_KEY_B: selectTracer(ttSMMbd, !isTracer(ttSMMbd)); break;
//     case GLFW_KEY_M: selectTracer(ttAll, true);  break;
//
//
//     case GLFW_KEY_ENTER:
//       initBatch();
//       startTrace();
//       break;
//
//     case GLFW_KEY_TAB:
//       isTraceRun = false;
//       break;
//
//     case GLFW_KEY_0:
//     case GLFW_KEY_1:
//     case GLFW_KEY_2:
//     case GLFW_KEY_3:
//     case GLFW_KEY_4:
//     case GLFW_KEY_5:
//     case GLFW_KEY_6:
//     case GLFW_KEY_7:
//     case GLFW_KEY_8:
//     case GLFW_KEY_9:
//       if ((mods & GLFW_MOD_SHIFT) > 0)
//         selectCamPos(currScene.scene, key - GLFW_KEY_0);
//       else
//         selectScene(key - GLFW_KEY_0, 0);
//       break;
//
//     case GLFW_KEY_EQUAL:
//       addAllScenesToQueue();
//       setAutoStopMode(true);
//       setParallelMode(true);
//       break;
//   }
// }

// int initOpenGL()
// {
//   // Initiate glwf to handle windows
//   if( !glfwInit() )
//   {
//     fprintf( stderr, "Failed to initialize GLFW\n" );
//     return -1;
//   }
//
//   // Set window settings
//   glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
//   glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
//   glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
//   glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // Needed for app to work on Mac
//   glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Setting core OpenGl
//
//   window = glfwCreateWindow(GlobalWidth * 3 / 2, GlobalHeight * 3 / 2, "All the tracing", nullptr, nullptr);
//   if (window == nullptr)
//   {
//     fprintf( stderr, "Failed to open GLFW window.\n" );
//     glfwTerminate();
//     return -1;
//   }
//
//   glfwMakeContextCurrent(window);
//   glfwSetKeyCallback(window, key_callback);
//
//   int windowWidth, windowHeight;
//   glfwGetWindowSize(window, &windowWidth, &windowHeight);
//
//   glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);
//
//   // Initialize GLEW and check that it was initialised
//   glewExperimental = true;
//   if (glewInit() != GLEW_OK)
//   {
//     fprintf(stderr, "Failed to initialize GLEW\n");
//     return -1;
//   }
//
//   // return OK
//   return 0;
// };

// void stopOpenGL()
// {
// // Close OpenGL window and terminate GLFW
//   glfwTerminate();
// }

// void updateOpenGL()
// {
//   glfwPollEvents();
//
//   glEnable(GL_DEPTH_TEST);
//   glEnable(GL_FRAMEBUFFER_SRGB);
//   glDepthFunc(GL_LESS);
//   glClear(GL_DEPTH_BUFFER_BIT);
//
//   glClearColor(0.45f, 0.30f, 0.60f, 1.00f);
//   glClear(GL_COLOR_BUFFER_BIT);
// }

// void renderOpenGL()
// {
//   if (isTraceRun)
//   {
//     BaseTracer * tracer = (!tracersSet.empty() ? *tracersSet.rbegin() : nullptr);
//
//     if (tracer != nullptr)
//     {
//       //#TODO choose type of render RGB or HDR
//       //quad->renderImage(tracer->getImageRGB(), tracer->getImageSize());
//       quad->renderImage(tracer->getImageHDR(), tracer->getSizeInPixels());
//     }
//   }
//
//   int display_w, display_h;
//   glfwGetFramebufferSize(window, &display_w, &display_h);
//   glViewport(0, 0, display_w, display_h);
//   glfwSwapBuffers(window);
// }

// bool isDoneOpenGL()
// {
//   // Check if the ESC key was pressed or the window was closed
//   return glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
//          glfwWindowShouldClose(window) == 0;
// }



// Help ------------------------------------------------------------------------

void showHelp()
{
  std::cout << std::endl;
  std::cout << "  -h - Show this help" << std::endl;
  std::cout << std::endl;
  std::cout << "  -nw - Run without window" << std::endl;
  std::cout << std::endl;
  std::cout << "  -path  - Enable Path tracer" << std::endl;
  std::cout << "  -bdir  - Enable Bidirectional tracer" << std::endl;
  std::cout << "  -mlt   - Enable MLT tracer" << std::endl;
  std::cout << "  -smmpt - Enable SMMCLT Path based tracer" << std::endl;
  std::cout << "  -smmbd - Enable SMMCLT BDir based tracer" << std::endl;
  std::cout << "  -tall  - Enable All tracers" << std::endl;
  std::cout << std::endl;
  std::cout << "  -p  - Choose parallel mode" << std::endl;
  std::cout << "  -as - Enable autostop" << std::endl;
  std::cout << std::endl;
  std::cout << "  -ss - Save strategy images" << std::endl;
  std::cout << "  -sl - Save low quality image" << std::endl;
  std::cout << "  -sg - Save good quality image" << std::endl;
  std::cout << "  -sb - Save best quality image" << std::endl;
  std::cout << std::endl;
  std::cout << "  0.0 .. 8.9 - Select scene and camera position for render" << std::endl;
  std::cout << "  ALL - Put all scenes in queue and render them all" << std::endl;
  std::cout << std::endl;
  std::cout << "  Available scenes:" << std::endl;
  for (auto item = camPosForScene.rbegin(); item != camPosForScene.rend(); item++)
  {
    // Skip[ 0 scene which is for test only
    if (item->first == 0) continue;

    std::cout << "    " << item->first << ".[";

    // Add scene and all camera pos to queue
    for (int i = 0; i <  item->second.size(); i++)
      std::cout << i << (i <  item->second.size() - 1 ? ", " : "");

    std::cout << "]" << std::endl;
  }
  std::cout << std::endl;
}


// Main ------------------------------------------------------------------------

int main(int argc, char *argv[])
{
  // Parse command line parameters
  for (int i = 0; i < argc; i++)
  {
    const std::string param(argv[i]);

    // if (param == "-nw") setWindowMode(false);


  }


  // Creating a window for the app
  // if (isWindow)
  // {
  //   int result = initOpenGL();
  //   if (0 != result) return result;
  //
  //   // Set up Dear Imgui
  //   if (isImGUI)
  //   {
  //     result = initImGUI();
  //     if (0 != result) return result;
  //   }
  //
  //   Scene::isOpenGLEnabled  = true;
  //   Object::isOpenGLEnabled = true;
  //
  //   quad = new Quad();
  // }


  // Init tracers
  camera = new Camera(glm::vec3(0.0f), glm::vec2(GlobalWidth, GlobalHeight), glm::radians(60.0f));
  initCamPos();

  scene = new Scene(*camera);
  scene->generateTestMeshes();

  tracersMap.emplace(ttPath,  new PathTracer(*scene, *camera));
  tracersMap.emplace(ttBDir,  new BDirTracer(*scene, *camera));
  tracersMap.emplace(ttMLT,   new MLTTracer(*scene, *camera));
  tracersMap.emplace(ttSMMpt, new SMMCLTracer(*scene, *camera, smmPathBased));
  tracersMap.emplace(ttSMMbd, new SMMCLTracer(*scene, *camera, smmBDirBased));


  // Parse command line parameters
  for (int i = 0; i < argc; i++)
  {
    const std::string param(argv[i]);

    if (param == "-h")  showHelp();

    if (param == "-path")  selectTracer(ttPath,  true);
    if (param == "-bdir")  selectTracer(ttBDir,  true);
    if (param == "-mlt")   selectTracer(ttMLT,   true);
    if (param == "-smmpt") selectTracer(ttSMMpt, true);
    if (param == "-smmbd") selectTracer(ttSMMbd, true);
    if (param == "-tall")  selectTracer(ttAll,   true);

    if (param == "-p")    setParallelMode(true);
    if (param == "-as")   setAutoStopMode(true);

    if (param == "-ss")   setSaveStrategies(true);
    if (param == "-sl")   setRenderForLow(true);
    if (param == "-sg")   setRenderForGood(true);
    if (param == "-sb")   setRenderForBest(true);

    if (param == "ALL") addAllScenesToQueue();

    parseSceneSelectParam(param);
  }

  // Output warnings
  if (tracersSet.empty())
  {
    std::cout << "Error: tracer should be selected: -path, -bdir, -mlt -smmlt" << std::endl;
    return 1;
  }

  if (!isAutoStop)
  {
    std::cout << "Warning: auto stop is not selected, render will never end: -as" << std::endl;
  }

  if (!BaseTracer::renderForLow && !BaseTracer::renderForGood && !BaseTracer::renderForBest)
  {
    std::cout << "Warning: render until low, good or best image isn't selected, tracers will newer stop: -fl, -fg, -fb" << std::endl;
  }

  if (renderQueue.empty())
  {
    std::cout << "Error: there are no scenes selected for render, stop" << std::endl;
    return 0;
  }

  // Start tracing
  initBatch();
  startTrace();


  // Main Cycle
  for (bool isRunning = true; isRunning; )
  {
    // if (isWindow)
    // {
    //   updateOpenGL();
    //   scene->render();
    // }

    // Rendering geometries
    if (isTraceRun) runTrace();

    // if (isWindow)
    // {
    //   renderOpenGL();
    //   isRunning = isDoneOpenGL();
    // }
    // else
    isRunning = isTraceRun;
  }


  // Shutdown and clean up

  // delete object;
  // delete quad;
  delete camera;
  delete scene;

  for (auto & tracer : tracersMap)
    delete tracer.second;

  // if (isWindow)
  //   stopOpenGL();

  return 0;
}
