

#include <ray.hpp>


Ray::Ray()
{
  distance = FLT_MAX;
  startSurf = nullptr;
};


Ray::Ray(const glm::vec3 & ori, const glm::vec3 & dir)
{
  origin = ori;
  direction = glm::normalize(dir);

  distance = FLT_MAX;
  startSurf = nullptr;
}


Ray::Ray(const IntersectionPoint & point, const glm::vec3 & dir)
{
  origin = point.getPoint();
  direction = glm::normalize(dir);

  distance = FLT_MAX;
  startSurf = point.getTriangle();
}


IntersectionPoint Ray::getIntersection() const
{
  return IntersectionPoint(isectPoint, isectSurf);
}


void Ray::setIntersection(const Triangle * t, const glm::vec3 & p, float d)
{
  distance   = d;
  isectPoint = p;
  isectSurf  = t;
}
