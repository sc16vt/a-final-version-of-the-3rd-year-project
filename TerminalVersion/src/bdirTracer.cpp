
#include <bdirTracer.hpp>


BDirTracer::BDirTracer(const Scene & sc, const Camera & cam) : PathTracer(sc, cam)
{
  // Set tracer parameters
  tracerName      = "BDir";
  maxPathLength   = 14;       // Max light path length of path
  heuristicType   = Balanced; // Heuristic type for MIS

  samplesLowQuality  = 1;   // Num of samples for low quality image
  samplesGoodQuality = 128; // Num of samples for good quality image
  samplesBestQuality = 512; // Num of samples for best quality image

  // Init strategy buffers
  strategyIDs.clear();
  for (int c = 0; c < maxPathLength + 1; c++)
    for (int l = 0; l < maxPathLength + 1; l++)
    {
      strategyIDs.emplace(makeStrategyID(c, l, false), makeStrategyName(c, l, false));
      strategyIDs.emplace(makeStrategyID(c, l, true),  makeStrategyName(c, l, true));
    }

  // Set camera buffer scale to adjust brightness
  cameraBuffer.setScaleFactor(1.0f);
};


int BDirTracer::makeStrategyID(int c, int l, bool w) const
{
  return ((c * 100) + l) * 10 + (w ? 1 : 0) + 10;
}


std::string BDirTracer::makeStrategyName(int c, int l, bool w) const
{
  return std::string("(") + std::to_string(c) + "-" + std::to_string(l) + ")" + (w ? "w" : "");
}


void BDirTracer::tracePathFromLight(LightPath & path, int length)
{
  // Create random light point and random ray from it
  IntersectionPoint lightPoint;
  Ray rayFromLight;
  sampleLightPoint(lightPoint, rayFromLight);

  // Init path insert position to add insert points before light
  path.setPathDir(Reverse);
  path.setCurrentPosToEnd();

  // Add light point as final
  path.insertPoint(lightPoint);

  // Trace path from light
  findPath(path, rayFromLight, length);
}


void BDirTracer::traceBiDirPath(LightPath & path, int lcam, int llight)
{
  // Trace path from Camera
  tracePathFromCam(path, lcam);

  // Special case, we directly hit the light when tracing from Cam
  if (path.isDirectCamToLight()) return;

  // Special case, we hit the light when tracing from Cam
  if (path.rbegin()->isL()) return;

  // If the endpoint was hit, remove it to connect path with prev point
  if (path.rbegin()->isEnd() && !path.rbegin()->isE())
    path.deletePoint((int)path.size() - 1);

  // Trace path from random point of random light
  tracePathFromLight(path, llight - 1);

  // We need at least 1 Cam and 1 Light point, so path less then 2 points is should not appear
  assert(path.size() >= 2);

  // If we hit background or other end point during trace, remove this point
  if (path.getLastInsertPos() + 1 < path.size() && path.getLastInsertPoint().isEnd())
    path.deletePoint(path.getLastInsertPos());
}


bool BDirTracer::runOneStrategy(LightPath & path, const glm::vec2 & crd)
{
  // For bidirectional paths
  if (!path.isSingle())
  {
    // Connect two sub paths
    if (!findConnection(path)) return false;

    // Update screen coords and check if we are still in the same pixel bounds
    if (!path.updateScreenCrd(camera)) return false;
  }

  // Compute sub path Radiance and Weight
  path.computePath();

  // Check for obstructed sub path
  if (path.isBlack()) return false;

  return true;
}


void BDirTracer::tracePixel(const glm::vec2 & crd)
{
  // Create an initial path (a path through sample point on the screen (in the pixel) towards one of the light sources)
  LightPath path(crd, maxPathLength + 1);

  // Trace bidirectional path
  traceBiDirPath(path, maxPathLength / 2, maxPathLength / 2);

  // If path consists only of one subpath (the light was hit when tracing cam path)
  if (path.isSingle())
  {
    // Compute sub path Radiance and Weight
    path.computePath();

    // Store result in strategy buffers
    if (saveStrategies)
    {
      addColorToStrategy(makeStrategyID((int)path.size() - 1, 0, false), path.getScreenCrd(), path.getRadiance());
      addColorToStrategy(makeStrategyID((int)path.size() - 1, 0, true),  path.getScreenCrd(), path.getRadianceW(heuristicType));
    }

    // Store result in color buffer
    addColorToCamera(path.getScreenCrd(), path.getRadianceW(heuristicType));
  }
  else
    // Scan all over connections
    for (int c = 0; c < std::min(path.getLastInsertPos(), (int)path.size()); c++)
      for (int l = path.getLastInsertPos(); l < path.size(); l++)
      {
        LightPath sub = path;
        sub.makeStrategy(c, l);

        // Run strategy and skip if it failed
        if (!runOneStrategy(sub, path.getScreenCrd())) continue;

        // Strategy sub path sizes
        int sc = (int)sub.getLastInsertPos();
        int sl = (int)sub.size() - sub.getLastInsertPos();

        // Store result in strategy buffers
        if (saveStrategies)
        {
          addColorToStrategy(makeStrategyID(sc, sl, false), sub.getScreenCrd(), sub.getRadiance());
          addColorToStrategy(makeStrategyID(sc, sl, true),  sub.getScreenCrd(), sub.getRadianceW(heuristicType));
        }

        // Store result in color buffer
        addColorToCamera(sub.getScreenCrd(), sub.getRadianceW(heuristicType));
      }

  // Add samples counter
  for (auto & id : strategyIDs)
    addCountToStrategy(id.first, crd, 1.0f);

  addCountToCamera(crd, 1.0f);
}
