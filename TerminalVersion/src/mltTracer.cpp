
#include <mltTracer.hpp>


enum StrategyID
{
  sidDencity = 0,
};


MLTTracer::MLTTracer(const Scene & sc, const Camera & cam) : BDirTracer(sc, cam)
{
  // Set tracer parameters
  tracerName      = "MLT";
  maxPathLength   = 14;      // Max light path length of path

  maxNumOfSeeds   = 50;      // Num of iterations trying to find valid seed
  qtyMutPerPixel  = 32;      // Num of mutations in average per pixel

  seedsForAvrLum  = 99000;   // Number of samples fo compute average luminance

  samplesLowQuality  = 1;    // Num of samples for low quality image
  samplesGoodQuality = 64;   // Num of samples for good quality image
  samplesBestQuality = 256;  // Num of samples for best quality image

  // Init mutation types distribution
  mutations = { NewPath, Pixel, Caustic, CamRay };
  mutDistr  = std::discrete_distribution<int>({ 1.0f, 1.0f, 1.0f, 1.0f });

  // Init strategy buffers
  strategyIDs.clear();
  strategyIDs.emplace(sidDencity, "(density)");

  // Set camera buffer scale to adjust brightness
  cameraBuffer.setScaleFactor(1.0f);
}


void MLTTracer::sampleInitialPath(LightPath & path, const glm::vec2 & crd)
{
  // Sample new path
  int pathLen = sampler.pathLengthSample(maxPathLength);
  int camLen  = sampler.pathPositionSample(pathLen - 2) + 1;

  path = LightPath(crd, pathLen + 1);

  // Trace bidirectional path
  traceBiDirPath(path, camLen, pathLen - camLen);
}


float MLTTracer::mutateNewPath(LightPath & path)
{
  const float prevLum = path.getLuminanceW(heuristicType);

  // Sample new path
  sampleInitialPath(path, sampler.screenPointSample(imageSize));

  // Run strategy and skip if it failed
  if (!runOneStrategy(path, path.getScreenCrd())) return 0.0;

  const float nextLum = path.getLuminanceW(heuristicType);

  // Compute acceptance probability
  return std::min(1.0f, (prevLum != 0.0f ? nextLum / prevLum : 1.0f));
}


float MLTTracer::mutatePixel(LightPath & path, float r1, float r2)
{
  const float prevLum = path.getLuminanceW(heuristicType);

  // Sample new path
  sampleInitialPath(path, sampler.mutPixelOffsetSample(path.getScreenCrd(), imageSize, r1, r2));

  // Run strategy and skip if it failed
  if (!runOneStrategy(path, path.getScreenCrd())) return 0.0;

  const float nextLum = path.getLuminanceW(heuristicType);

  // Compute acceptance probability
  return std::min(1.0f, (prevLum != 0.0f ? nextLum / prevLum : 1.0f));
}


float MLTTracer::mutateCaustic(LightPath & path, float t1, float t2)
{
  // Check if path has a good type for this mutation
  int mutIdx = path.isE_D_Sm_DorL();
  if (mutIdx == -1) return 0.0f;

  if (path[mutIdx].isLd()) return 0.0f;

  // Declare lambda for T factor calculation
  auto calcA = [&](LightPath & path)
  {
    glm::vec3 cVec = path[1].getPoint() - path[0].getPoint();
    float cLen = glm::length(cVec);

    float cos1 = fabsf(glm::dot(cVec, camera.getDirection()) / cLen);
    float cos2 = fabsf(glm::dot(cVec, path[1].getNormal()) / cLen);

    return path.getLuminanceW(heuristicType) * (cLen * cLen) * (cos1 * cos1 * cos1) / (cos2 * cos2);
  };

  // Compute T(y|x) factor for acceptance probability calculation
  const float prevA = calcA(path);

  // Create new mutated path
  LightPath mut(path.getScreenCrd(), path.size());

  // Init path insert position to add insert points before light
  mut.setPathDir(Reverse);
  mut.setCurrentPosToEnd();

  // Add light or diffuse point as final
  mut.insertPoint(path[mutIdx]);

  // Compute new perturbated ray from light
  glm::vec3 oldDir = glm::normalize(path[mutIdx - 1].getPoint() - path[mutIdx].getPoint());
  Ray newRay = Ray(path[mutIdx], sampler.mutRayOffsetSample(oldDir, t1, t2));

  // Trace new path from light
  if (!findPath(mut, newRay, mutIdx - 1)) return 0.0f;

  // Trying to replace old path with new points, points type should be the same
  for (auto i = 0; i < mutIdx - 1; i++)
  {
    if (path[i + 1].getType() != mut[i].getType()) return 0.0f;
    path[i + 1] = mut[i];
  }

  // Set new connection position to D point
  path.setPathDir(Reverse);
  path.setCurrentPos(1);

  // Connect two sub paths
  if (!findConnection(path)) return 0.0f;

  // Update screen coords and check if we are still in screen bounds
  if (!path.updateScreenCrd(camera)) return false;

  // Compute path and check if it is obstructed
  path.computePath();
  if (path.isBlack()) return 0.0f;

  // Compute T(x|y) factor for acceptance probability calculation
  const float nextA = calcA(path);

  // Compute acceptance probability
  return std::min(1.0f, (prevA != 0.0f ? nextA / prevA : 1.0f));
}


float MLTTracer::mutateCamRay(LightPath & path, float t1, float t2)
{
  // Mutations starting at the camera

  // Check if path has a good type for this mutation
  int mutIdx = path.isE_Sm_D_DorL();
  if (mutIdx == -1) return 0.0f;

  //#TODO DirLight should be coded separately
  if (path[mutIdx].isLd()) return 0.0f;

  // Declare lambda for T factor calculation
  auto calcA = [&](LightPath & path)
  {
    glm::vec3 cVec = path[mutIdx].getPoint() - path[mutIdx - 1].getPoint();
    float cLen = glm::length(cVec);

    float cos1 = fabsf(glm::dot(cVec, path[mutIdx    ].getNormal()) / cLen);
    float cos2 = fabsf(glm::dot(cVec, path[mutIdx - 1].getNormal()) / cLen);

    return path.getLuminanceW(heuristicType) * (cLen * cLen) / (cos1 * cos2);
  };

  // Compute T(y|x) factor for acceptance probability calculation
  const float prevA = calcA(path);

  // Create new mutated path
  LightPath mut(path.getScreenCrd(), path.size());

  // Init path insert position to add insert points before light
  mut.setPathDir(Forward);
  mut.setCurrentPosToEnd();

  // Add light or diffuse point as final
  mut.insertPoint(path[0]);

  // Compute new perturbated ray from light
  glm::vec3 oldDir = glm::normalize(path[1].getPoint() - path[0].getPoint());
  Ray newRay = Ray(path[mutIdx], sampler.mutRayOffsetSample(oldDir, t1, t2));

  // Trace new path from light
  if (!findPath(mut, newRay, mutIdx - 1)) return 0.0f;

  // Trying to replace old path with new points, points type should be the same
  for (auto i = 0; i < mutIdx - 1; i++)
  {
    if (path[i].getType() != mut[i].getType()) return 0.0f;
    path[i] = mut[i];
  }

  // Set new connection position to D point
  path.setPathDir(Reverse);
  path.setCurrentPos(mutIdx);

  // Connect two sub paths
  if (!findConnection(path)) return 0.0f;

  // Update screen coords and check if we are still in screen bounds
  if (!path.updateScreenCrd(camera)) return false;

  // Compute path and check if it is obstructed
  path.computePath();
  if (path.isBlack()) return 0.0f;

  // Compute T(x|y) factor for acceptance probability calculation
  const float nextA = calcA(path);

  // Compute acceptance probability
  return std::min(1.0f, (prevA != 0.0f ? nextA / prevA : 1.0f));
}


float MLTTracer::mutate(LightPath & path, MutationType & mutType)
{
  float accProb = 0.0f;

  // Randomly pick between different mutation strategies
  switch (static_cast<MutationType>(mutations[sampler.mltMutationSample(mutDistr)]))
  {
    case NewPath: accProb = mutateNewPath(path); break;
    case Pixel:   accProb = mutatePixel(path, 0.1f, imageSize.x * 0.25f); break;
    case Caustic: accProb = mutateCaustic(path, 0.0001, 0.1); break;
    case CamRay:  accProb = mutateCamRay(path, 0.0001, 0.1); break;

    default: break;
  }

  return accProb;
}


void MLTTracer::computeAverageLuminance()
{
  // Find average luminance if we are tracing first sample
  float samplesCount = 0.0f;

  for (int i = 0; i < seedsForAvrLum; i++)
  {
    // Sample new path
    LightPath path;
    sampleInitialPath(path, sampler.screenPointSample(imageSize));

    // Scan all over connections
    for (int c = 0; c < path.getLastInsertPos(); c++)
      for (int l = path.getLastInsertPos(); l < path.size(); l++)
      {
        LightPath sub = path;
        sub.makeStrategy(c, l);

        // Run strategy and skip if it failed
        if (runOneStrategy(sub, path.getScreenCrd()))
          // Add value to average lum
          averageLuminance += sub.getLuminanceW(heuristicType);
      }

    samplesCount += 1.0f;
  }

  averageLuminance /= remap0to1(samplesCount);
}


void MLTTracer::runMarkovChain(LightPath & path)
{
  LightPath & currPath = path;

  // Save in strategy buffers
  if (saveStrategies)
    addColorToStrategy(sidDencity, currPath.getScreenCrd(), glm::vec3(0.1f));

  // For the number of mutations per chain:
  for(auto m = 0; m < varea(imageSize); m++)
  {
    LightPath nextPath = currPath;

    // Propose a new chain. If not successful
    // (the connection between changed parts was obstructed, go to a new iteration of the loop)
    MutationType mutType;
    float accProb = mutate(nextPath, mutType);

    // Skip if new path to long
    if (nextPath.size() > maxPathLength) accProb = 0.0f;

    // Store result in buffer
    glm::vec3 currC = (1.0f - accProb) * averageLuminance * currPath.getRadianceN();
    glm::vec3 nextC =         accProb  * averageLuminance * nextPath.getRadianceN();

    if (accProb > 0.0f)
      addColorToCamera(nextPath.getScreenCrd(), nextC);

    addColorToCamera(currPath.getScreenCrd(), currC);

    // Randomly with prob of acceptance accept or reject the new Path
    if (sampler.pathAcceptanceSample() < accProb)
      // The new path is accepted and mutated further
      currPath = nextPath;

    // Save in strategy buffers
    if (saveStrategies)
      addColorToStrategy(sidDencity, currPath.getScreenCrd(), glm::vec3(0.1f));
  }

  // Add samples counter
  addCountToStrategy(sidDencity, 1.0f);
  addCountToCamera(1.0f);
}


void MLTTracer::prepareRender()
{
  computeAverageLuminance();
}


void MLTTracer::updateSample()
{
  LightPath path;

  // If there are no items in queue create the new one with a seed from BDir
  int counter = maxNumOfSeeds;
  do
  {
    counter--;

    const glm::vec2 & pixel = sampler.screenPointSample(imageSize);
    path = LightPath(pixel, maxPathLength + 1);

    // Sample new path
    sampleInitialPath(path, sampler.screenPointSample(imageSize));

    // Run strategy and skip if it failed
    if (!runOneStrategy(path, path.getScreenCrd())) continue;

    break;
  }
  while (counter > 0);

  // Skip if we haven't found valid paths
  if (counter <= 0) return;

  // Run Markov chain
  for (int i = 0; i < qtyMutPerPixel; i++)
    runMarkovChain(path);
}
