

#include <lightPath.hpp>


void LightPath::insertPoint(const IntersectionPoint & point)
{
  if (insertPos < 0) insertPos = 0;

  if (insertPos >= size())
    emplace_back(point);
  else
    insert(begin() + insertPos, point);

  if (Forward == insertDir) insertPos++;
}

// Delete point and correct current insert pos if needed
void LightPath::deletePoint(int index)
{
  erase(begin() + index);
  if (index < insertPos) insertPos--;
}

// Create new cut path from the current one
void LightPath::makeStrategy(int c, int l)
{
  if (c + 1 <= l - 1)
    erase(begin() + c + 1, begin() + l);
  setCurrentPos(c + 1);
}


float LightPath::findPrevRefraction()
{
  refractStack.clear();
  refractStack.emplace_back(1.0f);

  for (int i = 0; i < insertPos; i++)
  {
    if ((*this)[i].getRefractionDir() == Inside)
      refractStack.emplace_back((*this)[i].getMaterial().getRefractionCoef());
    if ((*this)[i].getRefractionDir() == Outside)
      refractStack.pop_back();

    // Return -1 as sign that path is broken
    if (refractStack.empty()) return -1;
  }

  refractStack.pop_back();

  return (refractStack.empty() ? 1.0f : *refractStack.rbegin());
}


float LightPath::findNextRefraction()
{
  refractStack.clear();
  refractStack.emplace_back(1.0f);

  for (int i = (int)size() - 1; i >= insertPos; i--)
  {
    if ((*this)[i].getRefractionDir() == Inside)
      refractStack.emplace_back((*this)[i].getMaterial().getRefractionCoef());
    if ((*this)[i].getRefractionDir() == Outside)
      refractStack.pop_back();

    // Return -1 as sign that path is broken
    if (refractStack.empty()) return -1;
  }

  refractStack.pop_back();

  return (refractStack.empty() ? 1.0f : *refractStack.rbegin());
}


int LightPath::isE_D_Sm_DorL()
{
  if (size() < 4) return -1;

  int idx = 0;

  if (!(*this)[idx++].isE()) return -1;        // E

  if (!(*this)[idx++].isD()) return -1;        // D

  while ((*this)[idx].isS()) idx++;            // Sm
  if (!(*this)[idx].isPrevDelta()) return -1;

  if (!(*this)[idx].isL() &&
      !(*this)[idx].isD()) return -1;          // DorL

  return idx;
};

int LightPath::isE_Sm_D_DorL()
{
  if (size() < 4) return -1;
  int idx = 0;

  if (!(*this)[idx++].isE())  return -1;       // E

  while ((*this)[idx].isS()) idx++;            // Sm
  if (!(*this)[idx].isPrevDelta()) return -1;

  if (!(*this)[idx++].isD()) return -1;        // D

  if (!(*this)[idx].isL() &&
      !(*this)[idx].isD()) return -1;          // DorL

  return idx;
}


// Radiance and probability calculations

void LightPath::computePath()
{
  // Initialize
  radiance = glm::vec3(1.0f);

  wBalance = 0.0f;
  wPower2  = 0.0f;
  wMaximum = 0.0f;

  float pathPDF = 1.0f;

  // Run all over points in the path part from Cam
  for (auto i = 0; i < std::min(insertPos, (int)size()); i++)
  {
    radiance *= (*this)[i].getPrevBSDF() / (*this)[i].getPrevPDFpa();
    pathPDF  *= (*this)[i].getPrevPDFpa();
  }

  // Run all over points in the path part from Light
  for (auto i = insertPos; i < size(); i++)
  {
    radiance *= (*this)[i].getNextBSDF() / (*this)[i].getNextPDFpa();
    pathPDF  *= (*this)[i].getNextPDFpa();
  }

  if (!isSingle())
    radiance *= getLastInsertPoint().getPrevG();


  // Check path
  if (isBlack()) return;

  // Compute weight
  wBalance = 1.0f;
  wPower2  = 1.0f;
  wMaximum = 1.0f;

  // Run all over points in the path part from Cam
  float currPc = 1.0f;
  for (auto i = std::min(insertPos, (int)size()) - 1; i >= 0; i--)
  {
    currPc *= (*this)[i].getNextPDFsa() / (*this)[i].getPrevPDFsa();

    // Handling Delta function in probability calculation for reflect / refract points
    if (!(*this)[i].isDelta() && !(*this)[i].isPrevDelta())
    {
      wBalance += currPc;
      wPower2  += currPc * currPc;
      wMaximum  = pathPDF > currPc ? wMaximum : 0.0f;
    }
  }

  // Run all over points in the path part from Light
  float currPl = 1.0f;
  for (auto i = insertPos; i < size(); i++)
  {
    currPl *= (*this)[i].getPrevPDFsa() / (*this)[i].getNextPDFsa();

    // Handling Delta function in probability calculation for reflect / refract points
    if (!(*this)[i].isDelta() && !(*this)[i].isNextDelta())
    {
      wBalance += currPl;
      wPower2  += currPl * currPl;
      wMaximum  = pathPDF > currPl ? wMaximum : 0.0f;
    }
  }

  wBalance = 1.0f / wBalance;
  wPower2  = 1.0f / wPower2;
}

float LightPath::getWeight(HeuristicType ht) const
{
  switch (ht)
  {
    case Always1:  return 1.0f;
    case Balanced: return wBalance;
    case Power2:   return wPower2;
    case Maximum:  return wMaximum;
  }

  return 0.0f;
}
