
#include <outliersDetector.hpp>


int OutliersDetector::pointIndex(int buf, const glm::vec2 & crd) const
{
  return buf * varea(imageSize) + (int)crd.y * imageSize.x + (int)crd.x;
}


int OutliersDetector::fillIndex(int isample) const
{
  return (isample + 0) % 2;
}


int OutliersDetector::checkIndex(int isample) const
{
  return (isample + 1) % 2;
}


OutliersDetector::OutliersDetector()
{

}


void OutliersDetector::setParameters(int md, float sf)
{
  maxDepth = md;
  valScale = sf;
}

void OutliersDetector::initBuffer(const glm::ivec2 & size)
{
  imageSize = size;

  for (int p = 0; p < 2; p++)
  {
    valBuf[p].resize(size.x * size.y * maxDepth);
    for (auto & val : valBuf[p]) val = 0.0f;

    sppBuf[p].resize(size.x * size.y);
    for (auto & spp : sppBuf[p]) spp = 0.0f;
  }
}

void OutliersDetector::addToBuffer(const glm::vec2 & crd, int ipage, float value)
{
  std::lock_guard<std::mutex> lock(bufferMutex);

  if (isnan(value) || isinf(value)) return;

  float logValue = log2(1.0f + value * valScale);
  if (logValue >= (float)maxDepth - 1.0f) return;

  int j0 = (int)floorf(logValue);
  int j1 = j0 + 1;

  float w0 = logValue - floorf(logValue);
  float w1 = 1.0f - w0;

  assert((int)j1 < maxDepth);

  const int ifl = fillIndex(ipage);

  valBuf[ifl][pointIndex(j0, crd)] += w0;
  valBuf[ifl][pointIndex(j1, crd)] += w1;
  sppBuf[ifl][pointIndex(0,  crd)] += 1.0f;
}


float OutliersDetector::checkOutlier(const glm::vec2 & crd, int ipage, float value)
{
  std::lock_guard<std::mutex> lock(bufferMutex);

  if (isnan(value) || isinf(value)) return 0.0f;
  if (ipage <= 0) return 0.0f;

  float logValue = log2(1.0f + value * valScale);
  if (logValue >= (float)maxDepth - 1.0f) return 1.0f;

  int j0 = (int)floorf(logValue);
  int j1 = j0 + 1;

  float w0 = logValue - floorf(logValue);
  float w1 = 1.0f - w0;

  assert((int)j1 < maxDepth);

  const int ich = checkIndex(ipage);

  float c0  = valBuf[ich][pointIndex(j0, crd)];
  float c1  = valBuf[ich][pointIndex(j1, crd)];
  float spp = sppBuf[ich][pointIndex(0,  crd)];

  float tr = std::max((float)minSPL, spp / (float)maxDepth);
  float ch  = w0 * c0 + w1 * c1;

  return (ch < tr) ? 1.0f - ch / tr : 0.0f;
}


int OutliersDetector::getMaxBufferDepth() const
{
  return maxDepth;
}


float OutliersDetector::getValue(int buf, const glm::vec2 & crd)
{
  std::lock_guard<std::mutex> lock(bufferMutex);

  if (buf >= maxDepth) return 0.0f;

  const int ipx = pointIndex(buf, crd);
  return (valBuf[0][ipx] + valBuf[1][ipx]) / std::max(1.0f, (valBuf[0][ipx] + valBuf[1][ipx]));
}
