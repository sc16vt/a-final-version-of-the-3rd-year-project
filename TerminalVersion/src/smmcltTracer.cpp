
#include <smmcltTracer.hpp>


enum StrategyID
{
  sidOnlyBase  = 0,
  sidOnlyBaseW = 1,
  sidOnlyMlt   = 2,
  sidOnlyMltW  = 3,
  sidOutliers  = 4,

  sidOutlier   = 10,
};


SMMCLTracer::SMMCLTracer(const Scene & sc, const Camera & cam, SMMCLType tp) :
  MLTTracer(sc, cam)
{
  // Set tracer parameters
  typeOfSMMCL     = tp;
  maxPathLength   = 14;      // Max light path length of path

  switch (typeOfSMMCL)
  {
    case smmPathBased:
      tracerName      = "SMMpt";
      qtyMutPerPixel  = 128;

      samplesLowQuality  = 16;   // Num of samples for low quality image
      samplesGoodQuality = 256;  // Num of samples for good quality image
      samplesBestQuality = 1024; // Num of samples for best quality image
      break;

    case smmBDirBased:
      tracerName      = "SMMbd";
      qtyMutPerPixel  = 32;

      samplesLowQuality  = 1;    // Num of samples for low quality image
      samplesGoodQuality = 64;  // Num of samples for good quality image
      samplesBestQuality = 256;  // Num of samples for best quality image
      break;
  }

  // Init outliers detector
  outliersDetector.setParameters(8, 512.0f);

  // Init mutation types distribution
  mutations = { Pixel, Caustic, CamRay };
  mutDistr  = std::discrete_distribution<int>({ 1.0f, 1.0f, 1.0f });

  // Init strategy buffers
  strategyIDs.clear();

  strategyIDs.emplace(sidOnlyBase,  "(base)");
  strategyIDs.emplace(sidOnlyBaseW, "(base)w");
  strategyIDs.emplace(sidOnlyMlt,   "(mlt)");
  strategyIDs.emplace(sidOnlyMltW,  "(mlt)w");
  strategyIDs.emplace(sidOutliers,  "(outliers)");

  for (int i = 0; i < outliersDetector.getMaxBufferDepth(); i++)
    strategyIDs.emplace(sidOutlier + i, "(" + std::to_string(i) + ")");

  // Set camera buffer scale to adjust brightness
  cameraBuffer.setScaleFactor(1.0f);
}


void SMMCLTracer::runMarkovChain(const LightPath & path, float weight)
{
  LightPath currPath = path;

  weight /= (float)qtyMutPerPixel;

  // for the number of mutations per chain:
  for(auto m = 0; m < qtyMutPerPixel; m++)
  {
    LightPath nextPath = currPath;

    // Propose a new chain. If not successful
    // (the connection between changed parts was obstructed, go to a new iteration of the loop)
    MutationType mutType;
    float accProb = mutate(nextPath, mutType);

    // Skip if new path to long
    if (nextPath.size() > maxPathLength) accProb = 0.0f;

    // Store result in buffer
    glm::vec3 currC = (1.0f - accProb) * currPath.getRadianceN();
    glm::vec3 nextC =         accProb  * nextPath.getRadianceN();

    if (accProb > 0.0f)
    {
      // Save in strategy buffers
      if (saveStrategies)
      {
        addColorToStrategy(sidOnlyMlt,  nextPath.getScreenCrd(), nextC);
        addColorToStrategy(sidOnlyMltW, nextPath.getScreenCrd(), weight * nextC);
      }

      // Store result in color buffer
      addColorToCamera(nextPath.getScreenCrd(), weight * nextC);
    }

    // Save in strategy buffers
    if (saveStrategies)
    {
      addColorToStrategy(sidOnlyMlt,  currPath.getScreenCrd(), currC);
      addColorToStrategy(sidOnlyMltW, currPath.getScreenCrd(), weight * currC);
    }

    // Store result in color buffer
    addColorToCamera(currPath.getScreenCrd(), weight * currC);

    // Randomly with prob of acceptance accept or reject the new Path
    if (sampler.pathAcceptanceSample() < accProb)
      // The new path is accepted and mutated further
      currPath = nextPath;
  }
}


void SMMCLTracer::checkPathAndRunMlt(const LightPath & path, HeuristicType ht)
{
  int idxPage = std::min(numOfCurrentRun, numOfCurRunSample / outliersDetector.getMaxBufferDepth());

  // Add sample to outlier detector
  outliersDetector.addToBuffer(path.getScreenCrd(), idxPage, path.getLuminanceW(ht));

  // Check outlier detector
  float weight = outliersDetector.checkOutlier(path.getScreenCrd(), idxPage, path.getLuminanceW(ht));
  glm::vec3 baseColor = path.getRadianceW(ht);

  // Save in strategy buffers
  if (saveStrategies)
  {
    addColorToStrategy(sidOnlyBase,  path.getScreenCrd(), baseColor);
    addColorToStrategy(sidOnlyBaseW, path.getScreenCrd(), (1.0f - weight) * baseColor);

    for (int i = 0; i < outliersDetector.getMaxBufferDepth(); i++)
      addColorToStrategy(sidOutlier + i, path.getScreenCrd(), glm::vec3(outliersDetector.getValue(i, path.getScreenCrd())));
  }

  // Run MLT if outlier is detected
  glm::vec3 mltColor = glm::vec3(0.0f);
  if (weight > 0.0f)
  {
    // Run MLT for pixel
    runMarkovChain(path, weight * luminance(baseColor));

    // Save in strategy buffers
    if (saveStrategies)
      addColorToStrategy(sidOutliers, path.getScreenCrd(), glm::vec3(weight));
  }

  // Store result in color buffer
  addColorToCamera(path.getScreenCrd(), (1.0f - weight) * baseColor);
}


void SMMCLTracer::prepareRender()
{
  outliersDetector.initBuffer(imageSize);
}


void SMMCLTracer::updateSample()
{
  BaseTracer::updateSample();
}


void SMMCLTracer::tracePixelPath(const glm::vec2 & crd)
{
  // Create an initial path (a path through sample point on the screen (in the pixel) towards one of the light sources)
  LightPath path(crd, maxPathLength + 1);

  // Trace path from Camera
  tracePathFromCam(path, maxPathLength);

  // Skip if we haven't hit any light source (including background)
  if (path.getLastInsertPoint().isL())
  {
    // Update screen coords and check if we are still in screen bounds
    if (!path.updateScreenCrd(camera)) return;

    // Compute path Radiance and Weight
    path.computePath();

    // Check path with outlier detector and run MLT and add final color
    checkPathAndRunMlt(path, Always1);
  }
}

void SMMCLTracer::tracePixelBDir(const glm::vec2 & crd)
{
  // Create an initial path (a path through sample point on the screen (in the pixel) towards one of the light sources)
  LightPath path(crd, maxPathLength + 1);

  // Trace bidirectional path
  traceBiDirPath(path, maxPathLength / 2, maxPathLength / 2);

  // If path single and isn't consist of two parts (we hit the light when was tracing cam path)
  if (path.isSingle())
  {
    // Compute sub path Radiance and Weight
    path.computePath();

    // Check path with outlier detector and run MLT and add final color
    checkPathAndRunMlt(path, heuristicType);
  }
  else
    // Scan all over connections
    for (int c = 0; c < std::min(path.getLastInsertPos(), (int)path.size()); c++)
      for (int l = path.getLastInsertPos(); l < path.size(); l++)
      {
        LightPath sub = path;
        sub.makeStrategy(c, l);

        // Run strategy and skip if it failed
        if (!runOneStrategy(sub, path.getScreenCrd())) continue;

        // Check path with outlier detector and run MLT and add final color
        checkPathAndRunMlt(sub, heuristicType);
      }
}


void SMMCLTracer::tracePixel(const glm::vec2 & crd)
{
  switch (typeOfSMMCL)
  {
    case smmPathBased: tracePixelPath(crd); break; // Trace pixel based on Path tracer
    case smmBDirBased: tracePixelBDir(crd); break; // Trace pixel based on Path tracer
  }

  // Add samples counter
  for (auto & id : strategyIDs)
    addCountToStrategy(id.first, crd, 1.0f);

  addCountToCamera(crd, 1.0f);
}
