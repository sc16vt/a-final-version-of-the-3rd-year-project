
#include <quad.hpp>

#include "stb_image.h"
#include "stb_image_write.h"


Quad::Quad()
{
  std::vector<GLfloat> vertices(
  {
    // positions
     1.0f,   1.0f,   0.0f, //-2.0f,   // 0
     1.0f,  -1.0f,  -0.0f, //-2.0f,   // 1
    -1.0f,  -1.0f,  -0.0f, //-2.0f,   // 2
    -1.0f,   1.0f,  -0.0f, //-2.0f,   // 3

  });

  std::vector<GLfloat> uv(
  {
    // positions
    1.0f,  0.0f,    // 0
    1.0f,  1.0f,    // 1
    0.0f,  1.0f,    // 2
    0.0f,  0.0f,    // 3
  });

  std::vector<GLuint> indices(
  {
    0, 1, 3, // 0 triangle
    1, 2, 3,  // 1 triangle
  });

  // Attributes
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &TBO);
  glGenBuffers(1, &EBO);

  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(decltype(vertices)::value_type), vertices.data(), GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, TBO);
  glBufferData(GL_ARRAY_BUFFER, uv.size() * sizeof(decltype(uv)::value_type), uv.data(), GL_STATIC_DRAW);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
  glEnableVertexAttribArray(1);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(decltype(indices)::value_type), indices.data(), GL_STATIC_DRAW);

  number_of_elements = indices.size();

  program = new Shader("../shaders/vTracing.glsl", "../shaders/fTracing.glsl");
  program->use();
}

Quad::~Quad()
{
  glDeleteVertexArrays(1, &VAO);
  glDeleteBuffers(1, &VBO);
  glDeleteBuffers(1, &TBO);
  glDeleteBuffers(1, &EBO);
}


void Quad::renderImage(const std::vector<GLubyte> & tracerBuffer, const glm::ivec2 & size)
{
  program->use();
  setTextureFromArrayRGB(tracerBuffer, size.x, size.y, "screenTexture", 1);

  glBindVertexArray(VAO);

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glDrawElements(GL_TRIANGLES, number_of_elements, GL_UNSIGNED_INT, 0);
}


void Quad::renderImage(const std::vector<GLfloat> & tracerBuffer, const glm::ivec2 & size)
{
  program->use();
  setTextureFromArrayHDR(tracerBuffer, size.x, size.y, "screenTexture", 1);

  glBindVertexArray(VAO);

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glDrawElements(GL_TRIANGLES, number_of_elements, GL_UNSIGNED_INT, 0);
}

void Quad::renderImage(char * texturePath)
{
  program->use();
  setTextureFromImage(texturePath, "screenTexture", 1);

  glBindVertexArray(VAO);

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glDrawElements(GL_TRIANGLES, number_of_elements, GL_UNSIGNED_INT, 0);
}


void Quad::setTextureFromImage(const char * texturePath, const char * uniName, int num)
{
  unsigned int texture;

  std::cout << " before " << glGetError() << std::endl;

  glGenTextures(1, &texture);
  glActiveTexture(GL_TEXTURE0 + num);
  glBindTexture(GL_TEXTURE_2D, texture);
  std::cout << " 1 " << glGetError() << std::endl;
  // set the texture wrapping parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  // set texture filtering parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  std::cout << " 2 " << glGetError() << std::endl;
  // load image, create texture and generate mipmaps
  int width, height, nrChannels;
  stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.

  unsigned char *data = stbi_load(texturePath, &width, &height, &nrChannels, 0);
  if (data)
  {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    std::cout << " glTexImage2D " << glGetError() << std::endl;

    glGenerateMipmap(GL_TEXTURE_2D);
    std::cout << " glGenerateMipmap " << glGetError() << std::endl;
  }
  else
  {
    std::cout << "Failed to load texture" << std::endl;
  }
  stbi_image_free(data);

  program->setInt(uniName, num);
}

void Quad::setTextureFromArrayRGB(const std::vector<GLubyte> & buffer, int width, int height, const char * uniName, int num)
{
  unsigned int texture;

  glGenTextures(1, &texture);
  glActiveTexture(GL_TEXTURE0 + num);
  glBindTexture(GL_TEXTURE_2D, texture);
  // set the texture wrapping parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);    // set texture wrapping to GL_REPEAT (default wrapping method)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  // set texture filtering parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer.data());

  glGenerateMipmap(GL_TEXTURE_2D);

  program->setInt(uniName, num);
}

void Quad::setTextureFromArrayHDR(const std::vector<GLfloat> & buffer, int width, int height, const char * uniName, int num)
{
  unsigned int texture;

  glGenTextures(1, &texture);
  glActiveTexture(GL_TEXTURE0 + num);
  glBindTexture(GL_TEXTURE_2D, texture);
  // set the texture wrapping parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);    // set texture wrapping to GL_REPEAT (default wrapping method)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  // set texture filtering parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, buffer.data());

  glGenerateMipmap(GL_TEXTURE_2D);

  program->setInt(uniName, num);
}
