
#include <triangle.hpp>
#include <ray.hpp>


Triangle::Triangle(
        const glm::vec3 & pa, const glm::vec3 & pb, const glm::vec3 & pc,
        const glm::vec3 & na, const glm::vec3 & nb, const glm::vec3 & nc,
        const glm::vec3 & ca, const glm::vec3 & cb, const glm::vec3 & cc,
        const glm::vec2 & ta, const glm::vec2 & tb, const glm::vec2 & tc,
        const Material & m
) : material(m), baryCoord(pa, pb, pc)
{
  pA = pa; pB = pb; pC = pc;

  nA = glm::normalize(na);
  nB = glm::normalize(nb);
  nC = glm::normalize(nc);

  tA = ta; tB = tb; tC = tc;

  glm::vec3 cross = glm::cross((pB-pA), (pC-pA));
  float length = glm::length(cross);

  normal = cross / length;
  surfArea = 0.5f * length;
}


glm::vec3 Triangle::getNormal(const glm::vec3 & point) const
{
  const glm::vec3 barCoor = baryCoord(pA, point);
  return glm::normalize(barCoor.x * nA + barCoor.y * nB + barCoor.z * nC);
}


glm::vec3 Triangle::getColor(const glm::vec3 & point) const
{
  const glm::vec3 barCoor = baryCoord(pA, point);
  return material.getColor(barCoor.x * tA + barCoor.y * tB + barCoor.z * tC);
}


glm::vec3 Triangle::getUniformPoint(const glm::vec2 & p) const
{
  // https://math.stackexchange.com/questions/18686/uniform-random-point-in-triangle
  float r1 = sqrtf(p.x), r2 = p.y;
  return (1.0f - r1) * pA + (r1 * (1.0f - r2)) * pB + (r1 * r2) * pC;
}





glm::vec3 Triangle::getMin() const
{
  glm::vec3 result;
  for (auto i = 0; i < 3; i++) result[i] = std::min(std::min(pA[i], pB[i]), pC[i]);
  return result;
}


glm::vec3 Triangle::getMax() const
{
  glm::vec3 result;
  for (auto i = 0; i < 3; i++) result[i] = std::max(std::max(pA[i], pB[i]), pC[i]);
  return result;
}


bool Triangle::traceRay(Ray & ray) const
{
  // Check that it is not the same triangle
  if (nullptr != ray.getStartSurf() && this == ray.getStartSurf()) return false;

  // Check that the ray is not parallel to the triangle
  float cross = glm::dot(ray.getDirection(), normal);
  if (0.0f == cross) return false;

  // Find ray's distance to the intersection point with the plane where the triangle lies
  const float distance = glm::dot(pA - ray.getOrigin(), normal) / cross;
  if (0.0f >= distance) return false;

  // If the distance to this plane is bigger than the smallest so far,
  // skip checking for intersection with triangle all together
  if (distance >= ray.getDistance()) return false;

  // If smaller check if the ray actually hits the triangle
  const glm::vec3 intersection = ray.getOrigin() + distance * ray.getDirection();
  if (isPointInside(intersection))
  {
    ray.setIntersection(this, intersection, distance);
    return true;
  }

  return false;
}


bool Triangle::isPointInside(const glm::vec3 & point) const
{
  const glm::vec3 barCoor = baryCoord(pA, point);

  // Check where a point is in respect to a triangle using barycentrics
  return 0.0f <= barCoor.x && barCoor.x <= 1.0f &&
         0.0f <= barCoor.y && barCoor.y <= 1.0f &&
         0.0f <= barCoor.z && barCoor.z <= 1.0f;
}

bool Triangle::isSamePlane(const Triangle * cmp) const
{
  if (nullptr == cmp) return false;
  if (this == cmp) return true;

  return (normal == cmp->normal) && (glm::dot(normal, pA - cmp->pA) == 0.0f);
}
