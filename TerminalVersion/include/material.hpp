/*
  A class representing an object's material.
  Implemented as a combination of different materials,
  so it is possible to have a complex material or a pure one of only one type is chosen.
*/

#ifndef material_hpp
#define material_hpp

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <random>

#include <someMath.hpp>

class Material
{
  private:

    float Emission   = 0.0f;
    float DiffSpec   = 0.0f;
    float Reflection = 0.0f;
    float Refraction = 0.0f;

    std::discrete_distribution<int> distribution;

    glm::vec3 Color = glm::vec3(0.0f);

    float radiance   = 1.0f;
    float beamRadius = 1.0f;

    float refractCoef  = 1.0f;

    float specularCoef = 0.0f;
    float specularPow  = 1.0f;

  public:

    inline Material() {};

    // Setters -------------------------------------------------------------------

    Material & setColor(const glm::vec3 & color);

    Material & initEmission(float prob, float rad, float beam);

    Material & initDiffSpec(float prob, float spec, float spow);

    Material & initDiffuse(float prob);

    Material & initReflection(float prob);

    Material & initRefraction(float prob, float coef);


    // Getters -------------------------------------------------------------------

    [[nodiscard]] const std::discrete_distribution<int> & getDistribution() const { return distribution; }

    [[nodiscard]] inline float getEmissionPDF() const   { return Emission; }

    [[nodiscard]] inline float getDiffSpecPDF() const   { return DiffSpec; }

    [[nodiscard]] inline float getReflectionPDF() const { return Reflection; }

    [[nodiscard]] inline float getRefractionPDF() const { return Refraction; }

    [[nodiscard]] inline bool hasEmission() const { return Emission > 0.0f; }

    [[nodiscard]] inline glm::vec3 getColor(const glm::vec2 & texCoord) const { return Color; }

    [[nodiscard]] inline glm::vec3 getColor() const { return Color; }

    [[nodiscard]] inline float getRadiance() const { return radiance; }

    [[nodiscard]] inline float getBeamRadius() const { return beamRadius; }

    [[nodiscard]] inline float getRefractionCoef() const { return refractCoef; }

    [[nodiscard]] inline float getSpecularCoef() const { return specularCoef; }

    [[nodiscard]] inline float getSpecularPower() const { return specularPow; }



    // Fresnel's equation
    [[nodiscard]] float fresnel(float cosi, float etai, float etat) const;

    // Sets material type distribution
    // The tracers use it to probabilistically choose
    // what material type the point traced is.
    void prepareForTracer();
};

#endif
