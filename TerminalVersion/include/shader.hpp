#ifndef shader_hpp
#define shader_hpp

#include <cstdio>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader
{
private:

  void checkCompileErrors(unsigned int shader, std::string type);

public:

  // The program ID
  unsigned int ID;

  // Constructor reads and builds the shader
  Shader(const GLchar* vertexPath, const GLchar* fragmentPath);

  // Use/activate the shader
  void use();

  // Utility uniform functions
  void setBool(const std::string &name, bool value) const;

  void setInt(const std::string &name, int value) const;

  void setFloat(const std::string &name, float value) const;

  void set4Float(const std::string &name, float x, float y, float z, float w) const;

  void set4Matrix(const std::string &name, glm::mat4 objToWorld) const;
};


#endif 
