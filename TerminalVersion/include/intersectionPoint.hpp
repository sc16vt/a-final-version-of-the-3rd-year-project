/*
  A class representing an intersection point.

  An intersection point is a point on the surface which is hit by the ray.
  That ray is cast by a tracer in order to construct a light path.
  It stores all the information about that point that is needed to calculate
  the light sample, such as it's coordinates in the scene,
  PDFs of the rays coming to and going from it, e.t.c.
*/

#ifndef intersectionPoint_hpp
#define intersectionPoint_hpp

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <material.hpp>
#include <triangle.hpp>
#include <someMath.hpp>


class Ray;


enum PointType
{
  Unknown    = 0,
  Ambient    = 1,
  DirLight   = 2,
  Emission   = 3,
  DiffSpec   = 4,
  Reflection = 5,
  Refraction = 6,
  CamPoint   = 7,
};


enum RefractionDir
{
  Inside,
  Outside,
  None,
};


class IntersectionPoint
{
private:

  PointType pointType = Unknown;

  glm::vec3 point;
  glm::vec3 normal;

  const Triangle * triangle = nullptr; // The triangle this point belong to


  // Sampled random value for choosing between reflected and refracted rays
  float refractionSample = 1.0f;

  float prevRefraction = 1.0f;
  float nextRefraction = 1.0f;

  RefractionDir refractionDir = None;

  // BSDF in this surface point in respect of incoming ray and sampled outgoing ray
  glm::vec3 prevBSDF = glm::vec3(1.0f); // when incoming ray is from prev point
  glm::vec3 nextBSDF = glm::vec3(1.0f); // when incoming ray is from next point


  // Related point flags
  bool prevIsDelta = false;
  bool nextIsDelta = false;


  // We mean that all path are starting from Cam (0 index) and ended in Light (size() - 1 index)
  // independent on how we build this path (from Cam or from Light ot from both directions)
  //   Cam ... prev point <-> this point <-> next point ... Light

  // PDFs
  float prevRayPDF = 1.0f; // sampled ray PDF for Prev ray depends of tracing direction
  float nextRayPDF = 1.0f; // sampled ray PDF for Next ray depends of tracing direction

  // Geometry factors
  float prevG   = 1.0f; // G(prev <-> this) === cosPrev * cosThis / distance^2
  float nextG   = 1.0f; // G(this <-> next) === cosThis * cosNext / distance^2

  float prevCos = 1.0f; // cos(Prev ray X Prev point surface normal)
  float nextCos = 1.0f; // cos(Next ray X Next point surface normal)

public:

  inline IntersectionPoint()
  {
    setSampledData(Unknown);
  }

  inline IntersectionPoint(const glm::vec3 & coord, const Triangle * tr = nullptr)
  {
    point     = coord;
    normal    = (tr != nullptr ? tr->getNormal(point) : glm::vec3(0.0f));
    triangle  = tr;

    setSampledData(Unknown);
  }

  inline IntersectionPoint(PointType type, const glm::vec3 & coord, const Triangle * tr = nullptr)
  {
    point     = coord;
    normal    = (tr != nullptr ? tr->getNormal(point) : glm::vec3(0.0f));
    triangle  = tr;

    setSampledData(type);
  }

  // Point types ---------------------------------------------------------------

  [[nodiscard]] inline PointType getType() const { return pointType; };


  [[nodiscard]] inline bool isE() const { return (CamPoint  == pointType); }

  [[nodiscard]] inline bool isL() const { return (Ambient   == pointType) || (DirLight  == pointType) || (Emission  == pointType); }

  [[nodiscard]] inline bool isD() const { return (DiffSpec  == pointType);}

  [[nodiscard]] inline bool isS() const { return (Reflection == pointType) || (Refraction == pointType); }

  [[nodiscard]] inline bool isLd() const { return (DirLight  == pointType); } // Dir light

  [[nodiscard]] inline bool isLi() const { return (Ambient   == pointType) || (DirLight  == pointType); } // Infinite light


  [[nodiscard]] inline bool isUnset() const { return (Unknown   == pointType); }

  [[nodiscard]] inline bool isEnd() const
  {
    return (Unknown   == pointType) ||
           (Ambient   == pointType) ||
           (DirLight  == pointType) ||
           (Emission  == pointType) ||
           (CamPoint  == pointType);
  }

  [[nodiscard]] inline bool isSurface() const
  {
    return (DiffSpec   == pointType) ||
           (Reflection == pointType) ||
           (Refraction == pointType) ||
           (Emission   == pointType);
  }

  [[nodiscard]] inline bool isDelta() const
  {
    return (DirLight   == pointType) ||
           (Reflection == pointType) ||
           (Refraction == pointType);
  }


  // General getters -----------------------------------------------------------

  [[nodiscard]] inline const glm::vec3 & getPoint() const { return point; }

  [[nodiscard]] inline const glm::vec3 & getNormal() const { return normal; }

  [[nodiscard]] inline float getObjectArea() const { return triangle->getObjArea(); }

  [[nodiscard]] inline glm::vec3 getSurfaceColor() const { return triangle->getColor(point); }

  [[nodiscard]] inline const Material & getMaterial() const { return triangle->getMaterial(); }

  [[nodiscard]] inline const Triangle * getTriangle() const { return triangle; }

  [[nodiscard]] inline bool isBlack() const
  {
    return
           (prevBSDF.x + prevBSDF.y + prevBSDF.z <= 0.0f) ||
           (nextBSDF.x + nextBSDF.y + nextBSDF.z <= 0.0f);
  }


  // Refraction related functions ----------------------------------------------
  [[nodiscard]] inline float getRefractionSample() const { return refractionSample; }
  inline void setRefractionSample(float val) { refractionSample = val; }

  [[nodiscard]] inline float getPrevRefraction() const { return prevRefraction; }
  inline void setPrevRefraction(float val) { prevRefraction = val; }

  [[nodiscard]] inline float getNextRefraction() const { return nextRefraction; }
  inline void setNextRefraction(float val) { nextRefraction = val; }

  [[nodiscard]] inline RefractionDir getRefractionDir() const { return refractionDir; }
  inline void setRefractionDir(RefractionDir val) { refractionDir = val; }


  // Setters for samples data --------------------------------------------------
  inline void setSampledData(PointType rt)
  {
    pointType = rt;

    switch (pointType)
    {
      case CamPoint:
        prevBSDF = glm::vec3(1.0f);
        nextBSDF = glm::vec3(1.0f);
        break;

      case Unknown:
        prevBSDF = glm::vec3(0.0f);
        nextBSDF = glm::vec3(0.0f);
        break;

      default: break;
    }
  }

  inline void setSampledData(PointType rt, const glm::vec3 & pbsdf, const glm::vec3 & nbsdf, float ppdf, float npdf)
  {
    pointType  = rt;
    prevBSDF   = pbsdf;
    nextBSDF   = nbsdf;
    prevRayPDF = ppdf;
    nextRayPDF = npdf;
  }


  // Get BSDF in point
  [[nodiscard]] inline const glm::vec3 & getPrevBSDF() const { return prevBSDF; }
  [[nodiscard]] inline const glm::vec3 & getNextBSDF() const { return nextBSDF; }


  // Getters and setters for PDFs
  inline void swapPrevPDF(IntersectionPoint & p) { std::swap(p.nextRayPDF, prevRayPDF); }
  inline void swapNextPDF(IntersectionPoint & p) { std::swap(nextRayPDF, p.prevRayPDF); }

  // Sampled PDF in respect of solid angle and projected solid angle
  // In case of delta function we need to use probability P instead of dP / dA so we have to skip cosine
  [[nodiscard]] inline float getPrevPDFpa() const{ return prevRayPDF / (prevIsDelta ? 1.0f : prevCos);}
  [[nodiscard]] inline float getNextPDFpa() const { return nextRayPDF / (nextIsDelta ? 1.0f : nextCos); }

  [[nodiscard]] inline float getPrevPDFsa() const { return getPrevPDFpa() * prevG; }
  [[nodiscard]] inline float getNextPDFsa() const { return getNextPDFpa() * nextG; }


  // Getters and setters for Cosines
  inline void setPrevCos(float cosine) { prevCos = cosine; }
  inline void setNextCos(float cosine) { nextCos = cosine; }


  // Getters and setters for prev and next points are Delta points
  [[nodiscard]] inline bool isPrevDelta() const { return prevIsDelta; }
  inline void setPrevIsDelta(bool delta) { prevIsDelta = delta; }

  [[nodiscard]] inline bool isNextDelta() const { return nextIsDelta; }
  inline void setNextIsDelta(bool delta) { nextIsDelta = delta; }


  // Getters and setters for G factors
  [[nodiscard]] inline float getPrevG() const { return prevG; }
  inline void setPrevG(float g) { prevG = g; }

  [[nodiscard]] inline float getNextG() const { return nextG; }
  inline void setNextG(float g) { nextG = g; }

};



#endif
