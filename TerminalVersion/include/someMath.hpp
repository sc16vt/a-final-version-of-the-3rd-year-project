/*
  A class containing some general often used calculations
*/
#ifndef someMath_hpp
#define someMath_hpp

// #include <math.h>
#include <cmath>


#include <glm/vec4.hpp>
#include <glm/vec3.hpp>


const float SM_PI   = M_PI;
const float SM_2PI  = 2.0f * SM_PI;

const float SM_INV_PI   = 1.0f / SM_PI;
const float SM_INV_2PI  = 1.0f / SM_2PI;

const float SM_RGB      = 255.0f;
const float SM_INV_RGB  = 1.0f / SM_RGB;


[[nodiscard]] inline float remap0to1(float val)
{
  return (val != 0.0f ? val : 1.0f);
}

[[nodiscard]] inline float luminance(const glm::vec3 & c)
{
  return 0.299f * c.r + 0.587f * c.g + 0.114f * c.b;
}

[[nodiscard]] inline float luminance(const glm::vec4 & c)
{
  return (0.299f * c.r + 0.587f * c.g + 0.114f * c.b) / remap0to1(c.w);
}

// Convert a float vector to int vector
[[nodiscard]] inline glm::ivec2 VtoI(const glm::vec2 & v)
{
  return glm::ivec2(floorf(v.x), floorf(v.y));
}

[[nodiscard]] inline float varea(const glm::vec2 & v)
{
  return fabsf(v.x * v.y);
}

[[nodiscard]] inline int varea(const glm::ivec2 & v)
{
  return abs(v.x * v.y);
}

[[nodiscard]] inline bool vbounds(const glm::vec2 & v, const glm::vec2 & s)
{
  return (0.0f <= v.x && v.x < s.x) &&
         (0.0f <= v.y && v.y < s.y);
}

[[nodiscard]] inline float deltaf(bool condition)
{
  return condition ? 1.0f : 0.0f;
}


#endif
