/*
  A class implementing a Path Tracer algorithm.
*/

#ifndef pathTracer_hpp
#define pathTracer_hpp


#include <baseTracer.hpp>


class PathTracer : public BaseTracer
{
protected:

  int maxPathLength;

  void tracePathFromCam(LightPath & path, int length);

  void tracePixel(const glm::vec2 & crd) override;

public:

  PathTracer(const Scene & sc, const Camera & cam);
};


#endif
