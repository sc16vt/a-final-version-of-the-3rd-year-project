/*
  A class containing all the probability math.
  All the different probability distributions and sampling from them is abstracted by this class.
*/

#ifndef sampler_hpp
#define sampler_hpp

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <someMath.hpp>
#include <material.hpp>
#include <scene.hpp>


class Sampler
{
private:

  std::random_device random_device;
  std::mt19937 random_generator;


  // Generate local coordinate system from surface normal
  void coordSysFromNorm(const glm::vec3 & n, glm::vec3 & a, glm::vec3 & b);

  int random_uniform_int_0I(int i);

  glm::vec2 random_uniform_2D_0I(float to = 1.0f);

  float random_uniform_float_0I(float to = 1.0f);

  float random_normal_float(float a, float d);

  glm::vec2 random_normal_float_2D(float a, float d);

  glm::vec3 random_normal_float_3D(float a, float d);

  glm::vec2 uniformDiskSample(float rad);

  [[nodiscard]] glm::vec3 cosineHemisphereSample(const glm::vec3 & dir, float radius);

  glm::vec3 uniformHemisphereSample(const glm::vec3 & dir);

public:

  // Initialization

  Sampler();

  unsigned int resetRandomGenerator();

  void resetRandomGenerator(unsigned int seed);


  // Sample screen points

  glm::vec2 pixelPointSample(const glm::vec2 & o);

  glm::vec2 screenPointSample(const glm::ivec2 & size);


  // Sample materials

  PointType materialSample(const std::discrete_distribution<int> & distribution);

  glm::vec3 diffuseSample(const IntersectionPoint & point);

  [[nodiscard]] float diffuseSamplePDF(float cosine) const;

  [[nodiscard]] float specularSamplePDF(float cosine, float pow) const;

  float refractionSample();


  // Sample lights

  glm::vec3 lightRaySample(const IntersectionPoint & point, const Scene & scene);

  [[nodiscard]] float lightRaySamplePDF(const IntersectionPoint & point, float cosine) const;

  IntersectionPoint lightPointSample(const Scene & scene);

  [[nodiscard]] float lightPointSamplePDF(const IntersectionPoint & point, const Scene & scene) const;


  // Sample path length

  int pathLengthSample(int lmax);

  int pathPositionSample(int mxl);


  // Sample mutations

  int mltMutationSample(std::discrete_distribution<int> & distribution);

  float pathAcceptanceSample();

  glm::vec3 mutRayOffsetSample(const glm::vec3 & o, float t1, float t2);

  glm::vec2 mutPixelOffsetSample(const glm::vec2 & o, const glm::vec2 & s, float r1, float r2);


  // Number of points to del (number of edges - 1 to del), return value in range 0 .. ndel
  int mutDelPathSample(int ndel);

  [[nodiscard]] float mutDelPathSamplePDF(int points) const;


  // Number of points to add (number of edges - 1 to add), return value in range 0 .. maxsize
  int mutAddPathSample(int ndel, int maxsize);

  // Pa1 from distribution and Pa2 == 1 / (num eges added)
  [[nodiscard]] float mutAddPathSamplePDF(int nadd, int ndel) const;

};


#endif
