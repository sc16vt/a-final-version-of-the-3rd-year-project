/*
  A class representing a triangle.
  It is used to model the geometry in the scene and
  enable intersection check needed by a tracer 
  (whether a given ray goes through this triangle or not).
*/

#ifndef triangle_hpp
#define triangle_hpp

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <material.hpp>

#include <iostream>


class Ray;


// Optimization, stores some constants for barycentric coord calculation
struct BarycentricCoord
{
  glm::vec3 BA;
  glm::vec3 CA;

  float denBB;
  float denBC;
  float denCC;

  float invDen;

  inline BarycentricCoord(const glm::vec3 & pA, const glm::vec3 & pB, const glm::vec3 & pC)
  {
    BA = pB - pA;
    CA = pC - pA;

    denBB = glm::dot(BA, BA);
    denBC = glm::dot(BA, CA);
    denCC = glm::dot(CA, CA);

    invDen = 1.0f / (denBB * denCC - denBC * denBC);
  }

  inline glm::vec3 operator()(const glm::vec3 & pA, const glm::vec3 & point) const
  {
    glm::vec3 PA = point - pA;

    float denPB = glm::dot(PA, BA);
    float denPC = glm::dot(PA, CA);

    float bcB = (denCC * denPB - denBC * denPC) * invDen;
    float bcC = (denBB * denPC - denBC * denPB) * invDen;
    float bcA = 1.0f - bcB - bcC;

    return glm::vec3(bcA, bcB, bcC);
  }
};


class Triangle
{
private:

  glm::vec3 pA, pB, pC; // Location coordinates
  glm::vec3 nA, nB, nC; // Normal coordinates
  glm::vec2 tA, tB, tC; // Texture coordinates

  glm::vec3 normal;     // Normal of the triangle (calculated from triangle points)

  float surfArea;
  float objArea;

  const Material & material;  // A pointer to material of this triangle (same material as of object)

  BarycentricCoord baryCoord;

public:

  Triangle(
      const glm::vec3 & pa, const glm::vec3 & pb, const glm::vec3 & pc,
      const glm::vec3 & na, const glm::vec3 & nb, const glm::vec3 & nc,
      const glm::vec3 & ca, const glm::vec3 & cb, const glm::vec3 & cc,
      const glm::vec2 & ta, const glm::vec2 & tb, const glm::vec2 & tc,
      const Material & m
    );


  // Setters  ------------------------------------------------------------------
  inline void setObjArea(float area) { objArea = area; };

  // Getters  ------------------------------------------------------------------
  [[nodiscard]] inline const glm::vec3 & getNormal() const { return normal; }

  [[nodiscard]] inline const Material & getMaterial() const { return material; }

  [[nodiscard]] inline float getSurfArea() const { return surfArea; };

  [[nodiscard]] inline float getObjArea() const { return objArea; };


  [[nodiscard]] inline float getRadiance() const { return material.getRadiance(); }

  [[nodiscard]] inline float getRefraction() const { return material.getRefractionCoef(); }


  [[nodiscard]] glm::vec3 getNormal(const glm::vec3 & point) const;

  [[nodiscard]] glm::vec3 getColor(const glm::vec3 & point) const;


  [[nodiscard]] glm::vec3 getUniformPoint(const glm::vec2 & p) const;


  [[nodiscard]] glm::vec3 getMin() const;

  [[nodiscard]] glm::vec3 getMax() const;



  // Checks if this triangle is hit by the given ray and is the closest to it so far
  [[nodiscard]] bool traceRay(Ray & ray) const;

  // Check if the given point is inside this triangle or not
  [[nodiscard]] bool isPointInside(const glm::vec3 & point) const;

  // Check if the given triangle lies in the same plane as this
  [[nodiscard]] bool isSamePlane(const Triangle * cmp) const;
};

#endif
