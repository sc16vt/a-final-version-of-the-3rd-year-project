This is a terminal version of the demo app created for the project "" by Valentina Tabunova.

To install the project:
- open the terminal
- go to folder build
- run "cmake .."
- run "make"
- an exicutable franken is the program


To use the project:

The project has 8 premade scenes available for render. Some of them are availabe
under different angles of the camera in the scene. Additionally, it is possible
to choose the tracing algorithms to render with. To make render faster paralell
option can be turned on. Finally, there are predefined numbers of samples
coressponding to quality levels of the picture that can be chosen. All of those
options are submittied to the program when starting it as usual.

To choose which tracing algorithms will render the image:

"-path"  - Enable Path tracer;
"-bdir"  - Enable Bidirectional tracer;
"-mlt"   - Enable MLT tracer;
"-smmpt" - Enable SMMCLT Path based tracer;
"-smmbd" - Enable SMMCLT Bidirectional based tracer;
"-tall"  - Enable All tracers";


To set a specific quality of image. If none of those options are specified
the renderer will keep going until manually stoped:

"-sl" - Save low quality image;
"-sg" - Save good quality image;
"-sb" - Save best quality image;

To choose which scenes to render "n.m", where n is the number of the scene and
m is a predefined option for the camera position. The scenes available:
  8.0, 8.1    A very dark scene with light shinning through a crack in the wall
  7.0, 7.1    A scene with a bunch of spheres. Half of the scene and spheres are under water
  6.0         A scene with light shinning through a crack in the wall  and then through pink glass box
  5.0, 5.1    A well lit scene with glass box and sphere containing a diffuse sphere and box respectively
  4.0, 4.1    A scene with light shinning through a crack in the wall and then through pink glass sphere
  3.0, 3.1    A scene with a projector like beam of light going through a glass sphere
  2.0         A scene with beam of light reflecting in the mirrors
  1.0         A well lit scene with spheres made of different material


To show the help:
"-h"



Examples of using the program:

"./franken -path -bdir -sb 0.0 3.1"
That command will render the scenes 0.0 and 3.1. It will render using both
path tracer and bidirectional path tracer. The quality of the picture setting is set to max.

"./franken -p -tall ALL"
That command will render all the scenes using all the tracers. It will not stop until stoped manually.
It will run in parallel, making render faster.
